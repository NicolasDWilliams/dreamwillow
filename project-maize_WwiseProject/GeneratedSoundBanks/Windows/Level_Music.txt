Event	ID	Name			Wwise Object Path	Notes
	557932600	Play_Level_Music			\Music\Level_Music\Play_Level_Music	

State Group	ID	Name			Wwise Object Path	Notes
	308635872	Progress			\Default Work Unit\Progress	
	1551306167	UI			\Default Work Unit\UI	
	1926883983	Scene			\Default Work Unit\Scene	

State	ID	Name	State Group			Notes
	0	None	Progress			
	514064485	Fight	Progress			
	779278001	Death	Progress			
	2201105581	Credits	Progress			
	2463176523	Cleared	Progress			
	0	None	UI			
	953277036	Resume	UI			
	3092587493	Pause	UI			
	0	None	Scene			
	251412225	Shop	Scene			
	2607556080	Menu	Scene			
	2782712965	Level	Scene			
	4072209002	Credit	Scene			

Custom State	ID	Name	State Group	Owner		Notes
	316360996	Pause	UI	\Interactive Music Hierarchy\Music\Level_Music_System\Gameplay_Loop_Cleared_Layers		
	423112652	Level	Scene	\Interactive Music Hierarchy\Music\Level_Music_System\Credits		
	447986444	Menu	Scene	\Interactive Music Hierarchy\Music\Level_Music_System\Credits		
	556126322	Menu	Scene	\Interactive Music Hierarchy\Music\Level_Music_System		
	626214860	Pause	UI	\Interactive Music Hierarchy\Music\Level_Music_System\Gameplay_Loop_Fight_Layers		
	791026203	Shop	Scene	\Interactive Music Hierarchy\Music\Level_Music_System		
	835622915	Shop	Scene	\Interactive Music Hierarchy\Music\Level_Music_System\Credits		

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	8723584	4_ harmony	Z:\Volumes\Faulkner\dreamwillow\project-maize_WwiseProject\.cache\Windows\SFX\4. harmony_D79FB2B7.wem		\Interactive Music Hierarchy\Music\Level_Music_System\Gameplay_Loop_Cleared_Layers\Cleared Layers\4_ harmony		15395136
	229404591	maintheme	Z:\Volumes\Faulkner\dreamwillow\project-maize_WwiseProject\.cache\Windows\SFX\maintheme_D79FB2B7.wem		\Interactive Music Hierarchy\Music\Level_Music_System\Credits\maintheme\maintheme		28612820
	465133881	playerDeath6	Z:\Volumes\Faulkner\dreamwillow\project-maize_WwiseProject\.cache\Windows\SFX\playerDeath6_D79FB2B7.wem		\Interactive Music Hierarchy\Music\Level_Music_System\Death_Music_Layers\playerDeath6\playerDeath6		3235940
	491132703	Cleared Mixdown	Z:\Volumes\Faulkner\dreamwillow\project-maize_WwiseProject\.cache\Windows\SFX\Cleared Mixdown_D79FB2B7.wem		\Interactive Music Hierarchy\Music\Level_Music_System\Gameplay_Loop_Fight_Layers\Fight\Cleared Mixdown		15394980
	519977049	Fight Perc	Z:\Volumes\Faulkner\dreamwillow\project-maize_WwiseProject\.cache\Windows\SFX\Fight Perc_D79FB2B7.wem		\Interactive Music Hierarchy\Music\Level_Music_System\Gameplay_Loop_Fight_Layers\Fight\Fight Perc		15394980
	595928976	Cleared Harmony 2	Z:\Volumes\Faulkner\dreamwillow\project-maize_WwiseProject\.cache\Windows\SFX\Cleared Harmony 2_D79FB2B7.wem		\Interactive Music Hierarchy\Music\Level_Music_System\Gameplay_Loop_Cleared_Layers\Cleared Layers\Cleared Harmony 2		15394980
	925140149	3_ lead	Z:\Volumes\Faulkner\dreamwillow\project-maize_WwiseProject\.cache\Windows\SFX\3. lead_D79FB2B7.wem		\Interactive Music Hierarchy\Music\Level_Music_System\Gameplay_Loop_Cleared_Layers\Cleared Layers\3_ lead		15395136
	1072397870	Voices	Z:\Volumes\Faulkner\dreamwillow\project-maize_WwiseProject\.cache\Windows\SFX\Voices_D79FB2B7.wem		\Interactive Music Hierarchy\Music\Level_Music_System\Gameplay_Loop_Fight_Layers\Fight\Voices		15394980

