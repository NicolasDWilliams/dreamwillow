Event	ID	Name			Wwise Object Path	Notes
	452547817	Stop_All			\Default Work Unit\Stop_All	
	945336053	Player_Spell			\Player\Player_Spell	
	961245488	Companion_Ambient			\Ambient\General\Companion_Ambient	
	1068092414	Player_Hurt			\Player\Player_Hurt	
	1161789238	Player_Spell_Impact			\Player\Player_Spell_Impact	
	1511177642	Player_Pickup_Coins			\Player\Player_Pickup_Coins	
	2281174335	Companion_Ambient_Stop			\Ambient\General\Companion_Ambient_Stop	
	3083087645	Player_Death			\Player\Player_Death	
	3223646134	Companion_Death			\Companions\Companion_Death	
	3498424654	Companion_Resurrected			\Companions\Companion_Resurrected	
	3592264982	Rusher_Hurt			\Enemies\Rusher_Hurt	

In Memory Audio	ID	Name	Audio source file		Wwise Object Path	Notes	Data Size
	71587553	OnHit_Dialogue	Z:\Volumes\Faulkner\project-maize\project-maize_WwiseProject\.cache\Windows\SFX\OnHit_Dialogue_D045EF7B.wem		\Actor-Mixer Hierarchy\Default Work Unit\World\Player\Player\Health\OnHit\OnHit_Dialogue\OnHit_Dialogue		80496
	74386538	coin	Z:\Volumes\Faulkner\project-maize\project-maize_WwiseProject\.cache\Windows\SFX\coin_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\World\Player\Player_Pickup\Pickup_Coins\coin		97804
	80568971	Enemy_Oof	Z:\Volumes\Faulkner\project-maize\project-maize_WwiseProject\.cache\Windows\SFX\Oof_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\World\Enemies\Enemies\Rusher\Health\OnHit_Dialogue\Enemy_Oof		74152
	182141750	smw_coin	Z:\Volumes\Faulkner\project-maize\project-maize_WwiseProject\.cache\Windows\SFX\smw_coin_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\World\Player\Player_Pickup\Pickup_Coins\smw_coin		18546
	277428614	OnHit_1	Z:\Volumes\Faulkner\project-maize\project-maize_WwiseProject\.cache\Windows\SFX\zMB6^HEAVY HIT.M_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\World\Player\Player\Health\OnHit\Punch\OnHit_1		138304
	333745687	Player_Death_Temp	Z:\Volumes\Faulkner\project-maize\project-maize_WwiseProject\.cache\Windows\SFX\Player_Death_Temp_20E638CF.wem		\Actor-Mixer Hierarchy\Default Work Unit\World\Player\Player\Health\Player_Death\Player_Death_Temp		300160
	336302377	OnHit_2	Z:\Volumes\Faulkner\project-maize\project-maize_WwiseProject\.cache\Windows\SFX\zBW4^Wet impact.M_790FB848.wem		\Actor-Mixer Hierarchy\Default Work Unit\World\Player\Player\Health\OnHit\Punch\OnHit_2		57130
	410059834	Companion_Ambient	Z:\Volumes\Faulkner\project-maize\project-maize_WwiseProject\.cache\Windows\SFX\Companion_Ambient_C5579977.wem		\Actor-Mixer Hierarchy\Default Work Unit\World\Ambient\Ambiences\Ambiences_Emitters\Companion_Ambient		977308
	421718574	Companion_Resurrected	Z:\Volumes\Faulkner\project-maize\project-maize_WwiseProject\.cache\Windows\SFX\Companion Aquired_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\World\Companions\Companions\Companion_Resurrected\Companion_Resurrected		793864
	667444075	Companion_Death	Z:\Volumes\Faulkner\project-maize\project-maize_WwiseProject\.cache\Windows\SFX\Companion Lost_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\World\Companions\Companions\Companion_Death\Companion_Death		705664
	702918488	OnHit_Dialogue_02	Z:\Volumes\Faulkner\project-maize\project-maize_WwiseProject\.cache\Windows\SFX\OnHit_Dialogue_4829E30D.wem		\Actor-Mixer Hierarchy\Default Work Unit\World\Player\Player\Health\OnHit\OnHit_Dialogue\OnHit_Dialogue_02		78888
	824881915	spellMagicVariant1	Z:\Volumes\Faulkner\project-maize\project-maize_WwiseProject\.cache\Windows\SFX\spellMagicVariant1_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\World\Player\Player\Magic\Player_Spell\Magic_Accent\spellMagicVariant1		768064
	851525665	OnHit_Dialogue_03	Z:\Volumes\Faulkner\project-maize\project-maize_WwiseProject\.cache\Windows\SFX\OnHit_Dialogue_C2C6F819.wem		\Actor-Mixer Hierarchy\Default Work Unit\World\Player\Player\Health\OnHit\OnHit_Dialogue\OnHit_Dialogue_03		120696
	913368152	spellMagicVariant2	Z:\Volumes\Faulkner\project-maize\project-maize_WwiseProject\.cache\Windows\SFX\spellMagicVariant2_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\World\Player\Player\Magic\Player_Spell\Magic_Accent\spellMagicVariant2		768064
	1058179729	punchMagic	Z:\Volumes\Faulkner\project-maize\project-maize_WwiseProject\.cache\Windows\SFX\punchMagic_10C4C929.wem		\Actor-Mixer Hierarchy\Default Work Unit\World\Player\Player\Magic\Player_Spell\punchMagic		768064

