State Group	ID	Name			Wwise Object Path	Notes
	308635872	Progress			\Default Work Unit\Progress	
	1551306167	UI			\Default Work Unit\UI	
	1926883983	Scene			\Default Work Unit\Scene	

State	ID	Name	State Group			Notes
	0	None	Progress			
	514064485	Fight	Progress			
	779278001	Death	Progress			
	2201105581	Credits	Progress			
	2463176523	Cleared	Progress			
	0	None	UI			
	953277036	Resume	UI			
	3092587493	Pause	UI			
	0	None	Scene			
	251412225	Shop	Scene			
	2607556080	Menu	Scene			
	2782712965	Level	Scene			
	4072209002	Credit	Scene			

Game Parameter	ID	Name			Wwise Object Path	Notes
	1006694123	Music_Volume			\Default Work Unit\Music_Volume	
	1564184899	SFX_Volume			\Default Work Unit\SFX_Volume	
	4179668880	Master_Volume			\Default Work Unit\Master_Volume	

Audio Bus	ID	Name			Wwise Object Path	Notes
	1502772432	SFX Bus			\Default Work Unit\Master Audio Bus\SFX Bus	
	3127962312	Music Bus			\Default Work Unit\Master Audio Bus\Music Bus	
	3803692087	Master Audio Bus			\Default Work Unit\Master Audio Bus	

Auxiliary Bus	ID	Name			Wwise Object Path	Notes
	3657263530	Verb			\Default Work Unit\Master Audio Bus\Verb	

Effect plug-ins	ID	Name	Type				Notes
	1018826380	Wwise_Parametric_EQ_(Custom)	Wwise Parametric EQ			
	3442768367	Hall_Large_Dark	Wwise RoomVerb			

Audio Devices	ID	Name	Type				Notes
	2317455096	No_Output	No Output			
	3859886410	System	System			

