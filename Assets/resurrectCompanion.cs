﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class resurrectCompanion : MonoBehaviour
{
    public UnityEvent onResurrect;
    void Start()
    {
        var main = GetComponent<ParticleSystem>().main;
        main.stopAction = ParticleSystemStopAction.Callback;
    }

    void OnParticleSystemStopped()
    {
        PlayerInventory playerInventory = PlayerCharacter.Get().GetComponent<PlayerInventory>();

        if (playerInventory.collarsUsed < playerInventory.CollarsMax)
        {
            onResurrect.Invoke();
        }
    }
}
