﻿// This is used to storing the value of settings menu sliders
// - Matt Rader 12/7/19

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class FloatSettingVariable : ScriptableObject
{
    public float multiplier = 1;
}
