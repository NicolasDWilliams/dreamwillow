﻿// This is used to storing the value of settings menu toggles
// - Matt Rader 12/10/19

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BoolSettingVariable : ScriptableObject
{
    public bool toggle;
}
