﻿// Scriptable object to hold information about controller prompt images used in UI
// Created by Nicolas Williams, 11/12/2019
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ControllerPrompt : ScriptableObject, ISerializationCallbackReceiver
{
    public Sprite xboxOne, xbox360, dualshock4, mouseKeyboard;

    private Dictionary<DeviceType, Sprite> deviceMap;

    // read-only access to the dictionary (getter voodoo magic :D)
    // return appropriate controller icon. If device type is not found, return mouse & keyboard icon
    public Sprite this[DeviceType deviceKey] => deviceMap.ContainsKey(deviceKey) ? this.deviceMap[deviceKey] : mouseKeyboard;


    // reset to initial values
    public void OnAfterDeserialize()
    {
        // initialize map of device type -> sprite
        deviceMap = new Dictionary<DeviceType, Sprite>()
        {
            {DeviceType.XboxOne, xboxOne},
            {DeviceType.Xbox360, xbox360},
            {DeviceType.Dualshock4, dualshock4},
            {DeviceType.MouseAndKeyboard, mouseKeyboard}
        };
    }

    // required for ISerializationCallbackReceiver interface
    public void OnBeforeSerialize() { }
}
