// Scriptable objects specialized for audio events. allows audio team work to be
// completely decoupled from all other work, minimizing risk of merge conflicts during the workflow
// Created by Nicolas Williams, 11/17/2019
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class AudioEvent : ScriptableObject
{
    public AK.Wwise.Event wwiseEvent;

    // triggers wwise event, takes in the object calling this function
    public void PostWwiseEvent(GameObject caller)
    {
        // post wwise event
        if (wwiseEvent.IsValid())
        {
            Debug.Log(wwiseEvent);
            wwiseEvent.Post(caller);
        }
        // audio not yet created
        else
        {
            Debug.LogWarning("Warning: missing audio for audio event: " + name);
        }
    }
}
