﻿// Scriptable object to contain global game state
// Created by Nicolas Williams, 11/12/2019
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GameInformation : ScriptableObject, ISerializationCallbackReceiver
{
    public DeviceType activeDeviceType;

    // reset to initial values
    public void OnAfterDeserialize()
    {
        activeDeviceType = DeviceType.MouseAndKeyboard;
    }

    public void OnBeforeSerialize() { }
}
