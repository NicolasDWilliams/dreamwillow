﻿/**
 * Attach this script to a food object to make it pick-up-able.
 * Change heal amount in the inspector, if desired
 * Zena Abulhab
 */

using UnityEngine;

public class FoodPickup : MonoBehaviour
{

    // TODO: heal partial hearts instead once health script/UI can handle that
    public float healAmount = 1f; // current default

    public UnityEngine.Events.UnityEvent OnPickup;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PlayerCharacter>() != null)
        {
            // If the player collides with this food item, restore their health as 
            // much as possible; if at full health, do nothing
            if (other.GetComponent<Health>().Current < other.GetComponent<Health>().max)
            {
                other.GetComponent<Health>().Heal((int)healAmount);
                Destroy(gameObject);
                OnPickup.Invoke();
            }
        }

    }

}