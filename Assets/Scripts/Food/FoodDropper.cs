﻿/**
 * Attach this script to an enemy to make it drop food with a random chance.
 * Uses FoodDropperEditor script to enable custom input for drop chance
 * when not using the default floor drop chance.
 * Zena Abulhab
 */

using UnityEngine;

public class FoodDropper : MonoBehaviour
{
    // The chance that this enemy will drop 1 food item
    public float dropChance;

    // Should we use the floor default spawn chance? Default is true.
    public bool useDefaultChance = true;

    // How far from the enemy the food can spawn. 
    // As of final sprint, on't change from 1 for tilemap collision detection reasons
    // (to avoid spawning food inside water or walls)
    private int dropRadius = 1;

    // The food manager for this floor
    private FoodSpawnManager foodManager;

    private void Start()
    {
        // Assign food manager
        foodManager = (FoodSpawnManager)FindObjectOfType(typeof(FoodSpawnManager));

        // Find and assign default drop chance, if needed
        if (useDefaultChance)
        {
            dropChance = foodManager.FloorDefaultChance;

        }
        // (else)
        // dropChance is already assigned a custom value, if needed, through FoodDropperEditor

        // subscribing to OnDeathEvent is done in the enemy's inspector under Health
    }

    /// <summary>
    /// Drops random food with a random chance of success
    /// </summary>
    public void TryDropRandomFood()
    {
        // Exclude 0% chance by starting at 1% chance
        if (Random.Range(0.01f,1f) <= dropChance)
        {
            Vector3 spawnPosition = GetSpawnPosition();
            Instantiate(foodManager.GetRandomFood(),spawnPosition, Quaternion.identity);
        }
    }

    /// <summary>
    /// Drops a given food item with random chance of success
    /// </summary>
    public void TryDropSpecificFood(GameObject specificFood)
    {
        if (specificFood == null)
        {
            Debug.LogError("You need to specify which food " + gameObject.name + " drops!");
            return;
        }
        // Exclude 0% chance by starting at 1% chance
        if (Random.Range(0.01f, 1f) <= dropChance)
        {
            Vector3 spawnPosition = GetSpawnPosition();
            Instantiate(foodManager.GetSpecificFood(specificFood), spawnPosition, Quaternion.identity);
        }
    }

    /// <summary>
    /// Returns a random Vector3 position for one food item to spawn at
    /// If food spawns inside an inaccessible place, the position will be recalculated
    /// </summary>
    /// <returns>The spawn position in relation to the enemy position.</returns>
    private Vector3 GetSpawnPosition()
    {
        // calculate a spawn location until an accesible one is found
        // HACK: use 10 as a limit rather than a while loop because having food spawn
        // in an unreachable place is better than having the game crash because someone 
        // designed a level to allow an enemy to walk on water
        for (int i = 0; i<10; i++)
        {
            Vector3 position = transform.position + (Vector3)(Random.insideUnitCircle * dropRadius);

            // check if collides with something (eg the tilemap colliders for water and walls)
            // radius of 1 for approximate food size and better chance of hitting collider
            Collider2D[] hitColliders = Physics2D.OverlapCircleAll(position, 1f);

            if (hitColliders.Length != 0)
            {
                // check if any colliders are unreachable places
                foreach (Collider2D c in hitColliders)
                {
                    // if the point is inside a non-trigger, solid collider
                    if (!c.isTrigger)
                    {
                        // if not colliding with with players, enemies, and companions
                        if (!(c.gameObject.tag == "Player") &&
                            !(c.gameObject.tag == "Enemy") &&
                            !(c.gameObject.tag == "Companion"))
                        {
                            // we must have hit water or a wall
                            break; //generate a new position

                        }
                    }
                }
            }
            return position;
        }
        // at this point, we have tried 10 times and we give up and spawn
        return transform.position + (Vector3)(Random.insideUnitCircle * dropRadius);

    }
}