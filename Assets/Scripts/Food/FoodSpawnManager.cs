﻿/**
 * Include this script in a level scene to make enemies spawn food.
 * Default spawn chance can be changed from the enemy inspector on a
 * per-enemy basis from the FoodDropper script attached to each.
 * Zena Abulhab
 */

using UnityEngine;
using System.Collections.Generic; // for List

public class FoodSpawnManager : MonoBehaviour
{
    /// <summary>
    /// 0 = will never spawn, 1 = will always spawn
    /// </summary>
    public float FloorDefaultChance = .5f;

    /// <summary>
    /// List of all food objects that can be dropped
    /// </summary>
    public List<GameObject> foodItems;


    /// <summary>
    /// Called by FoodDropper. Returns random food item.
    /// </summary>
    /// <returns>Random food item.</returns>
    public GameObject GetRandomFood()
    {
        return foodItems[Random.Range(0, foodItems.Count)];
    }

    /// <summary>
    /// Called by FoodDropper. Returns requested food item.
    /// </summary>
    /// <returns>Specific food item.</returns>
    public GameObject GetSpecificFood(GameObject foodObject)
    {
        foreach (GameObject food in foodItems)
        {
            if (food.name == foodObject.name)
            {
                return food;
            }
        }
        // else
        Debug.Log("You input an invalid food name!");
        return null;
    }
}
