﻿/* Applies damage through OnCollisionEnter, uses a layermask to filter
 * -Max Perraut
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class DamageOnCollision : MonoBehaviour
{
    public int damage;

    [Tooltip("Only damages objects on these layers.")]
    public LayerMask damageLayers;

    void DoDamage(GameObject target)
    {
        //Check if collision is included in layermask
        if ((damageLayers.value & 1 << target.layer) != 0)
        {
            //Find health component on collided object
            Health health = target.GetComponent<Health>();
            if (health)
            {
                health.ApplyDamage(damage);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        DoDamage(collision.gameObject);
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        DoDamage(collision.gameObject);
    }
}
