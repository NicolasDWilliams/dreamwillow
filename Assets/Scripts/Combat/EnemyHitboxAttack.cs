﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/*
 * Controls the attack of an enemy that uses a hitbox.
 * Written by Nikhil Ghosh
 */

public class EnemyHitboxAttack : MonoBehaviour
{
    bool isAttacking = false; // Is the enemy attacking at this moment

    private GameObject playerObject;
    Collider2D hitbox; // A reference to the hitbox

    public UnityEngine.Events.UnityEvent OnAttackStart; // An event triggered when the attack starts
    public UnityEngine.Events.UnityEvent OnAttackDamage; // An event triggered when the attack makes contact
    public UnityEngine.Events.UnityEvent OnAttackFinish; // An event trigger when the attack ends

    [Tooltip("How long the attack lasts")]
    public float attackDuration; // The total duration of the attack
    [Tooltip("At what point in the animation does the attack make contact")]
    public float timeOfHit; // The point in time at which contact hits
    [Tooltip("How much damage to apply")]
    public int damageAmount; // How much damage to do to the player
    [Tooltip("Should the hitbox rotate towards the player")]
    public bool rotateHitbox = true; // Should the hitbox rotate towards the player
    [Tooltip("Should the attack be controlled by animation or by Invoking")]
    public bool shouldInvoke = true;
    [Tooltip("The animator that might require information about the hitbox attack")]
    public Animator animator;

    public bool GetIsAttacking { get { return isAttacking; } } // What it sounds like

    // Start is called before the first frame update
    void Start()
    {
        playerObject = PlayerCharacter.Get();
        hitbox = gameObject.GetComponent<Collider2D>();

        InvokeRepeating("AdjustHitbox", 0.5f, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {

    }

    // Called when the target enters the hitbox
    public void StartAttack()
    {
        if(!isAttacking)
        {
            if (playerObject != null)
            {
                bool isInHitbox = hitbox.OverlapPoint((Vector2)playerObject.transform.position);
                Debug.Log(isInHitbox);
                if(isInHitbox)
                {
                    OnAttackStart.Invoke();
                    isAttacking = true;
                    if(shouldInvoke)
                    {
                        Invoke("AttackHit", timeOfHit);
                        Invoke("EndAttack", attackDuration);
                    }
                    if(animator != null)
                    {
                        if (AnimationFunctions.HasParameter(animator, "isAttacking"))
                        {
                            animator.SetBool("isAttacking", true);
                        }
                    }
                }
            }
        }
    }

    // Called when the attack is actually supposed to make contact
    public void AttackHit()
    {
        if (playerObject != null)
        {
            bool isInHitbox = hitbox.OverlapPoint((Vector2)playerObject.transform.position);
            if (isInHitbox)
            {
                Health targetHealth = playerObject.GetComponent<Health>();
                if(targetHealth != null)
                {
                    targetHealth.ApplyDamage(damageAmount);
                    OnAttackDamage.Invoke();
                }
            }
        }
    }

    // Called when the attack ends
    public void EndAttack()
    {
        isAttacking = false;
        OnAttackFinish.Invoke();
        StartAttack();
        if (animator != null)
        {
            if (AnimationFunctions.HasParameter(animator, "isAttacking"))
            {
                animator.SetBool("isAttacking", false);
            }
        }
    }

    // Called every quarter second to adjust the hitbox so it faces the player.
    // (If this function didn't exist, the player would have to be directly under the enemy for the attack to register)
    void AdjustHitbox()
    {
        if(!isAttacking && rotateHitbox)
        {
            if (playerObject != null)
            {
                Vector2 targetToTransform = (Vector2)(playerObject.transform.position - transform.position);
                Vector3 euler = new Vector3(0f, 0f, Vector2.SignedAngle(-targetToTransform, Vector2.right) - 90f);
                transform.rotation = Quaternion.Euler(euler);
            }
        }
    }
}
