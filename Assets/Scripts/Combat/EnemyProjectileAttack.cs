﻿// Shoots projectiles
// @Alex Kisil

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectileAttack : MonoBehaviour
{
    // EXTERNAL REFERENCES
    public GameObject projectilePrefab;
    private GameObject playerObject;
    private AimPivot aimPivot;

    //PARAMETERS
    [SerializeField] private float fireInterval = 1.5f;
    [Tooltip("Time from start of prefire to launching of projectile")]
    public float prefireTime = 0.2f;
    [Tooltip("Distance from the center of transform where the projectile spawns")]
    [SerializeField] private float spawnDistance = 1f;
    [SerializeField] float projectileSpeed = 5f;
    [SerializeField] int damage = 1;

    [SerializeField] private bool multiFire = false;
    [SerializeField] private float multiFireInterval = 2.5f;
    [SerializeField] private float multiPreFireTime = 1.0f;
    [SerializeField] private int multiProjectileCount = 6;
    [SerializeField] private float multiProjectileAngle = 0f;

    //EVENTS
    public UnityEngine.Events.UnityEvent OnPrefire; //Start of firing seqeunce
    public UnityEngine.Events.UnityEvent OnFire; //Actual projectile launch


    //Internal state
    private float nextFireTime = 0f;
    private bool firing = false;
    private bool shootWhileEnabled = false;

    private void Start()
    {
        playerObject = PlayerCharacter.Get();
        aimPivot = gameObject.transform.parent.gameObject.GetComponent<AimPivot>();
    }

    public void SetShootWhileEnabledLow()
    {
        shootWhileEnabled = false;
    }

    public void SetShootWhileEnabledHigh()
    {
        shootWhileEnabled = true;
    }

    // Fires a projectile at a target
    // This method is public so it can be called from a character controller
    public void Shoot()
    {
        if (nextFireTime <= Time.time)
        {
            if (!firing)
            {
                //Debug.Log("FIRING. player: " + playerObject.transform.position + " enemy: " + transform.position);
                aimPivot.AimToward(playerObject.transform.position - transform.position);
                if (multiFire)
                {
                    StartCoroutine(DoMultiShot());
                }
                else
                {
                    StartCoroutine(DoShot());
                }
            }
        }
    }

    private void Update()
    {
        if (shootWhileEnabled)
        {
            Shoot();
        }
    }

    private IEnumerator DoShot()
    {
        // Begin prefire
        firing = true;
        nextFireTime = Time.time + fireInterval;
        OnPrefire.Invoke();

        // Wait through prefire
        yield return new WaitForSeconds(prefireTime);

        //Instantiate(projectilePrefab, transform.position + transform.up * spawnDistance, transform.rotation);
        GameObject spawnedProjectile = Instantiate(projectilePrefab);

        //Set transform and velocity
        spawnedProjectile.transform.position = transform.position + (Vector3)(transform.right * spawnDistance);
        spawnedProjectile.transform.right = transform.right;
        spawnedProjectile.GetComponent<Rigidbody2D>().velocity = transform.right * projectileSpeed;

        //Set damage
        spawnedProjectile.GetComponent<DamageOnTrigger>().damage = damage;

        OnFire.Invoke();
        firing = false;
    }

    private IEnumerator DoMultiShot()
    {
        // Begin prefire
        firing = true;
        nextFireTime = Time.time + multiFireInterval;
        OnPrefire.Invoke();

        // Wait through prefire
        yield return new WaitForSeconds(multiPreFireTime);

        //Set transform and velocity
        for (int i = 0; i < multiProjectileCount; ++i)
        {
            GameObject spawnedProjectile = Instantiate(projectilePrefab);

            float angle = 360f / multiProjectileCount * i + multiProjectileAngle;
            Vector3 direction = Quaternion.AngleAxis(angle, Vector3.forward) * transform.right;
            spawnedProjectile.transform.position = transform.position + (Vector3)(direction * spawnDistance);
            spawnedProjectile.transform.right = direction;
            spawnedProjectile.GetComponent<Rigidbody2D>().velocity = direction * projectileSpeed;

            //Set damage
            spawnedProjectile.GetComponent<DamageOnTrigger>().damage = damage;
        }

        OnFire.Invoke();
        firing = false;
    }
}
