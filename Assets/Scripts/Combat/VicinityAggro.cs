﻿/* Aggro's enemies within a certain radius of this gameobject
 * - Alex Kisil
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VicinityAggro : MonoBehaviour
{
    private List<GameObject> enemiesInVicinity;
    private GameObject player;

    void Awake()
    {
        enemiesInVicinity = new List<GameObject>();
    }

    private void Start()
    {
        player = PlayerCharacter.Get();
        enemiesInVicinity.Add(gameObject);
    }

    public void AddEnemyToVicinity(GameObject enemy)
    {
        enemiesInVicinity.Add(enemy);
    }

    public void RemoveEnemyFromVicinity(GameObject enemy)
    {
        enemiesInVicinity.Remove(enemy);
    }

    public void AggroVicinity()
    {
        Aggro enemyAggro;
        foreach (GameObject enemy in enemiesInVicinity)
        {
            enemyAggro = enemy.GetComponent<Aggro>();
            if (enemyAggro != null)
            {
                enemyAggro.StartAggro();
            }
        }
    }

}
