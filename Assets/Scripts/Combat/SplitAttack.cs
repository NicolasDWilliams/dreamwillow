﻿// A shooter that splits and destroy itself when exploding.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplitAttack : ProjectileAttack
{
    public float minSpeed, maxSpeed;
    public float countdown = 0.3f;

    private Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Shoot();
    }

    public override void Shoot()
    {
        StartCoroutine(DelayedShoot());
    }

    private IEnumerator DelayedShoot()
    {
        yield return new WaitForSeconds(countdown);
        SpawnFragment();
        Destroy(gameObject);
    }

    private void SpawnFragment()
    {
        Vector3 direction = Quaternion.AngleAxis(-spreadAngle / 2, Vector3.forward) * transform.right;
        Quaternion rotationBetween = Quaternion.AngleAxis(spreadAngle / numProjectiles, Vector3.forward);
        for (int i = 0; i < numProjectiles; ++i)
        {
            float speed = Random.Range(minSpeed, maxSpeed);
            Vector2 velocity = rb.velocity + (Vector2)(direction * speed);
            SpawnProjectile(velocity.normalized, velocity.magnitude);

            direction = rotationBetween * direction;
        }
    }
}
