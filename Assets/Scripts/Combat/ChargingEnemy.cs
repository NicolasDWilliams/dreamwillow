﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum ChargingState
{
    Wandering,
    Stopped,
    PullingBack,
    WaitingForCharge,
    Charging
};

public class ChargingEnemy : MonoBehaviour
{
    [Tooltip("The distance for which the enemy will pullback")]
    public float pullbackDistance;

    [Tooltip("The speed at which the enemy will pullback")]
    public float pullbackSpeed;

    [Tooltip("The speed at which the enemy will charge at")]
    public float chargingSpeed;


    public float timeUntilPullback;
    public float timeUntilCharge;


    [Tooltip("The animation that plays directly after the enemy sees the player")]
    public Animation onSeePlayerAnimation;

    [Tooltip("The animation that plays after the enemy is done pulling back but before the actual charge")]
    public Animation chargeUpAnimation;

    public UnityEvent OnEnemySeePlayer;
    public UnityEvent OnEnemyPullbackStart;
    public UnityEvent OnEnemyPullbackEnd;
    public UnityEvent OnEnemyChargeStart;
    public UnityEvent OnEnemyChargeStop;

    CharacterMovement charMove;
    GameObject player;

    float timeBetweenChecks = 0.75f;

    ChargingState currentState = ChargingState.Wandering;
    Vector2 directionToPlayer = Vector2.zero;

    // Start is called before the first frame update
    void Start()
    {
        charMove = gameObject.GetComponent<CharacterMovement>();
        player = PlayerCharacter.Get();

        //InvokeRepeating("CheckForPlayerInSight", timeBetweenChecks, timeBetweenChecks);

    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(currentState);
    }

    public void StartCheckingForPlayer()
    {
        InvokeRepeating("CheckForPlayerInSight", timeBetweenChecks, timeBetweenChecks);
    }
    public void StopCheckingForPlayer()
    {
        CancelInvoke("CheckForPlayerInSight");
    }

    LayerMask GetLayersToConsider(bool shouldIncludePlayer)
    {
        if (shouldIncludePlayer)
        {
            return ~LayerMask.GetMask("Default", "Trigger", "Camera", "Companion", "Corpse", "Enemy", "Aggro Trigger");
        }
        return ~LayerMask.GetMask("Default", "Trigger", "Camera", "Companion", "Corpse", "Player", "Enemy", "Aggro Trigger");
    }

    void CheckForPlayerInSight()
    {
        if (currentState != ChargingState.Wandering)
        {
            return;
        }

        LayerMask layersToConsider = GetLayersToConsider(true);
        directionToPlayer = player.transform.position - transform.position;
        RaycastHit2D hitFromRaycast = Physics2D.Raycast(transform.position, directionToPlayer, directionToPlayer.magnitude, layersToConsider);
        //Debug.DrawLine(transform.position, (Vector2)transform.position + directionToPlayer, Color.cyan, 0.5f);
        //Debug.Log(hitFromRaycast.collider.gameObject.layer);
        if (hitFromRaycast.collider.tag == player.tag)
        {
            currentState = ChargingState.Stopped;
            OnEnemySeePlayer.Invoke();
            Invoke("StartPullback", timeUntilPullback);
        }
    }

    void StartPullback()
    {
        OnEnemyPullbackStart.Invoke();
        LayerMask layersToConsider = GetLayersToConsider(true);
        RaycastHit2D hitFromRaycast = Physics2D.Raycast(transform.position, -directionToPlayer, pullbackDistance, layersToConsider);
        Debug.DrawLine(transform.position, (Vector2)transform.position - (directionToPlayer * pullbackDistance), Color.blue, 0.5f);

        float actualPullbackDistance = pullbackDistance;
        if (hitFromRaycast.collider != null)
        {
            float pullbackRatio = 0.75f;
            actualPullbackDistance = hitFromRaycast.distance * pullbackRatio;
        }

        currentState = ChargingState.PullingBack;
        charMove.SetMoveSpeed(pullbackSpeed);
        charMove.Move(-directionToPlayer);
        float timeTillPullbackEnd = charMove.GetTimeForDistance(actualPullbackDistance);
        Invoke("EndPullback", timeTillPullbackEnd);
    }

    void EndPullback()
    {
        OnEnemyPullbackEnd.Invoke();
        currentState = ChargingState.WaitingForCharge;
        charMove.SetMoveSpeed(0);
        Invoke("StartCharge", timeUntilCharge);
    }

    void StartCharge()
    {
        LayerMask layersToConsider = GetLayersToConsider(true);
        directionToPlayer = player.transform.position - transform.position;
        float chargingRatio = 5f / 3f;
        RaycastHit2D hitFromRaycast = Physics2D.Raycast(transform.position, directionToPlayer, directionToPlayer.magnitude * chargingRatio, layersToConsider);
        Debug.DrawLine(transform.position, (Vector2)transform.position + (directionToPlayer * chargingRatio), Color.red, 0.5f);

        float actualChargeDistance = directionToPlayer.magnitude * chargingRatio;
        if (hitFromRaycast.collider != null)
        {
            actualChargeDistance = hitFromRaycast.distance - 1.5f;
        }
        Debug.DrawLine(transform.position, (Vector2)transform.position + (directionToPlayer.normalized * actualChargeDistance), Color.red, 0.5f);

        OnEnemyChargeStart.Invoke();
        currentState = ChargingState.Charging;
        charMove.SetMoveSpeed(chargingSpeed);
        charMove.Move(directionToPlayer);
        float timeTillChargeEnd = charMove.GetTimeForDistance(actualChargeDistance);
        Invoke("EndCharge", timeTillChargeEnd);
    }

    void EndCharge()
    {
        OnEnemyChargeStop.Invoke();
        currentState = ChargingState.Wandering;
    }
}
