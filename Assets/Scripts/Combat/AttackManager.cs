﻿// Manages the selection of secondary weopens and cool down.
// @Zhang Zhenyuan
// Edited by Nikhil Ghosh 12/4/2019

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AttackManager : MonoBehaviour
{
    [SerializeField] private Transform primaryAttack;
    [SerializeField] private Transform secondaryAttacks;
    [SerializeField] private int currentSpell;

    private float cooldownEndTime = 0;

    private int maxSecondaryProjectileCount = 16;

	// This event is primarily used to send data to the SecondaryAttackCoolDownVisual script
	[System.Serializable]
	public class CooldownStartEvent : UnityEvent<float> { }
	public CooldownStartEvent OnSecondaryAttackCooldownStart;

    public UnityEvent OnSecondaryAttackCooldownEnd;

    public int SpellCount
    {
        get { return secondaryAttacks.childCount; } 
    }

    public int CurrentSpell
    {
        get { return currentSpell; }
        set { currentSpell = value; }
    }

    public void Attack(bool primary)
    {
		if (Time.time < cooldownEndTime && !primary) {
			return;
		}

        ProjectileAttack attack;

        if (primary)
            attack = primaryAttack.GetComponent<ProjectileAttack>();
        else
        {
            attack = secondaryAttacks.GetChild(currentSpell).GetComponent<ProjectileAttack>();
        }

		if (attack) {
			attack.Shoot();

			if (!primary) {
				cooldownEndTime = attack.CooldownEndTime;
				StartCoroutine(CooldownCounter());
			}
		}
    }

    private IEnumerator CooldownCounter()
    {
		OnSecondaryAttackCooldownStart.Invoke(cooldownEndTime - Time.time);
        yield return new WaitForSeconds(cooldownEndTime - Time.time);
        OnSecondaryAttackCooldownEnd.Invoke();
    }

    public ProjectileAttack GetSecondaryProjectileAttack()
    {
        return secondaryAttacks.GetChild(currentSpell).GetComponent<ProjectileAttack>();
    }

    public int GetSecondaryProjectileCount() {
        return secondaryAttacks.GetChild(currentSpell).GetComponent<ProjectileAttack>().numProjectiles;
    }

    public int GetMaxSecondaryProjectileCount() {
        return maxSecondaryProjectileCount;
    }

    public void SetSecondaryProjectileCount(int newCount) {
        secondaryAttacks.GetChild(currentSpell).GetComponent<ProjectileAttack>().numProjectiles = newCount;
    }
}
