﻿/* Applies damage to objects on trigger collision
 * @Max Perraut
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOnTrigger : MonoBehaviour
{
    public int damage;

    private void DoDamage(GameObject target)
    {
        //Try to get health from collision
        Health health = target.GetComponent<Health>();

        if (health)
        {
            //If a health component was found on the collided-with object, apply damage
            health.ApplyDamage(damage);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        DoDamage(collision.gameObject);
    }
    /*
    private void OnTriggerStay2D(Collider2D collision)
    {
        DoDamage(collision.gameObject);
    }*/
}
