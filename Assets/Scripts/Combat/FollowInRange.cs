﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class VectorEvent : UnityEvent<Vector2> { }

[RequireComponent(typeof(Pathfinder))]
public class FollowInRange : MonoBehaviour
{
    private GameObject player;
    Pathfinder pathfinder;
    public GameObject playerTrigger;
    private CharacterMovement charMove;
    private float rangeToFollow = 5f;
    bool isInRange;

    public UnityEngine.Events.UnityEvent OnEnterRange;
    public UnityEngine.Events.UnityEvent OnExitRange;
    public VectorEvent InCombat;

    private void Awake()
    {
        rangeToFollow = playerTrigger.GetComponent<CircleCollider2D>().radius;
        charMove = GetComponent<CharacterMovement>();
    }

    // Start is called before the first frame update
    void Start()
    {
        player = PlayerCharacter.Get();
        pathfinder = gameObject.GetComponent<Pathfinder>();
        pathfinder.targetPosition = player.transform;
    }

}
