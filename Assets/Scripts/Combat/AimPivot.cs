﻿/* Rotates towards a given aim direction
 * -Max
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AimPivot : MonoBehaviour
{
    [System.Serializable]
    public class AimEvent : UnityEvent<Vector2> { }
    public AimEvent OnAim;
    
    public void AimToward(Vector2 aimVec)
    {
        OnAim.Invoke(aimVec);
        if (!Mathf.Approximately(aimVec.sqrMagnitude, 0))
        {
            transform.right = aimVec;
        }
    }
}
