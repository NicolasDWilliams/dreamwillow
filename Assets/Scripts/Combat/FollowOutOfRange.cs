﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(WSoftPathfinder))]
public class FollowOutOfRange : MonoBehaviour
{
    private GameObject player;
    WSoftPathfinder pathfinder;

    public float rangeToFollow = 5f;

    bool isInRange = false;

    public UnityEngine.Events.UnityEvent OnEnterRange;
    public UnityEngine.Events.UnityEvent OnExitRange;
    public VectorEvent InCombat;

    // Start is called before the first frame update
    void Start()
    {
        player = PlayerCharacter.Get(); // GameObject.FindGameObjectWithTag("Player");
        pathfinder = gameObject.GetComponent<WSoftPathfinder>();
        pathfinder.target = player.transform;
        pathfinder.continousUpdate = true;
    }

    // Update is called once per frame
    void Update()
    {
        float distanceToPlayer = Vector2.SqrMagnitude((Vector2)player.transform.position - (Vector2)transform.position);
        if (distanceToPlayer < rangeToFollow * rangeToFollow)
        {
            pathfinder.currentMode = PathfinderMode.Nothing;
            if(!isInRange)
            {
                OnEnterRange.Invoke();
            }
            isInRange = true;
        }
        else
        {
            pathfinder.currentMode = PathfinderMode.Straight;
            InCombat.Invoke(pathfinder.GetVelocity());
            if (isInRange)
            {
                OnExitRange.Invoke();
            }
            isInRange = false;
        }
    }

    public bool GetIsInRange()
    {
        return isInRange;
    }
}
