﻿/** 
 * Controls enemy death.
 * @Alex Kisil
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyDeath : MonoBehaviour
{
    // INTERNAL REFERENCES
    CharacterMovement charMove;

    // PARAMETERS
    [SerializeField] public GameObject corpseToSpawn;
    [SerializeField] float despawnTime = 0f;


    IEnumerator Disable()
    {
        while (true)
        {
            // Suspend execution for despawnTime seconds
            yield return new WaitForSeconds(despawnTime);

            gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Clean things up upon the enemy's death
    /// </summary>
    public void PreDisable()
    {
        // Set all children (and therefore sprites) to inactive
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }

        // Turn off collider for this enemy
        GetComponent<BoxCollider2D>().enabled = false;

        // Instantiate a corpse in this enemy's place
        GameObject corpse = Instantiate(corpseToSpawn, transform.position, transform.rotation) as GameObject;

        // Disable this object with despawnTime seconds delay
        StartCoroutine(Disable());
    }

}
