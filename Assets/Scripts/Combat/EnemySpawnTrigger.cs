﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnTrigger : MonoBehaviour
{
    [SerializeField]
    private GameObject enemyGroupToSpawn;

    // Start is called before the first frame update
    void Start()
    {
        if (enemyGroupToSpawn != null) {
            enemyGroupToSpawn.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player") {
            enemyGroupToSpawn.SetActive(true);
            gameObject.GetComponent<Collider2D>().enabled = false;
        }
    }
}
