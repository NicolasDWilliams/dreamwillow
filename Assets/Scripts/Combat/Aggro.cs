﻿/* Determines what "aggro" means for a character and allows for aggroing.
 * - Alex Kisil
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Aggro : MonoBehaviour
{

    // REFERENCES
    private GameObject player;
    private Pathfinder pathfinder;
    private PlayerCharacter playerChar;
    private CharacterMovement charMove;
    public GameObject projectileAttackObject;
    private Renderer[] enemyRenderers;
    private EnemyProjectileAttack projectileAttackScript;

    // PARAMETERS
    [SerializeField] bool chaseTarget = false;
    [SerializeField] bool shootTarget = false;
    [SerializeField] float aggroCooldownTime = 3f;
    [SerializeField] GameObject playerTrigger;

    // EVENTS
    public UnityEvent OnAggro;
    public UnityEvent OnStopAggro;

    // STATE
    private bool isAggrod = false;
    private float playerTriggerRadius;

    void Awake()
    {
        pathfinder = GetComponent<Pathfinder>();
        charMove = GetComponent<CharacterMovement>();
    }

    void Start()
    {
        player = PlayerCharacter.Get();
        playerChar = player.GetComponent<PlayerCharacter>();
        playerTriggerRadius = playerTrigger.GetComponent<CircleCollider2D>().radius;
        enemyRenderers = GetComponentsInChildren<Renderer>();

        if (shootTarget)
        {
            projectileAttackScript = projectileAttackObject.GetComponent<EnemyProjectileAttack>();
        }
    }

    bool IsVisible()
    {
        // if any renderer is visible
        foreach (Renderer r in enemyRenderers)
        {
            if (r.isVisible)
            {
                return true;
            }
        }

        return false;
    }

    IEnumerator AggroCooldown()
    {
        while (true)
        {
            //Debug.Log("Started waiting.");
            yield return new WaitForSeconds(aggroCooldownTime);

            if (gameObject) { StopAggro(); }
        }
    }

    public void StartAggro()
    {

        //Debug.Log("Trying to be aggro'd.");

        if (IsVisible() && !isAggrod)
        {
            OnAggro.Invoke();
            isAggrod = true;
            playerChar.AddEnemyToCombat();

            StartCoroutine(AggroCooldown());

            if (shootTarget)
            {
                projectileAttackScript.SetShootWhileEnabledHigh();
            }

            if (chaseTarget)
            {
                pathfinder.enabled = true;
                pathfinder.SetTarget(player);
            }
        }
    }

    public void StopAggro()
    {

        if (isAggrod)
        {
            OnStopAggro.Invoke();
            isAggrod = false;
            playerChar.RemoveEnemyFromCombat();
            //Debug.Log("Stopping Aggro.");

            if (shootTarget)
            {
                projectileAttackScript.SetShootWhileEnabledLow();
            }

            if (chaseTarget)
            {
                pathfinder.enabled = false;
                charMove.Move(Vector2.zero);
            }
        }
    }

    void Update()
    {
        if (!isAggrod && Vector2.SqrMagnitude(player.transform.position - transform.position) < Mathf.Pow(playerTriggerRadius, 2))
        {
            StartAggro();
        }
    }
}
