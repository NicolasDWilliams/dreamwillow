﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ProjectileEnemy : MonoBehaviour
{
    public float outerRadius = 10f;
    public float innerRadius = 5f;
    public float retreatTime = 100000000f;
    public float stopTime = 10f;

    GameObject player;
    CharacterMovement charMove;
    Pathfinder pathfinder;

    bool isAggroed = false;
    bool canRetreat = true;
    bool isRetreating = false;
    bool isAdvancing = false;
    bool isStuck = false;

    public UnityEvent OnStartRetreat;
    public UnityEvent OnStopRetreat;
    public UnityEvent OnStartAdvance;
    public UnityEvent OnStopAdvance;
    public UnityEvent OnStuckBegin;
    public UnityEvent OnStuckEnd;

    // Start is called before the first frame update
    void Start()
    {
        player = PlayerCharacter.Get();
        charMove = gameObject.GetComponent<CharacterMovement>();
        pathfinder = gameObject.GetComponent<Pathfinder>();

        OnStopAdvance.AddListener(StopAdvance);
        OnStopRetreat.AddListener(StopRetreat);

        OnStartRetreat.AddListener(SetCanRetreatHigh);
    }

    // Update is called once per frame
    void Update()
    {
        if (!isAggroed)
        {
            return;
        }

        float distanceToPlayer = Vector2.SqrMagnitude((Vector2)transform.position - (Vector2)player.transform.position);
        if (distanceToPlayer < innerRadius * innerRadius && canRetreat)
        {
            Retreat();
            if (!isRetreating)
            {
                OnStartRetreat.Invoke();
                Debug.Log("MAX");
            }
            isRetreating = true;
        }
        else if (distanceToPlayer > outerRadius * outerRadius)
        {
            Advance();
            if (!isAdvancing)
            {
                OnStartAdvance.Invoke();
                Debug.Log("NICO");
            }
            isAdvancing = true;
        }
        else
        {
            if (isAdvancing)
            {
                OnStopAdvance.Invoke();
                Debug.Log("AMBER");
            }
            else if (isRetreating)
            {
                OnStopRetreat.Invoke();
                Debug.Log("GEORGE");
            }

            isAdvancing = false;
            isRetreating = false;
        }
    }

    public void StartAggro()
    {
        isAggroed = true;
    }

    public void StopAggro()
    {
        isAggroed = false;
    }

    LayerMask GetLayersToConsider(bool shouldIncludePlayer)
    {
        if (shouldIncludePlayer)
        {
            return ~LayerMask.GetMask("Default", "Trigger", "Camera", "Companion", "Corpse", "Enemy", "Aggro Trigger");
        }
        return ~LayerMask.GetMask("Default", "Trigger", "Camera", "Companion", "Corpse", "Player", "Enemy", "Aggro Trigger");
    }

    void Retreat()
    {
        Vector2 transformToPlayer = player.transform.position - transform.position;
        Vector2 movementVector = (Vector2)CrossProduct(transformToPlayer, Vector3.forward);
        if (transformToPlayer.sqrMagnitude < innerRadius * innerRadius * 0.81f)
        {
            movementVector -= transformToPlayer.normalized * (innerRadius / 6f);
        }
        Debug.DrawLine(transform.position, (Vector2)transform.position + movementVector, Color.red);

        LayerMask layersToConsider = GetLayersToConsider(false);
        RaycastHit2D hitFromRaycast = Physics2D.Raycast(transform.position, movementVector, charMove.GetSpeed() * Time.deltaTime * 1.4f, layersToConsider);
        if (hitFromRaycast.collider == null)
        {
            charMove.Move(movementVector);
            if (isStuck)
            {
                OnStuckEnd.Invoke();
            }
            isStuck = false;
        }
        else
        {
            Vector2 directionToPlayer = player.transform.position - transform.position;
            float raycastMagnitude = outerRadius - directionToPlayer.magnitude;
            hitFromRaycast = Physics2D.Raycast(transform.position, -directionToPlayer, raycastMagnitude, layersToConsider);
            if (hitFromRaycast.distance > 0.4f || hitFromRaycast.collider == null)
            {
                charMove.Move(-directionToPlayer);
                if (isStuck)
                {
                    OnStuckEnd.Invoke();
                }
                isStuck = false;
            }
            else
            {
                charMove.Move(Vector2.zero);
                if (!isStuck)
                {
                    OnStuckBegin.Invoke();
                }
                isStuck = true;
            }
        }
    }

    void StopRetreat()
    {
        charMove.Move(Vector2.zero);
    }

    void Advance()
    {
        pathfinder.targetPosition = player.transform;
        pathfinder.enabled = true;
    }

    void StopAdvance()
    {
        pathfinder.enabled = false;
    }

    Vector2 UnitCircleFromAngle(float angle)
    {
        return new Vector2(Mathf.Cos(angle), Mathf.Sin(angle));
    }

    Vector3 CrossProduct(Vector3 a, Vector3 b)
    {
        return new Vector3((a.y * b.z) - (a.z * b.y), (a.z * b.x) - (a.x * b.z), (a.x * b.y) - (a.y * b.x));
    }

    void SetCanRetreatHigh()
    {
        canRetreat = true;
        Invoke("SetCanRetreatLow", retreatTime);
    }
    
    void SetCanRetreatLow()
    {
        if(!isRetreating)
        {
            canRetreat = true;
        }
        else
        {
            canRetreat = false;
            Invoke("SetCanRetreatHigh", stopTime);
        }
    }
}

