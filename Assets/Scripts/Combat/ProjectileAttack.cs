﻿/* Shoots a projectile in the local right direction, restricted by a cooldown
 * @Max Perraut
 * 
 * Edited by CheWei Wang 10/18/2019
 * Edited by Zhang Zhenyuan 11/23/2019
 * Edited by Matthew Rader 12/9/2019 (Added aim assist)
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;


public class ProjectileAttack : MonoBehaviour
{
    // BASIC PARAMS
    public int damage = 1;
    public float cooldown = 0.5f;
    public float projectileSpeed = 5f;
	public float projectileSize = 1;

    // MULTISHOT PARAMS
    [Min(1)]
    public int numProjectiles = 1;
    [Tooltip("Angle of spread from aiming direction. No effect when numProjectiles is 1.")]
    public float spreadAngle = 360f;

    public GameObject projectilePrefab;

    [Tooltip("How far away from this transform that projectiles are spawned.")]
    public float spawnDistance;

    // AIM ASSIST
    [Header ("ONLY APPLIES TO THE PRIMARY ATTACK")]
    [Header ("Aim Assist Variables")]
    public BoolSettingVariable aimAssistSetting;
    [SerializeField] private bool useAimAssist;
    [SerializeField] private AimAssistSettings aimAssistController;
    [SerializeField] private AimAssistSettings aimAssistKeyboardMouse;
    [SerializeField] private LayerMask enemyLayerMask;

    [System.Serializable]
	public struct AimAssistSettings {
		public float numAimAssistRaycasts;
		public float aimAssistAngle;
    }

    ///<summary>
    ///The amount of cooldown time that is remaining, scaled to the range [1,0]
    ///</summary>
    public float CooldownLeft
    { 
        get
        {
            return Mathf.Clamp01((Time.time - cooldownEndTime) / cooldown);
        }
    }

    public UnityEngine.Events.UnityEvent OnShoot;

    private float cooldownEndTime = 0f;
    public float CooldownEndTime { get { return cooldownEndTime; } }

    /// <summary>
    /// Launch projectile(s) if the cooldown allows
    /// </summary>
    public virtual void Shoot()
    {
        if(Time.time > cooldownEndTime)
        {
            cooldownEndTime = Time.time + cooldown;

            OnShoot.Invoke();

            if(numProjectiles == 1)
            {
                useAimAssist = (aimAssistSetting != null) ? aimAssistSetting.toggle : true;
                Vector2 direction = useAimAssist ? ApplyAimAssist(transform.right) : (Vector2)transform.right;
                SpawnProjectile(direction, projectileSpeed);
            }
            else
            {
                SpawnMultiple();
            }
        }
       
    }
    
    protected void SpawnProjectile(Vector2 direction, float speed)
    {
        GameObject spawnedProjectile = Instantiate(projectilePrefab);

		//Set size
		spawnedProjectile.transform.localScale = new Vector3(projectileSize, projectileSize, 1);
        //Set transform and velocity
        spawnedProjectile.transform.position = transform.position + (Vector3)(direction * spawnDistance * projectileSize);
        spawnedProjectile.transform.right = direction;
        spawnedProjectile.GetComponent<Rigidbody2D>().velocity = direction * speed;

        //Set damage
        spawnedProjectile.GetComponent<DamageOnTrigger>().damage = damage;
    }

    private void SpawnMultiple()
    {
        Vector3 direction = Quaternion.AngleAxis(-spreadAngle/2, Vector3.forward) * transform.right;

        Quaternion rotationBetween = Quaternion.AngleAxis(spreadAngle / numProjectiles, Vector3.forward);
        for (int i = 0; i < numProjectiles; i++)
        {
            SpawnProjectile(direction, projectileSpeed);
            direction = rotationBetween * direction;
        }
    }

    // Create a FOV of raycasts spaced out by aimAssistAngle/numAimAssistRaycasts, 
    // find the nearest Enemy hit to the aim direction vector, then set the direction 
    // Vector to equal the adjusted direction to the enemy
    // Matthew Rader 12/9/19
    private Vector2 ApplyAimAssist(Vector2 direction) {
        float aimAssistAngle;
        float numAimAssistRaycasts;

        Player player = ReInput.players.GetPlayer(0);
        Controller currentDevice = player.controllers.GetLastActiveController();
        if (currentDevice.type == ControllerType.Joystick) {
            aimAssistAngle = aimAssistController.aimAssistAngle;
            numAimAssistRaycasts = aimAssistController.numAimAssistRaycasts;
        }
        else {
            aimAssistAngle = aimAssistKeyboardMouse.aimAssistAngle;
            numAimAssistRaycasts = aimAssistKeyboardMouse.numAimAssistRaycasts;
        }

        //Debug.DrawRay(transform.position, direction * 2, Color.blue, 5.0f);

        // Set the starting point of the aimAssit FOV
        Vector2 raycastVec = Quaternion.AngleAxis(aimAssistAngle/2, Vector3.forward) * direction;

        // Find the rotaion between every raycast
        Quaternion rotationBetween = Quaternion.AngleAxis(-aimAssistAngle/numAimAssistRaycasts, Vector3.forward);

        // The vector to the neemy closest to the direction Vector
        Vector2 directionToBestEnemy = direction;

        // Can be any angle greater than the aimAssitAngle, set to 360 just becuase.
        float closestEnemyHitAngle = 360.0f;

        for (int i = 0; i < numAimAssistRaycasts; ++i) {
            Debug.DrawRay(transform.position, (raycastVec.normalized)*25, Color.red, 5.0f);
            RaycastHit2D hit = Physics2D.Raycast(transform.position, raycastVec.normalized, 25.0f, enemyLayerMask);

            if (hit) {
                Vector2 toEnemyVec = hit.transform.position-transform.position;

                float enemyToFireDirectionAngle = direction.magnitude / (toEnemyVec).magnitude;

                if (enemyToFireDirectionAngle < closestEnemyHitAngle) {
                    closestEnemyHitAngle = enemyToFireDirectionAngle;
                    directionToBestEnemy = toEnemyVec;
                }
            }

            raycastVec = rotationBetween * raycastVec;
        }

        // Adjusted firing vector
        Vector2 adjustedDirection = directionToBestEnemy.normalized;

        return adjustedDirection;
    }
}

