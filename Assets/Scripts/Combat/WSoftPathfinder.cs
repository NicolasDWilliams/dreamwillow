﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

/*
 * An enum representing what mode the Pathfinder is in.
 *      Straight - Goes directly to the target as quickly as possible
 *      Nothing - Don't do anything
 *      Random - Goes randomly to a point on the map
 */
public enum PathfinderMode
{
    Straight,
    Nothing,
    Random,
    Bounded,
};

/*
 * Note that this class is called WSoft Pathfinder because all other good names were taken
 * This class calculates all the enemies neccessary character movement
 * Written by Nikhil Ghosh
 */
 [RequireComponent(typeof(Seeker))]
public class WSoftPathfinder : MonoBehaviour
{
    public bool debugMode = false; // Should debug statements print out
    public bool efficientPathfinding = true; // Should pathfinding use a time-efficient algorithm
    public PathfinderMode currentMode; // The current mode of the pathfinder

    [Tooltip("Where the enemy is supposed to go")]
    public Transform target; // The target (only for Straight)
    public bool continousUpdate = false; // Should the path continously update

    public float radius = 0f; // in what radius should a random point be obtained (set to 0 for a random point on the map)
    public float delayTime = 0f;

    public Collider2D boundingCollider;

    Path currentPath = null; // the path that the enemy will take to its next objective

    Seeker seeker; // the seeker component of the enemy
    Rigidbody2D rb; // the rigidbody component of the enemy
    Animator animator; // the animator component of the enemy
    CharacterMovement charMove;

    int currentWaypoint = 0; // the current waypoint index
    bool reachedEndOfPath = true; // has the enemy reached the end of the path
    bool pathCalculating = false; // is a path currently being calculated
    AstarPath aStar;

    PathfinderMode previousMode; // the mode that the pathfinder was on in the last frame. This is to check if the mode changed

    Vector2 velocity = Vector2.zero; // the velocity that the enemy is travelling at
    
    // Start is called before the first frame update
    // Find all the necessary components
    void Start()
    {
        seeker = gameObject.GetComponent<Seeker>();
        if(seeker == null)
        {
            Debug.LogError("Enemy " + gameObject.name + " does not have the necessary seeker component. Please add a seeker component to this object");
        }
        rb = gameObject.GetComponent<Rigidbody2D>();
        animator = gameObject.GetComponent<Animator>();
        if(animator == null)
        {
            animator = gameObject.GetComponentInChildren<Animator>();
        }

        aStar = (AstarPath)GameObject.FindObjectOfType(typeof(AstarPath));
        charMove = gameObject.GetComponent<CharacterMovement>();

        previousMode = currentMode;

        if(target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(seeker == null)
        {
            return;
        }

        UpdatePath();

        // Did the mode change between last frame and this one?
        if(HasSwitchedModes())
        {
            SwitchModes();
        }
        previousMode = currentMode;

        switch (currentMode)
        {
            // Pathfinding for Straight mode
            case PathfinderMode.Straight:
                StraightPath();
                break;
            case PathfinderMode.Random:
                RandomPath();
                break;
            case PathfinderMode.Bounded:
                BoundedPath();
                break;
        }

    }

    // Move the enemy a fixed velocity every frame
    void FixedUpdate()
    {
        //transform.Translate(velocity * Time.fixedDeltaTime);
    }

    // Returns the calculated velocity that the enemy needs to move at
    public Vector2 GetVelocity()
    {
        return velocity.normalized;
    }

    // Returns the "VectorPath", a list of all the points the enemy will need to go to. Returns null if there is no path.
    public List<Vector3> GetVectorPath()
    {
        if(currentPath == null)
        {
            return null;
        }
        return currentPath.vectorPath;
    }

    // Returns a string representing the current mode of the pathfinder
    public string GetCurrentModeString()
    {
        return GetModeString(currentMode);
    }

    // Stops waiting for a new path to be calculated. Note that this doesn’t actually stop path from calculating, just continues with the path it already has.
    public void StopPathCalculation()
    {
        pathCalculating = false;
    }

    // Recalculate all movement (intended for use if an additional parameter, like target, changes)
    public void Reset()
    {
        currentWaypoint = 0;
        reachedEndOfPath = true;
        pathCalculating = false;
        currentPath = null;
        
        if(debugMode)
        {
            Debug.Log("Enemy Movement Reset");
        }

        switch(currentMode)
        {
            case PathfinderMode.Straight:
                CalculateStraight();
                break;
            case PathfinderMode.Random:
                CalculateRandom();
                break;
            case PathfinderMode.Bounded:
                CalculateBounded();
                break;
        }
    }

    // Called when the path has completed calculations (i.e. the enemy actually has a path).
    void OnPathComplete(Path p)
    {
        if(!p.error)
        {
            currentPath = p;
            //currentPath.Claim(this);
            currentWaypoint = 0;
            reachedEndOfPath = false;
            if(debugMode)
            {
                Debug.Log("Path found with the following points");
                for (int i = 0; i < currentPath.vectorPath.Count; i++)
                {
                    Debug.Log("Waypoint " + i.ToString() + ": (" + currentPath.vectorPath[i].x + ", " + currentPath.vectorPath[i].y + ", " + currentPath.vectorPath[i].z + ")");
                }
                Debug.Log("Starting Path now");
            }
        }
        else
        {
            if (debugMode)
            {
                Debug.Log("Error in Pathfinding.");
            }
        }
        pathCalculating = false;
        if(isRandomBased(currentMode))
        {
            float timeToDestination = charMove.GetTimeForDistance(p.GetTotalLength());
            Invoke("Reset", timeToDestination + 1.2f);
        }
    }

    // Called when a new path has been recalculated
    void OnPathRecalculate(Path newPath)
    {
        if(!newPath.error)
        {
            // If the current path is null, this new path is automatically the current one
            if(currentPath == null)
            {
                currentPath = newPath;
            }

            // If there are less than three points on the new path, do nothing with it.
            else if(newPath.vectorPath.Count <= 3)
            {
                if(debugMode)
                {
                    Debug.Log("Path Found Too Short, Recalculation Aborted");
                }
            }

            // If there are less than three points on the current path, set the new path automatically equal to it.
            else if(currentPath.vectorPath.Count <= 3)
            {
                currentPath = newPath;
                if(debugMode)
                {
                    Debug.Log("Path Recalculated");
                }
            }

            // If the path should normally recalculate itself, do this
            else
            {
                float currentPathAngle = Vector2.SignedAngle((Vector2)transform.position, (Vector2)currentPath.vectorPath[0]);
                float newPathAngle = Vector2.SignedAngle((Vector2)transform.position, (Vector2)newPath.vectorPath[0]);
                // If the enemy doesn't need to make a significant turn, merge the paths to make it smooth.
                if (Mathf.Abs(currentPathAngle - newPathAngle) < 60)
                {
                    List<Vector3> newVectorPath = new List<Vector3>();
                    for(int i = 0; i < newPath.vectorPath.Count; i++)
                    {
                        if(i < 3)
                        {
                            newVectorPath.Add(currentPath.vectorPath[i]);
                        }
                        else
                        {
                            newVectorPath.Add(newPath.vectorPath[i]);
                        }
                    }
                    currentPath.vectorPath = newVectorPath;
                }
                // If the enemy needs to move to a new path immediately (because of a significant turn), do so.
                else
                {
                    currentPath = newPath;
                }

                if(debugMode)
                {
                    Debug.Log("Path Recalculated");
                }
            }
        }
        else
        {
            if(debugMode)
            {
                Debug.Log("Error In Recalculation");
            }
        }
    }
    
    // Figures out how the system should go about calculating paths
    void UpdatePath()
    {
        if(!continousUpdate)
        {
            return;
        }
        if(debugMode)
        {
            Debug.Log("Calculating New Path - " + GetCurrentModeString());
        }

        switch(currentMode)
        {
            case PathfinderMode.Straight:
                RecalculateStraight();
                break;
            case PathfinderMode.Random:
                RecalculateRandom();
                break;
            case PathfinderMode.Bounded:
                RecalculateBounded();
                break;
        }
    }

    // Returns a string corresponding to the mode that the enemy is in
    static string GetModeString(PathfinderMode mode)
    {
        switch(mode)
        {
            case PathfinderMode.Nothing:
                return "Nothing";
            case PathfinderMode.Straight:
                return "Straight";
            case PathfinderMode.Random:
                return "Random";
            case PathfinderMode.Bounded:
                return "Bounded";
        }

        return "";
    }

    // True if the mode is based on randomness, false otherwise
    // The Random and Bounded modes are based on random pathfinding.
    static bool isRandomBased(PathfinderMode mode)
    {
        if(mode == PathfinderMode.Random || mode == PathfinderMode.Bounded)
        {
            return true;
        }
        return false;
    }

    // returns true if the mode on the last frame is different than the mode on this frame
    bool HasSwitchedModes()
    {
        return previousMode != currentMode;
    }

    // When the enemy switches modes, this function will reset everything to what it needs to be.
    void SwitchModes()
    {
        switch(previousMode)
        {
            case PathfinderMode.Nothing:
                reachedEndOfPath = true;
                currentWaypoint = 0;
                break;
            case PathfinderMode.Straight:
                currentPath = null;
                break;
            case PathfinderMode.Random:
                pathCalculating = false;
                currentPath = null;
                break;
            case PathfinderMode.Bounded:
                pathCalculating = false;
                currentPath = null;
                break;
        }

        switch (currentMode)
        {
            case PathfinderMode.Nothing:
                currentPath = null;
                velocity = Vector2.zero;
                charMove.Move(Vector2.zero);
                break;
            case PathfinderMode.Straight:
                CalculateStraight();
                currentWaypoint = 0;
                reachedEndOfPath = false;
                break;
            case PathfinderMode.Random:
                currentWaypoint = 0;
                reachedEndOfPath = false;
                break;
            case PathfinderMode.Bounded:
                currentWaypoint = 0;
                reachedEndOfPath = false;
                break;
        }

        if(debugMode)
        {
            Debug.Log("Switched from " + GetModeString(previousMode) + " to " + GetModeString(currentMode));
        }
    }

    // Move along the predefined path
    void MoveAlongPath()
    {
        if (Time.deltaTime <= 0.001f)
        {
            return;
        }

        Vector2 direction = (Vector2)currentPath.vectorPath[currentWaypoint] - (Vector2)transform.position;
        velocity = direction;
        if(charMove != null)
        {
            charMove.Move(direction);
        }

        if (Vector2.Distance((Vector2)currentPath.vectorPath[currentWaypoint], (Vector2)transform.position) <  charMove.GetSpeed() * Time.deltaTime)
        {
            if (debugMode)
            {
                Debug.Log("Waypoint " + currentWaypoint.ToString() + " passed.");
            }
            currentWaypoint++;
        }
        else if(Vector2.SqrMagnitude((Vector2)currentPath.vectorPath[currentWaypoint] -  (Vector2)transform.position) < 0.01f)
        {
            if (debugMode)
            {
                Debug.Log("Waypoint " + currentWaypoint.ToString() + " passed.");
            }
            currentWaypoint++;
        }
    }

    // Selects a random node in the entire graph
    Vector2 RandomNodeInGraph(NavGraph graph)
    {
        if(graph is GridGraph)
        {
            GridGraph gridGraph = (GridGraph)graph;
            int randX = (int)Mathf.Floor(Random.Range(0, gridGraph.width));
            int randY = (int)Mathf.Floor(Random.Range(0, gridGraph.depth));
            int randIndex = randX + randY * gridGraph.depth;
            return (Vector2)((Vector3)gridGraph.nodes[randIndex].position);
        }
        return Vector2.zero;
    }

    // Selects a random node in the graph less than the radius from the position.
    Vector2 RandomNodeInGraph(NavGraph graph, Vector2 position, float radius)
    {
        if(graph is GridGraph)
        {
            GridGraph gridGraph = (GridGraph)graph;
            Vector2 randPosition = Random.insideUnitCircle * radius + position;
            NNConstraint constraint = new NNConstraint();
            constraint.constrainWalkability = true;
            constraint.walkable = true;
            NNInfoInternal nodeInfo = gridGraph.GetNearest((Vector3)randPosition, constraint);
            return (Vector2)((Vector3)nodeInfo.node.position);
        }
        return Vector2.zero;
    }

    // Selects a random node from the 0th graph of the aStar prefab.
    Vector2 RandomNodeInGraph(Vector2 position, float radius)
    {
        return RandomNodeInGraph(aStar.data.graphs[0], position, radius);
    }

    //Selects a random point in a 2D collider.
    Vector2 RandomPointInCollider(Collider2D collider)
    {
        if (collider is BoxCollider2D)
        {
            BoxCollider2D boxCollider = (BoxCollider2D)collider;
            Bounds boxBounds = boxCollider.bounds;
            float randX = Random.Range(-boxBounds.extents.x, boxBounds.extents.x);
            float randY = Random.Range(-boxBounds.extents.y, boxBounds.extents.y);
            return new Vector2(randX, randY) + (Vector2)boxBounds.center;
        }
        if (collider is CapsuleCollider2D)
        {
            CapsuleCollider2D capsuleCollider = (CapsuleCollider2D)collider;
            Bounds capsuleBounds = capsuleCollider.bounds;
            while (true)
            {
                float randX = Random.Range(0, capsuleBounds.size.x);
                float randY = Random.Range(0, capsuleBounds.size.y);
                Vector2 randPoint = new Vector2(randX, randY) + (Vector2)capsuleBounds.center;
                if (capsuleCollider.OverlapPoint(randPoint))
                {
                    return randPoint;
                }
            }
        }
        if (collider is CircleCollider2D)
        {
            CircleCollider2D circleCollider = (CircleCollider2D)collider;
            return circleCollider.radius * Random.insideUnitCircle + (Vector2)circleCollider.bounds.center;
        }
        if (collider is PolygonCollider2D)
        {
            PolygonCollider2D polygonCollider = (PolygonCollider2D)collider;
            Bounds capsuleBounds = polygonCollider.bounds;
            while (true)
            {
                float randX = Random.Range(0, capsuleBounds.size.x);
                float randY = Random.Range(0, capsuleBounds.size.y);
                Vector2 randPoint = new Vector2(randX, randY) + (Vector2)capsuleBounds.center;
                if (polygonCollider.OverlapPoint(randPoint))
                {
                    return randPoint;
                }
            }
        }
        return Vector2.zero;
    }

    /*
     * PathfinderMode - Straight:
     *      moves to a set location on the map (specified by target) in the most direct way possible. If not possible, the enemy will not move
     * Written by Nikhil Ghosh
     */

    // Executes straight path
    void StraightPath()
    {
        // Checks if movement should be executed

        if(reachedEndOfPath && !pathCalculating)
        {
            CalculateStraight();
            return;
        }

        if(currentPath == null)
        {
            return;
        }

        if (currentWaypoint >= currentPath.vectorPath.Count)
        {
            if(debugMode)
            {
                Debug.Log("End of Path Reached");
            }
            reachedEndOfPath = true;
            return;
        }

        reachedEndOfPath = false;

        if(efficientPathfinding)
        {
            CanGoDirectlyToTarget();
        }

        MoveAlongPath();
    }

    // Begin calculations of a straight path
    void CalculateStraight()
    {
        if(pathCalculating)
        {
            return;
        }
        if(efficientPathfinding)
        {
            if(CanGoDirectlyToTarget())
            {
                return;
            }
        }
        if (target != null)
        {
            seeker.StartPath(transform.position, target.position, OnPathComplete);
            pathCalculating = true;
        }
    }

    // Begins recalculations of a straight path
    void RecalculateStraight()
    {
        if (target != null)
        {
            if (efficientPathfinding)
            {
                if (CanGoDirectlyToTarget())
                {
                    return;
                }
            }
            seeker.StartPath(transform.position, target.position, OnPathRecalculate);
        }
    }

    // Aborts going on a straight path
    void AbortStraight()
    {
        currentPath = null;
    }

    // Returns true if it can go directly to target through raycast, false if otherwise.
    bool CanGoDirectlyToTarget()
    {
        LayerMask layersToConsider = aStar.data.gridGraph.collision.mask;
        Vector2 directionToTarget = (Vector2)target.transform.position - (Vector2)transform.position;
        RaycastHit2D hitFromRaycast = Physics2D.Raycast(transform.position, directionToTarget.normalized, directionToTarget.magnitude - 0.5f, layersToConsider);
        //Debug.DrawLine(transform.position, (Vector2)transform.position + directionToTarget, Color.cyan, 0.5f, false);
        if (hitFromRaycast.collider == null || hitFromRaycast.collider.tag == transform.tag)
        {
            List<Vector3> newPath = new List<Vector3>();
            newPath.Add(target.transform.position);
            currentPath = ABPath.FakePath(newPath);
            //currentPath.Claim(this);
            return true;
        }
        return false;
    }

    // Returns true if a pathfinder near this enemy has a path to the target.
    bool DoesOtherEnemyHavePath()
    {
        WSoftPathfinder[] allPathfinders = GameObject.FindObjectsOfType<WSoftPathfinder>();
        //Debug.Log(allPathfinders.Length);
        if (allPathfinders.Length <= 1)
        {
            return false;
        }
        List<WSoftPathfinder> enemyPathfinders = new List<WSoftPathfinder>();
        List<WSoftPathfinder> companionPathfinders = new List<WSoftPathfinder>();
        for (int i = 0; i < allPathfinders.Length; i++)
        {
            if (allPathfinders[i].gameObject.tag == "Enemy")
            {
                enemyPathfinders.Add(allPathfinders[i]);
            }
            else if (allPathfinders[i].gameObject.tag == "Companion")
            {
                companionPathfinders.Add(allPathfinders[i]);
            }
        }
        
        if (enemyPathfinders.Count <= 1 && gameObject.tag == "Enemy")
        {
            return false;
        }
        else if (companionPathfinders.Count <= 1 && gameObject.tag == "Companion")
        {
            return false;
        }
        List<WSoftPathfinder> compatiblePathfinders = new List<WSoftPathfinder>();
        if(gameObject.tag == "Enemy")
        {
            compatiblePathfinders.AddRange(enemyPathfinders);
        }
        else if(gameObject.tag == "Companions")
        {
            compatiblePathfinders.AddRange(compatiblePathfinders);
        }
        for(int i = 0; i < compatiblePathfinders.Count; i++)
        {
            if(compatiblePathfinders[i].target == target)
            {
                List<Vector3> compatiblePath = compatiblePathfinders[i].GetVectorPath();
                if(compatiblePath != null)
                {
                    Vector2 directionToTarget = (Vector2)compatiblePathfinders[i].transform.position - (Vector2)transform.position;
                    RaycastHit2D hitFromRaycast = Physics2D.Raycast(transform.position, directionToTarget.normalized, directionToTarget.magnitude - 0.5f, LayerMask.GetMask(new string[] { "Water", "Player", "Walls" }));
                    if(hitFromRaycast.collider == null || hitFromRaycast.transform == compatiblePathfinders[i].transform)
                    {
                        List<Vector3> newVectorPath = new List<Vector3>();
                        newVectorPath.Add(compatiblePathfinders[i].transform.position);
                        newVectorPath.AddRange(compatiblePath);
                        currentPath = ABPath.FakePath(newVectorPath);
                        //currentPath.Claim(this);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /*
     * PathMode - Nothing
     *      Does not move.
     *      Surprising that code needs to be involved for it to do nothing
     */

    void NothingPath()
    {
        /*
        if(charMove != null)
        {
            charMove.Move(Vector2.zero);
        }
        */
    }
    /*
     * PathMode - Random:
     *      moves to a random location on the map
     * Written by Nikhil Ghosh
     */

    // Executes random path
    void RandomPath()
    {
        // Checks if movement should be executed
        if (pathCalculating)
        {
            return;
        }

        if (reachedEndOfPath && !pathCalculating)
        {
            pathCalculating = true;
            Invoke("CalculateRandom", delayTime);
            return;
        }

        if (currentPath == null)
        {
            return;
        }

        if (currentWaypoint >= currentPath.vectorPath.Count)
        {
            if (debugMode)
            {
                Debug.Log("End of Path Reached");
            }
            reachedEndOfPath = true;
            CancelInvoke("Reset");
            return;
        }

        reachedEndOfPath = false;

        MoveAlongPath();
    }

    // Begins calculations for a random mode.
    void CalculateRandom()
    {
        Vector2 randomPoint = Vector2.zero;
        if(radius < 0.001)
        {
            randomPoint = RandomNodeInGraph(aStar.data.graphs[0]);
        }
        else if(target != null)
        {
            randomPoint = RandomNodeInGraph(aStar.data.graphs[0], target.transform.position, radius);
        }
        else
        {
            randomPoint = RandomNodeInGraph(aStar.data.graphs[0], transform.position, radius);
        }

        seeker.StartPath(transform.position, (Vector3)randomPoint, OnPathComplete);
        pathCalculating = true;
    }

    // Prints a debug message.
    void RecalculateRandom()
    {
        if(debugMode)
        {
            Debug.Log("Recalculation aborted, pathmode is Random");
        }
    }

    // Aborts the random path
    void AbortRandom()
    {
        currentPath = null;
    }

    /*
     * Pathmode - Bounded
     *      Uses a Collider2D to find random points and bound the enemy
     */

    void BoundedPath()
    {
        if(boundingCollider == null)
        {
            return;
        }
        // Checks if movement should be executed
        if (pathCalculating)
        {
            return;
        }

        if (reachedEndOfPath && !pathCalculating)
        {
            pathCalculating = true;
            Invoke("CalculateBounded", delayTime);
            return;
        }

        if (currentPath == null)
        {
            return;
        }

        if (currentWaypoint >= currentPath.vectorPath.Count)
        {
            if (debugMode)
            {
                Debug.Log("End of Path Reached");
            }
            reachedEndOfPath = true;
            CancelInvoke("Reset");
            return;
        }

        reachedEndOfPath = false;

        MoveAlongPath();
    }

    // Calculates a path for the bounded mode
    void CalculateBounded()
    {
        Vector2 randomPoint = RandomPointInCollider(boundingCollider);
        seeker.StartPath(transform.position, (Vector3)randomPoint, OnPathComplete);
        pathCalculating = true;
    }

    // Prints a debug message
    void RecalculateBounded()
    {
        if(debugMode)
        {
            Debug.Log("Recalculation Aborted. Pathmode is Bounded");
        }
    }

    // Aborts the bounded mode.
    void AbortBounded()
    {
        currentPath = null;
    }
}