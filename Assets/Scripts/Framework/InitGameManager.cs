﻿/* Creates game manager once at runtime.
* @Max Perraut
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitGameManager : MonoBehaviour
{
    public GameObject gameManager;

    void Awake()
    {
        if(GameManager.Instance == null)
        {
            Instantiate(gameManager);
            Debug.Log("Initialized Game Manager.");
        }

        Destroy(gameObject);
    }
}
