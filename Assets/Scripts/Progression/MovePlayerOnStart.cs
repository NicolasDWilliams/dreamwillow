﻿/* Moves the player character to this location on Awake
* @Max Perraut
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayerOnStart : MonoBehaviour
{
    public UnityEngine.Events.UnityEvent OnMovePlayer;

    private void Start()
    {
        Transform playerTransform = PlayerCharacter.Get()?.transform;
        if (playerTransform)
        {
            playerTransform.position = transform.position;
            OnMovePlayer.Invoke();
        }
    }
}
