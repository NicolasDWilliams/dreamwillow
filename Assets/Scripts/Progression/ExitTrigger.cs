﻿/* Exits the scene when triggered by the player
* @Max Perraut
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitTrigger : MonoBehaviour
{
    [Tooltip("Set to true if this scene is a shop.")]
    public bool sceneIsShop = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (sceneIsShop)
            {
                collision.gameObject.GetComponent<PlayerInteract>().SetInteractRange(1);
                GameManager.ExitShop();
            }
            else
            {
                collision.gameObject.GetComponent<PlayerInteract>().SetInteractRange(15);
                GameManager.ExitLevel();
            }
        }
    }
}
