﻿/* Spawns the player's companions at this location on Awake
* @Alex Kisil
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCompanionsOnAwake : MonoBehaviour
{
    public GameObject companionPrefab;

    // Start is called before the first frame update
    void Start()
    {
        CompanionManager companionManager = PlayerCharacter.Get()?.gameObject.GetComponent<CompanionManager>();
        for (int i = 0; i < companionManager.getNumCompanions(); i++)
        {
            GameObject companion = Instantiate(companionPrefab, transform.position + new Vector3(i * .01f, 0), Quaternion.identity) as GameObject;
            CompanionController companionController = companion.GetComponent<CompanionController>();
            companionController.innerRadius = companionController.innerRadius + (companionManager.getNumCompanions() * .2f);
            companionController.SetFollowRadius(companionController.GetFollowRadius() + (companionManager.getNumCompanions() * .2f));
        }
    }
}
