﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSCounter : MonoBehaviour
{
    public float fps = 0f;
    public float averageFPS = 0f;

    float[] deltaTimes = new float[100];

    void Update()
    {
        fps = 1 / Time.unscaledDeltaTime;
        float totalfps = 0f;
        for (int i = 1; i < Mathf.Min(100, deltaTimes.Length); i++)
        {
            deltaTimes[i - 1] = deltaTimes[i];
            totalfps += deltaTimes[i - 1];
        }
        deltaTimes[Mathf.Min(99, deltaTimes.Length - 1)] = Time.unscaledDeltaTime;
        averageFPS = totalfps / Mathf.Min(100, deltaTimes.Length);
    }
}
