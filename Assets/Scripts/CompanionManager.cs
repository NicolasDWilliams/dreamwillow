﻿/** 
 * Handle management of companions list.
 * @Alex Kisil
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CompanionManager : MonoBehaviour
{

    // INTERNAL REFERENCES
    Health playerHealth;

    // EVENTS
    public UnityEvent OnCompanionSacrifice;
    public UnityEvent OnTrueDeath;

    // STATE
    private int numCompanions;

    public void addCompanion()
    {
        numCompanions++;
    }

    public int getNumCompanions()
    {
        return numCompanions;
    }

    private void Awake()
    {
        playerHealth = gameObject.GetComponent<Health>();
    }

    // Disable companion and heal player to full.
    // If no companions exist, returns false
    public bool AttemptCompanionSacrifice()
    {
        GameObject[] companions = GameObject.FindGameObjectsWithTag("Companion");

        if (companions.Length > 0)
        {
            OnCompanionSacrifice.Invoke();
            companions[0].gameObject.SetActive(false);
            playerHealth.Heal(playerHealth.max);
            numCompanions--;
            this.GetComponent<PlayerInventory>().CollarDestroy(1);

            return true;
        }

        OnTrueDeath.Invoke();
        return false;
    }
}
