﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompanionFormation : MonoBehaviour
{
    public List<Vector3> formation(Vector3 pos, float radius, int num_comp, bool shift, int rot = 0){
        List<Vector3> comp_locats = new List<Vector3>();
        float angle = 360 / num_comp;
        //evenly space positions around center
        for (int i = 0; i < num_comp; i++) {
            float dir = angle * i;
            float x_pos = Mathf.Cos(dir) * radius + pos.x;
            float y_pos = Mathf.Cos(dir) * radius + pos.y;
            //shift each position slightly by random
            if (shift) {
                x_pos = Random.Range(-0.1f, 0.1f);
                x_pos = Random.Range(-0.1f, 0.1f);
            }
            Vector3 comp_locat = new Vector3(x_pos, y_pos, 0);
            comp_locats.Add(comp_locat);
        }
        return comp_locats;
    }
}