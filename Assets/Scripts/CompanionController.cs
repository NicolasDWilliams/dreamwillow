﻿/** 
 * Basic companion behavior, common across all enemies.
 * @Alex Kisil
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CompanionController : MonoBehaviour
{
    // INTERNAL REFERENCES
    CharacterMovement charMove;
    Pathfinder pathfinder;
    public GameObject playerTrigger;
    public GameObject enemyTrigger;

    // PARAMETERS
    [SerializeField] Vector2 moveVec = Vector2.zero;
    [SerializeField] bool chaseTarget = false;
    [SerializeField] bool moveRandomlyNearPlayer = false;
    [SerializeField] bool stopWhileAttacking = false;
    [SerializeField] bool followLeaderWhileAttacking = false;
    [SerializeField] float timeAlllowedToWander = 0f;
    //[SerializeField] float outerRadius = 4f;
    [SerializeField] public float innerRadius = 1f;

    // EVENT DEFINITIONS
    [System.Serializable]
    public class VectorEvent : UnityEvent<Vector2> { }

    // EVENTS
    public UnityEvent OnCombatStart;
    public UnityEvent OnCombatEnd;
    public VectorEvent InCombat;

    // EXTERNAL REFERENCES
    GameObject target;
    [SerializeField] GameObject leader;

    // INTERNAL STATE
    private bool inPursuit { get { return target != null; } }
    private bool awayFromPlayer = false;
    private Vector2 targetVec = Vector2.zero;
    private float outerRadius;
    private float enemyRadius;

    private void Awake()
    {
        charMove = GetComponent<CharacterMovement>();
        outerRadius = playerTrigger.GetComponent<CircleCollider2D>().radius;
        enemyRadius = enemyTrigger.GetComponent<CircleCollider2D>().radius;
        pathfinder = GetComponent<Pathfinder>();
    }

    private void Start()
    {
        leader = PlayerCharacter.Get();
        pathfinder.targetPosition = leader.transform;

    }

    public float GetFollowRadius()
    {
        return outerRadius;
    }

    public void SetFollowRadius(float newRadius)
    {
        playerTrigger.GetComponent<CircleCollider2D>().radius = newRadius;
        outerRadius = newRadius;
    }

    public void SetAwayFromPlayerHigh()
    {
        awayFromPlayer = true;
    }

    public void SetAwayFromPlayerLow()
    {
        awayFromPlayer = false;
    }

    public void ReturnToLeader()
    {
        pathfinder.targetPosition = leader.transform;
        target = null;
    }

    void TargetNearestEnemy()
    {
        if (target != null)
        {
            if (!target.activeInHierarchy)
            {
                target = null;
            }
            else if (target.transform != pathfinder.targetPosition && GameObjectInRange(target))
            {
                pathfinder.targetPosition = target.transform;
            }
            else if (!GameObjectInRange(target))
            {
                target = null;
            }
            return;
        }

        GameObject foundEnemy = FindNearestEnemy();
        if (foundEnemy != null)
        {
            target = foundEnemy;
            pathfinder.targetPosition = target.transform;
        }
    }

    bool IsEnemyInRange()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        foreach (GameObject enemy in enemies)
        {
            if (Vector2.SqrMagnitude(enemy.transform.position - leader.transform.position) < outerRadius * outerRadius)
            {
                return true;
            }
        }
        return false;
    }

    bool GameObjectInRange(GameObject tempObject)
    {
        if (Vector2.SqrMagnitude(tempObject.transform.position - leader.transform.position) < outerRadius * outerRadius)
        {
            return true;
        }
        return false;
    }

    GameObject FindNearestEnemy()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");

        float minimumDistance = float.MaxValue;
        GameObject minimumEnemy = null;
        foreach (GameObject enemy in enemies)
        {
            if (Vector2.SqrMagnitude(enemy.transform.position - leader.transform.position) < outerRadius * outerRadius)
            {
                if (Vector2.SqrMagnitude(enemy.transform.position - leader.transform.position) < minimumDistance)
                {
                    minimumDistance = Vector2.SqrMagnitude(enemy.transform.position - transform.position);
                    minimumEnemy = enemy;
                }
            }
        }
        return minimumEnemy;
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector2.SqrMagnitude(transform.position - leader.transform.position) > outerRadius * outerRadius)
        {
            pathfinder.enabled = true;

            if (pathfinder.targetPosition != leader.transform)
            {
                pathfinder.SetTarget(leader);
            }
        }
        else if (Vector2.SqrMagnitude(transform.position - leader.transform.position) < innerRadius * innerRadius)
        {
            if (IsEnemyInRange())
            {
                pathfinder.enabled = true;
                TargetNearestEnemy();
            }
            else
            {
                pathfinder.enabled = false;
                charMove.Move(Vector2.zero);
            }
        }
        else
        {
            if (IsEnemyInRange())
            {
                pathfinder.enabled = true;
                TargetNearestEnemy();
            }
        }
    }


}

