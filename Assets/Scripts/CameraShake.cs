﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraShake : MonoBehaviour
{
	[SerializeField] private CinemachineVirtualCamera vcam;
	CinemachineBasicMultiChannelPerlin noise;

	public CameraShakeSettings onDamage;
	public CameraShakeSettings onShoot;

	[System.Serializable]
	public struct CameraShakeSettings {
		public float cameraShakeDuration;
		public float cameraShakeAmount;
	}

	public FloatSettingVariable cameraShakeSetting;

	private void Awake () {
		noise = vcam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
	}

	public void ApplyCameraShakeOnDamage () {
		StartCoroutine(DoCameraShake(onDamage));
	}

	public void ApplyCameraShakeOnShoot () {
		StartCoroutine(DoCameraShake(onShoot));
	}

	public IEnumerator DoCameraShake (CameraShakeSettings shakeSettings) {
		noise.m_AmplitudeGain = noise.m_FrequencyGain = shakeSettings.cameraShakeAmount * cameraShakeSetting.multiplier;
		yield return new WaitForSeconds(shakeSettings.cameraShakeDuration);
		noise.m_AmplitudeGain = noise.m_FrequencyGain = 0;
	}
}
