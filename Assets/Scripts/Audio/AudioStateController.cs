﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AudioStateController : MonoBehaviour
{
    public string stateGroup;

    // Start is called before the first frame update
    public void Change_State(string in_state)
    {
        AkSoundEngine.SetState(stateGroup, in_state);
    }
}