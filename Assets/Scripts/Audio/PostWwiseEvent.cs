﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostWwiseEvent : MonoBehaviour
{
    public void PostEvent(string name)
    {
        AkSoundEngine.PostEvent(name, gameObject);
    }
}