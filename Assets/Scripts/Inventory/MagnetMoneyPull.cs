﻿using UnityEngine;

/// <summary>
/// Moves soul toward player. The actual collection is handled by MoneyPickup
/// </summary>
public class MagnetMoneyPull : MonoBehaviour
{
    public float speed = 12.0f;

	private bool isMovingTowardPlayer;

    private Collider2D playerCollider;

	/// <summary>
	/// The circle collider ("magnetic field") on this child gameobject
	/// </summary>
	/// <param name="other">Other.</param>
	private void OnTriggerEnter2D(Collider2D other)
	{
		// if player enters magnetic field, lerp soul towards it
		if (!isMovingTowardPlayer && other.GetComponent<PlayerInventory>() != null)
		{
			isMovingTowardPlayer = true;
            playerCollider = other;
            // disable the magnetic collection trigger field; not needed anymore
            gameObject.GetComponent<CircleCollider2D>().enabled = false; 
		}
	}

	private void FixedUpdate()
	{
		// smooth root gameobject toward player
        if (isMovingTowardPlayer)
        {
            Vector2 targetPosition = playerCollider.gameObject.transform.position;
            Transform parent = GetComponentInParent<MoneyPickup>().gameObject.transform;

            parent.position = Vector2.MoveTowards(parent.position, targetPosition, speed * Time.deltaTime);
        }
	}
}