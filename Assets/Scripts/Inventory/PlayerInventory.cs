﻿// Inventory system for the player. Each item is a int variable, and should be initialized as 0. Add new
// int variables as more items are developed.
// Seyhyun Yang(seyhyun@umich.edu)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Health))]
public class PlayerInventory : MonoBehaviour
{
    //PARAMETERS
    public int potionHealAmount = 5;

    //Wrap all events in a struct to group them in Editor
    [System.Serializable]
    public struct InventoryEvents
    {
        [System.Serializable]
        public class MoneyValueEvent : UnityEvent<int> { }

        [System.Serializable]
        public class CollarsValueEvent : UnityEvent<int> { }

        [System.Serializable]
        public class PotionValueEvent : UnityEvent<int> { }

        // Event that stores the new currency total
        [Tooltip("Invoked when money changes. It is passed the value of money after the change.")]
        public MoneyValueEvent OnMoneyChange;

        // Event that stores the new collars total
        [Tooltip("Invoked when collars changes. It is passed the value of collars after the change.")]
        public CollarsValueEvent OnCollarsChange;

        // Event that stores the new potion total
        [Tooltip("Invoked when potion count changes. It is passed the value of potion after the change.")]
        public MoneyValueEvent OnPotionCountChange;

        public UnityEvent OnHealthPotionUse;
    }

    public InventoryEvents events;

    // Prevent potions from being used inside store.
    public bool inShop = false;

    // Money variable
    public int Money { get {return _money;} }
    [SerializeField]
    private int _money = 0;

    // Health Potion variable
    public int HealthPotions { get {return _healthPotions;} }
    [SerializeField]
    private int _healthPotions = 0;
    private Health playerHealth;

    // Collar variable
    public int Collars { get { return _collars; } }
    [SerializeField]
    private int _collars = 0;

    public int collarsUsed = 0;

    // Max Collar count variable
    public int CollarsMax  = 3;



    private void Awake() {
        playerHealth = GetComponent<Health>();

    }

    private void Start()
    {
        // RESET COLLAR COUNT on LEVEL LOAD
        // (this is temporary -> TODO: remove this once collars can actually drop)
        //GameManager.Events.OnLevelLoaded.AddListener(delegate{CollarSet(3);});
    }

    private void OnDestroy(){
        GameManager.Events.OnLevelLoaded.RemoveListener(delegate{CollarSet(3);});
    }

    // Increment Money count by given amount when function is called.
    public void MoneyGain(int amount)
    {
        _money += amount;
        events.OnMoneyChange.Invoke(Money);
    }

    // Decrement money count by given amount when function is called.
    public void MoneyLose(int amount)
    {
        _money -= amount;
        events.OnMoneyChange.Invoke(Money);
    }

    // Set collars to certain count
    public void CollarSet(int count){
        _collars = count;
        events.OnCollarsChange.Invoke(Collars);
    }

    // Increment Collars count by given amount when function is called.
    public void CollarGain(int amount)
    {
        _collars += amount;
        events.OnCollarsChange.Invoke(Collars);
    }

    // Decrement money count by given amount when function is called.
    public void CollarLose(int amount)
    {
        collarsUsed += amount;
        //_collars -= amount;
        events.OnCollarsChange.Invoke(Collars);
    }

    public void CollarDestroy(int amount)
    {
        _collars -= amount;
        collarsUsed = (collarsUsed - amount < 0 ? 0 : collarsUsed - amount);
        CollarsMax -= amount;
        events.OnCollarsChange.Invoke(Collars);
    }

    // TODO: Add functions that increment different items as item types are added.

    public void HealthPotionGain(int amount)
    {
        _healthPotions += amount;
        events.OnPotionCountChange.Invoke(HealthPotions);

    }

    public void UseHeathPotion(){
		if (HealthPotions > 0 && playerHealth.Current < playerHealth.max){
            _healthPotions--;
            playerHealth.Heal(potionHealAmount);
            events.OnHealthPotionUse.Invoke();
            events.OnPotionCountChange.Invoke(HealthPotions);
        }
    }

}
