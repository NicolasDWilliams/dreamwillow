﻿// Script for the shopkeeper. On collision opens up shop menu.
// Seyhyun Yang(seyhyun@umich.edu)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopKeeper : MonoBehaviour
{
    GameObject Store;
    PlayerInventory Player;
    // Creates pointer to shop menu.
    private void Start()
    {
        Player = PlayerCharacter.Get().GetComponent<PlayerInventory>();
    }
    // Opens up shop menu on collision with shopkeeper.
    private void OnCollisionEnter2D(Collision2D other)
    {
        Store.SetActive(true);
    }
    // Closes shop menu on moving away from shopkeeper.
    private void OnCollisionExit2D(Collision2D other)
    {
        Store.SetActive(false);
    }

    /// <summary>
    /// TODO: Replace this test placeholder with an actual "buy potion" key and interface
    /// </summary>
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Log("~debug potion purchase~");
            BuyPotion();
        }
    }

    // Buy potion. If money is sufficient, reduce money and add potion.
    public void BuyPotion()
    {
        if (Player.GetComponent<PlayerInventory>().Money >= 2)
        {
            Player.MoneyLose(2);
            Player.HealthPotionGain(1);
        }
    }

}
