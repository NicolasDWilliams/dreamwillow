﻿using UnityEngine;

/// <summary>
/// Attached to the soul; the player collects it on collision
/// </summary>
public class MoneyPickup : MonoBehaviour
{
    /// <summary>
    /// Get collected if the player touches this soul
    /// </summary>
    /// <param name="other">Other.</param>
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PlayerInventory>() != null)
        {
            other.GetComponent<PlayerInventory>().MoneyGain(1);
            Destroy(gameObject);
        }
    }

}