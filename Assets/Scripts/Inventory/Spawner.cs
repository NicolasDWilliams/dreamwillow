﻿/* Spawns a random number of objects within a radius. Randomly distributed.
 * - Max
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject spawn;
    //public float radius;
    //public int minSpawns;
    //public int maxSpawns;

    public void Spawn()
    {
		Instantiate(spawn, transform.position, Quaternion.identity);
		//int count = Random.Range(minSpawns, maxSpawns);
		//for (int i = 0; i < count; i++) {
		//	Vector3 spawnPosition = transform.position + (Vector3)(Random.insideUnitCircle * radius);

		//	Instantiate(spawn, spawnPosition, Quaternion.identity);
		//}
	}
}
