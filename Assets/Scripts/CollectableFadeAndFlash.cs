﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Experimental.Rendering.LWRP;

public class CollectableFadeAndFlash : MonoBehaviour
{
	[SerializeField]
	private GameObject itemViewLight;

	[SerializeField]
	private GameObject itemView;

	[Header("Visual Despawn Type")]
	[SerializeField]
	private bool itemFade = true;
	[SerializeField]
	private bool itemFlash = true;

	[Header("Timers")]
	[SerializeField]
	private float preVisualDespawnDelayTimer = 2.0f;
	private float visualDespawnDelayCounter = 0.0f;

	[SerializeField]
	private float visualDespawnTimerLength = 5.0f;
	private float visualDespawnTimerCounter = 0.0f;

	[SerializeField]
	private float postVisualDespawnPaddingLength = 1.0f;
	private float postVDPCounter = 0.0f;

	[Header("Fade Variables")]
	[SerializeField]
	private float startingGlowIntensity = 0.4f;
	[SerializeField]
	private bool applyFadeToLight = true;
	//[SerializeField]
	//private bool applyFadeToSprite = true;

	[Header("Flash Rate variables")]
	[SerializeField]
	private float initialFlashRate = 1.0f;
	[SerializeField]
	private float fastestFlashRate = 0.25f;
	[SerializeField]
	private bool applyFlashToLight = true;
	[SerializeField]
	private bool applyFlashToSprite = true;

	private void Awake () {
		itemViewLight.GetComponent<Light2D>().intensity = startingGlowIntensity;
		StartCoroutine(DespawnTimer());
	}

	IEnumerator DespawnTimer () {
		#region Pre-Visual Fade / Flash Despawn Phase
		while (visualDespawnDelayCounter < preVisualDespawnDelayTimer) {
			visualDespawnDelayCounter += Time.deltaTime;
			yield return null;
		}
		#endregion

		#region Visual Fade / Flash Despawn Phase
		float t = 0.0f;
		float flashRate = initialFlashRate;
		float flashCounter = 0.0f;
		Light2D itemLight = itemViewLight.GetComponent<Light2D>();
		//SpriteRenderer itemSR = itemViewLight.GetComponent<SpriteRenderer>();

		while (visualDespawnTimerCounter < visualDespawnTimerLength) {
			t += Time.deltaTime / visualDespawnTimerLength;
			visualDespawnTimerCounter += Time.deltaTime;

			if (itemFade) {
				if (applyFadeToLight) {
					itemLight.intensity = Mathf.Lerp(startingGlowIntensity, 0.0f, t);
				}

				// if (applyFadeToSprite) {
				// 	Color tmp = itemSR.color;
				// 	tmp.a = Mathf.Lerp(1, 0, t);
				// 	itemSR.color = tmp;
				// }
			}

			if (itemFlash) {
				if (flashCounter >= flashRate) {
					flashCounter = 0.0f;

					if (applyFlashToSprite) {
						bool itemActiveState = itemView.activeSelf;
						itemView.SetActive(!itemActiveState);
					}

					if (applyFlashToLight) {
						itemLight.enabled = !itemLight.enabled;
					}
				}

				flashRate = Mathf.Lerp(initialFlashRate, fastestFlashRate, t);

				flashCounter += Time.deltaTime;
			}

			yield return null;
		}
		#endregion

		#region Post-Visual Despawn Phase
		// This is used to give the player a grace period where the pick up
		// is no longer visible but still attainable.
		while (postVDPCounter < postVisualDespawnPaddingLength) {
			postVDPCounter += Time.deltaTime;
			yield return null;
		}
		#endregion

		Destroy(gameObject);
	}
}
