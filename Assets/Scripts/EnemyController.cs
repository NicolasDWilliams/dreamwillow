﻿/** 
 * Basic enemy behavior, common across all enemies.
 * @Alex Kisil
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyController : MonoBehaviour
{
    // EVENT DEFINITIONS
    [System.Serializable]
    public class VectorEvent : UnityEvent<Vector2> { }

    // EVENTS
    public UnityEvent OnCombatStart;
    public UnityEvent OnCombatEnd;
    public VectorEvent InCombat;

    // EXTERNAL REFERENCES
    GameObject target;
    public GameEvent enterCombatEvent;
    public GameEvent endCombatEvent;
    public GameEvent whileInCombatEvent;

    // EXTERNAL STATE
    public GameObject getTarget { get { return target; } }

    // INTERNAL STATE
    private Vector2 targetVec = Vector2.zero;

    /// <summary>
    /// Give this enemy an object to pursue
    /// </summary>
    public void Target(GameObject newTarget)
    {
        if (target == null)
        {
            enterCombatEvent.Raise();
        }

        target = newTarget;
    }

    /// <summary>
    /// Stop pursuing a given object
    /// </summary>
    public void UnTarget(GameObject removeTarget)
    {
        if (target == removeTarget)
        {
            target = null;
            endCombatEvent.Raise();
        }
    }
}
