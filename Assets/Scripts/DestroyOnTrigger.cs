﻿/* Destroys gameobject on trigger enter.
 * Optionally instantiates hit particle prefab when destroyed.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnTrigger : MonoBehaviour
{
    public GameObject hitParticle;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(hitParticle) Instantiate(hitParticle, this.transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
