﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject MainMenuCanvas;
    public GameObject CreditsCanvas;
	public string sceneToLoadOnPlay = "Tutorial"; // Default is the tutorial
	public string sceneToLoadOnCredits = "Ending"; // Default is the ending

    public Button Play;
    public Button Credits;
    public Button Quit;
    public List<Button> MenuButtons;
    public Button ReturnToMenu;

    public GameObject PlaySelect;
    public GameObject CreditsSelect;
    public GameObject QuitSelect;
    public List<GameObject> MenuSelect;


    public bool isCredits;
    public bool hasChangedSelection;
    public float selection = 0;

    public void Start()
    {
        MenuButtons = new List<Button>() { Play, Credits, Quit };
        MenuSelect = new List<GameObject>() { PlaySelect, CreditsSelect, QuitSelect };
    }

    public void Update()
    {
        if (isCredits)
        {
            ReturnToMenu.Select();
        }
        else
        {
            //Debug.Log(GUI.GetNameOfFocusedControl());
            if (Input.GetAxisRaw("Vertical") == 0)
            {
                hasChangedSelection = false;
            }
            else if (hasChangedSelection == false)
            {
                // Disable old item selector
                MenuSelect[(int)selection].SetActive(false);
                // Get new selection
                selection -= Input.GetAxisRaw("Vertical");
                if (selection < 0) { selection = MenuSelect.Count - 1; }
                if (selection > MenuSelect.Count - 1) { selection = 0; }
                MenuButtons[(int)selection].Select();
                Debug.Log(MenuButtons[(int)selection].name);
                hasChangedSelection = true;
                MenuSelect[(int)selection].SetActive(true);
            }
        }
    }

    public void PlayButton()
    {
        GameManager.StartGame(sceneToLoadOnPlay);
	}

	public void PlayButtonRandom () {
		SceneManager.LoadScene("Lab_RandomGeneration");
	}

	public void CreditsButton()
    {
        /*
        MainMenuCanvas.SetActive(false);
        CreditsCanvas.SetActive(true);
        ReturnToMenu.Select();
        isCredits = true;
        */
        SceneManager.LoadScene(sceneToLoadOnCredits);
    }

    public void QuitButton()
    {
        Application.Quit();
    }

    public void ReturnToMenuButton()
    {
        MainMenuCanvas.SetActive(true);
        CreditsCanvas.SetActive(false);
        Play.Select();
        isCredits = false;
    }
}
