﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Experimental.Rendering.LWRP;

public class CoolDownVisual : MonoBehaviour 
{
	// These values are grabbed right before the effect starts.
	private Color defaultColor;
	private float defaultIntensity;

	[SerializeField]
	private Color disabledColor;

	[SerializeField]
	private float disabledIntensity;

	[SerializeField]
	private Light2D affectedLight;

	public UnityEvent visualStart;
	public UnityEvent visualEnd;

	public void InvokeEffect (float cooldownTime) {
		StartCoroutine(CooldownEffect(cooldownTime));
	}

	IEnumerator CooldownEffect (float coolDownTime) {
		visualStart.Invoke();

		defaultColor = affectedLight.color;
		defaultIntensity = affectedLight.intensity;

		// Set light to the disabled color and intensity
		affectedLight.color = disabledColor;
		affectedLight.intensity = disabledIntensity;

		float t;
		float counter;

		#region disabledColor to defaultColor fade
		t = 0.0f;
		counter = 0.0f;

		while (counter < coolDownTime) {

			t += Time.deltaTime / coolDownTime;
			counter += Time.deltaTime;

			affectedLight.color = Color.Lerp(disabledColor, defaultColor, t);
			affectedLight.intensity = Mathf.Lerp(disabledIntensity, defaultIntensity, t);

			yield return null;
		}
		#endregion

		visualEnd.Invoke();

		#region end PingPong up
		t = 0.0f;
		counter = 0.0f;
		float outerRadius = affectedLight.pointLightOuterRadius;

		while (counter < 0.08f) {

			t += Time.deltaTime / 0.08f;
			counter += Time.deltaTime;

			affectedLight.pointLightOuterRadius = Mathf.Lerp(outerRadius, outerRadius + 0.9f, t);

			yield return null;
		}
		#endregion

		#region end PingPong down
		t = 0.0f;
		counter = 0.0f;

		while (counter < 0.08f) {

			t += Time.deltaTime / 0.08f;
			counter += Time.deltaTime;

			affectedLight.pointLightOuterRadius = Mathf.Lerp(outerRadius + 0.9f, outerRadius, t);

			yield return null;
		}
		#endregion
	}
}
