﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.LWRP;

public class DimGlobalLights : MonoBehaviour
{
	enum dimTypeEnum { toMinIntensity, toMaxIntensity };

	[SerializeField]
	private dimTypeEnum dimType;

	[SerializeField]
	private float minIntensity = 0.5f;

	[SerializeField]
	private float maxIntensity = 1.0f;

	[SerializeField]
	private float dimDuration = 2.0f;

	[SerializeField]
	Light2D light;

	private bool currentlyDimming = false;

	private void OnTriggerEnter2D (Collider2D collision) {
		if (collision.tag == "Player") {
			if (dimType == dimTypeEnum.toMaxIntensity) {
				LightToMaxIntensity();
			}
			else {
				LightToMinIntensity();
			}
		}
	}

	public void LightToMaxIntensity () {
		if (light.intensity == minIntensity && !currentlyDimming) {
			StartCoroutine(FadeCanvasAlpha(minIntensity, maxIntensity));
		}
	}

	public void LightToMinIntensity () {
		if (light.intensity == maxIntensity && !currentlyDimming) {
			StartCoroutine(FadeCanvasAlpha(maxIntensity, minIntensity));
		}
	}

	IEnumerator FadeCanvasAlpha (float intensityFrom, float intensityTo) {
		currentlyDimming = true;

		float t = 0.0f;
		while (t < dimDuration) {
			t += Time.deltaTime / dimDuration;
			light.intensity = Mathf.Lerp(intensityFrom, intensityTo, t);

			yield return null;
		}

		currentlyDimming = false;
	}
}
