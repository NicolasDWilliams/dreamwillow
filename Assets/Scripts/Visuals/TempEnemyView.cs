﻿/** 
 * Enemy View script, turns enemies to face player 
 * and plays attack animation if in range
 * @George Castle
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TempEnemyView : MonoBehaviour
{
    [System.Serializable]
    public class PathFindingMoveEvent : UnityEvent<Vector2> { }
    [Tooltip("Called every frame that move velocity changes.")]

    public PathFindingMoveEvent OnMove;
    public PathFindingMoveEvent OnStop;
    public float sensitivity;
    Vector2 lastPos;

    bool isMoving = false;

    private void FixedUpdate()
    {
        Vector2 position = new Vector2(transform.position.x, transform.position.y);
        if ((Mathf.Abs(position.x - lastPos.x) > sensitivity) || (Mathf.Abs(position.y - lastPos.y) > sensitivity))
        {
            OnMove.Invoke((position - lastPos));
            lastPos = position;
            isMoving = true;
        }
        else if (isMoving == true && (Mathf.Abs(position.x - lastPos.x) < sensitivity) && (Mathf.Abs(position.y - lastPos.y) < sensitivity))
        {
            isMoving = false;
            OnStop.Invoke(Vector2.zero);
        }
    }
}
