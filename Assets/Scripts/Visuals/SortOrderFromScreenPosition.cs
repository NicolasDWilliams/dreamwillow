﻿/* Sets sprite renderers' sort order based on screen Y position. */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortOrderFromScreenPosition : MonoBehaviour
{

    public SpriteRenderer[] renderers;

    private int[] originalSortOrders;

    //Sprite sort order must be between -32768 and 32767
    // add buffer of 100 on each end for offset from original order
    private const int MIN_SORT = -32668;
    private const int MAX_SORT = 32667;

    private void Reset() {
        renderers = GetComponentsInChildren<SpriteRenderer>();
    }

    private void Start() {
        originalSortOrders = new int[renderers.Length];
        for (int i = 0; i < renderers.Length; i++)
        {
            originalSortOrders[i] = renderers[i].sortingOrder;
        }
    }

    private void LateUpdate() {

        //Calculate screen position
        float screenYPos = Camera.main.WorldToScreenPoint(transform.position).y;

        //Scale screen position to a [0,1] Range.
        screenYPos /= Camera.main.pixelHeight;

        //Calculate sort order from Y pos (lower Y gives higher sort order)
        int order = (int)Mathf.Lerp(MAX_SORT, MIN_SORT, screenYPos);

        for (int i = 0; i < renderers.Length; i++)
        {
            renderers[i].sortingOrder = order + originalSortOrders[i];
        }
    }
}
