﻿/* Matches this gameObject's position and rotation values to another's.
 * -Max
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewFollow : MonoBehaviour
{
    public Transform follow;

    public bool useWorld = false;

    private void LateUpdate()
    {
        if (useWorld)
        {
            transform.position = follow.position;
            transform.rotation = follow.rotation;
        }
        else
        {
            transform.localPosition = follow.localPosition;
            transform.localRotation = follow.localRotation;
        }
    }
}
