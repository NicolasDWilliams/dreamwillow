﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CharacterView : MonoBehaviour
{
    private Animator myAnim;

    Vector2 lastDir = new Vector2(0f, -1f);

    [SerializeField] float moveSensitivity;

    private bool attackWhileEnabled;

    void Start()
    {
        myAnim = gameObject.GetComponent<Animator>();
        myAnim.SetFloat("moveX", lastDir.x);
        myAnim.SetFloat("moveY", lastDir.y);
    }

    private void Update() {
        if (attackWhileEnabled) {
            playAttackAnimation();
        }
    }

    public void SetAttackWhileEnabledLow()
    {
        attackWhileEnabled = false;
    }

    public void SetAttackEnabledHigh()
    {
        attackWhileEnabled = true;
    }

    public void playAttackAnimation () {
        myAnim.SetTrigger("Attack");
    }


    public void updateAnimator(Vector2 moveDir)
    {
        if (!GameManager.gamePaused) {
        if (moveDir.x < -moveSensitivity)
        {
            transform.localScale = new Vector3(1f, 1f, 1);
        }
        if (moveDir.x > moveSensitivity)
        {
            transform.localScale = new Vector3(-1f, 1f, 1);
        }

        myAnim.SetBool("IsMoving", false);
        bool moveHoriz = false;
        if (Mathf.Abs(moveDir.x) > Mathf.Abs(moveDir.y)) {
            moveHoriz = true;
        }

        if (Mathf.Abs(moveDir.x) > moveSensitivity && moveHoriz) {
            lastDir = new Vector2(RoundToOne(moveDir.x), 0);
            myAnim.SetBool("IsMoving", true);
        }
        if (Mathf.Abs(moveDir.y) > moveSensitivity && !moveHoriz) {
            transform.localScale = new Vector3(1f, 1f, 1);
            lastDir = new Vector2(0, RoundToOne(moveDir.y));
            myAnim.SetBool("IsMoving", true);
        }

        myAnim.SetFloat("moveX", lastDir.x);
        myAnim.SetFloat("moveY", lastDir.y);
        }
        
    }

    float RoundToOne(float input) {
        if (input < 0) {
            return -1;
        }
        if (input > 0) {
            return 1;
        }
        return 0;
    }
}
