﻿/**Uses player input to control the UI and communicate with the GameManager
 * Formerly UIPauseToggle
 * @Author Zena Abulhab, Max Perraut
 */

using UnityEngine;
using Rewired;

public class UIControl : MonoBehaviour
{
    // The game and pause canvas children GameObjects
    [SerializeField]
    private GameObject gameCanvas;
    [SerializeField]
    private GameObject pauseCanvas;
    [SerializeField]
    private GameObject gameOverCanvas;
    [SerializeField]
    private GameObject settingsCanvas;
    private bool gameOver;

    private Rewired.Player player;

    private void Start()
    {
        player = ReInput.players.GetPlayer(0);
        player.AddInputEventDelegate(HandlePauseInput, UpdateLoopType.Update, InputActionEventType.ButtonJustPressed, "Pause");

        GameManager.Events.OnGameOver.AddListener(GameOver);

    }

    private void OnDestroy()
    {
        player.RemoveInputEventDelegate(HandlePauseInput);
    }

    public void HandlePauseInput(InputActionEventData _)
    {
        if (!gameOver)
        {
            if (GameManager.gamePaused)
            {
                if (!settingsCanvas.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("idle")) {
                    UnPause();
                }
                else {
                    closeSettings();
                }
            }
            else
            {
                Pause();
            }
        }
    }



    public void Pause()
    {
        GameManager.PauseGame();
        ShowPauseUI();
    }

    public void UnPause()
    {
        GameManager.UnpauseGame();
        HidePauseUI();
    }

    /// <summary>
    /// Enables the canvas component of the PauseCanvas to set it visible
    /// </summary>
    private void ShowPauseUI()
    {
        pauseCanvas.GetComponent<Animator>().SetTrigger("open");
        gameCanvas.SetActive(false);
    }



    /// <summary>
    /// Disables the canvas component of the PauseCanvas to hide it
    /// </summary>
    private void HidePauseUI()
    {
        gameCanvas.SetActive(true);
        pauseCanvas.GetComponent<Animator>().SetTrigger("close");
    }

    public void GameOver()
    {
        gameCanvas.SetActive(false);
        gameOver = true;
        gameOverCanvas.GetComponent<Animator>().SetTrigger("open");
    }
    
    public void closeSettings() {
        settingsCanvas.GetComponent<Animator>().SetTrigger("close");
    }

}
