﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeCanvasGroup : MonoBehaviour 
{
	enum fadeType {fadeIn, fadeOut};

	[SerializeField]
	private fadeType fadeEnum;

	[SerializeField]
	private float minAlpha = 0.0f;

	[SerializeField]
	private float maxAlpha = 1.0f;

	[SerializeField]
	private float fadeDuration = 2.0f;

	[SerializeField]
	CanvasGroup canvas;

	private bool currentlyFading = false;

	private void OnTriggerEnter2D (Collider2D collision) {
		if (collision.tag == "Player") {
			if (fadeEnum == fadeType.fadeIn) {
				FadeCanvasGroupIn();
			}
			else {
				FadeCanvasGroupOut();
			}
		}
	}

	public void FadeCanvasGroupIn () {
		if (canvas.alpha == minAlpha && !currentlyFading) {
			StartCoroutine(FadeCanvasAlpha(minAlpha, maxAlpha));
		}
	}

	public void FadeCanvasGroupOut () {
		if (canvas.alpha == maxAlpha && !currentlyFading) {
			StartCoroutine(FadeCanvasAlpha(maxAlpha, minAlpha));
		}
	}

	IEnumerator FadeCanvasAlpha (float alphaFrom, float alphaTo) {
		currentlyFading = true;

		float t = 0.0f;
		while (t < fadeDuration) {
			t += Time.deltaTime / fadeDuration;
			canvas.alpha = Mathf.Lerp(alphaFrom, alphaTo, t);

			yield return null;
		}

		currentlyFading = false;
	}
}
