﻿/**
 * Updates the active game view health UI(eg hearts) when events are triggered
 * Attached to the HealthBar child gameobject of the UI2 canvas
 * @Author Zena Abulhab
 */

 // TODO: This script assumes that the amount of heart gameObjects in the children
 // is same as the max number of hearts defined in Health.
 // Therefore, for best results, keep these the same.
 // Boundary-checking might be added in the future

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class UIHealthUpdater : MonoBehaviour
{
    // Player health reference
    private Health health;

    // The full and empty heart sprites
    [SerializeField]
    private Sprite fullHeartSprite;
    [SerializeField]
    private Sprite emptyHeartSprite;
    [SerializeField]
    private Sprite noneHeartSprite;

    // List of transforms of all hearts in order
    private List<RectTransform> hearts;

    // Start is called before the first frame update
    void Start()
    {
        // Initialize hearts transform list with all hearts in children
        // Assumes they are returned in order they are listed in editor
        hearts = new List<RectTransform>();
        foreach (RectTransform heart in
            gameObject.GetComponentInChildren<RectTransform>(true))
        {
            hearts.Add(heart);
        }

        // Find game object with player tag and get its Health script instance
        GameObject player = PlayerCharacter.Get();
        health = player.GetComponent<Health>();

        // Subscribe to health change event and alter UI if event detected
        health.events.OnHealthChange.AddListener(UpdateHeartsDisplayed);

        UpdateHeartsDisplayed(health.Current);
    }

    /// <summary>
    /// Updates the number of hearts showing in the active game view UI
    /// </summary>
    /// <param name="newHealth">the new health value</param>
    void UpdateHeartsDisplayed(int newHealth)
    {
        int newHealthInt = newHealth;

        // Make first [newHealth] hearts be full hearts
        for (int i = 0; i < newHealthInt; i++)
        {
            ChangeHeartToFull(hearts[i]);
        }

        // Make any remaining hearts empty
        for (int i = newHealthInt; i < health.max; i++)
        {
            ChangeHeartToEmpty(hearts[i]);
        }

        // Remove sprites for hearts beyond max
        for (int i = health.max; i < hearts.Count; i++)
        {
            ChangeHeartToNone(hearts[i]);
        }
    }

    /// <summary>
    /// Makes a heart icon have the full heart sprite
    /// </summary>
    void ChangeHeartToFull(RectTransform heart)
    {
        heart.gameObject.GetComponent<Image>().sprite = fullHeartSprite;
    }

    /// <summary>
    /// Makes a heart icon have the empty heart sprite
    /// </summary>
    void ChangeHeartToEmpty(RectTransform heart)
    {
        heart.gameObject.GetComponent<Image>().sprite = emptyHeartSprite;
    }

    void ChangeHeartToNone(RectTransform heart)
    {
        heart.gameObject.GetComponent<Image>().sprite = noneHeartSprite;
    }

    /// <summary>
    /// Unsubscribe from health change event 
    /// </summary>
    void OnDestroy()
    {
        health.events.OnHealthChange.RemoveListener(UpdateHeartsDisplayed);
    }
}
