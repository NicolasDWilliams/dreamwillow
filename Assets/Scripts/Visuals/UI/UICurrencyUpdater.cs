﻿// Updates the active game view currency UI (eg coin count) when events are triggered
// Zena Abulhab

using UnityEngine;
using UnityEngine.UI;

public class UICurrencyUpdater : MonoBehaviour
{
    // Player inventory reference
    private PlayerInventory Inventory;

    // The money count UI element
    [SerializeField]
    private Text MoneyText;

    // Start is called before the first frame update
    void Start()
    {
        // Find game object with player tag and get its PlayerInventory script instance
        GameObject player = PlayerCharacter.Get();
        Inventory = player.GetComponent<PlayerInventory>();


        // Subscribe to inventory currency change events and alter UI if event detected
        Inventory.events.OnMoneyChange.AddListener(UIAddMoney);
        Inventory.events.OnMoneyChange.AddListener(UISubtractMoney);

        UpdateMoney();
    }

    private void UpdateMoney()
    {
        MoneyText.text = Inventory.Money.ToString();
    }

    /// <summary>
    /// Adds money in the UI display
    /// </summary>
    /// <param name="newMoneyCount">New money count.</param>
    private void UIAddMoney(int newMoneyCount)
    {
        MoneyText.text = Inventory.Money.ToString();
    }

    /// <summary>
    /// Subtracts money in the UI display
    /// </summary>
    /// <param name="newMoneyCount">New money count.</param>
    private void UISubtractMoney(int newMoneyCount)
    {
        MoneyText.text = Inventory.Money.ToString();
    }
}
