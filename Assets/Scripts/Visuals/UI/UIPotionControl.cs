﻿// Updates the active game view potion UI when events are triggered
// Zena Abulhab

using UnityEngine;
using UnityEngine.UI;

public class UIPotionControl : MonoBehaviour
{
    // Player inventory reference
    private PlayerInventory Inventory;

    // The potion count UI element
    [SerializeField]
    private Text PotionCountText;

    // Start is called before the first frame update
    void Start()
    {
        // Find game object with player tag and get its PlayerInventory script instance
        GameObject player = PlayerCharacter.Get();
        Inventory = player.GetComponent<PlayerInventory>();


        // Subscribe to inventory potion change events and alter UI if event detected
        Inventory.events.OnPotionCountChange.AddListener(UIAddPotion);
        Inventory.events.OnPotionCountChange.AddListener(UISubtractpotion);

        UpdatePotions();
    }

    private void UpdatePotions()
    {
        PotionCountText.text = Inventory.HealthPotions.ToString();
    }

    /// <summary>
    /// Adds potion in the UI display
    /// </summary>
    /// <param name="newPotionCount">New potion count.</param>
    private void UIAddPotion(int newPotionCount)
    {
        PotionCountText.text = Inventory.HealthPotions.ToString();
    }

    /// <summary>
    /// Subtracts potion in the UI display
    /// </summary>
    /// <param name="newpotionCount">New potion count.</param>
    private void UISubtractpotion(int newpotionCount)
    {
        PotionCountText.text = Inventory.HealthPotions.ToString();
    }
}
