﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIToggleValueChange : MonoBehaviour
{
    public BoolSettingVariable aimAssistSetting;

    [SerializeField]
    private Toggle menuAimAssistToggle;
    
    void Start()
    {
        bool startingValue = aimAssistSetting.toggle;
        menuAimAssistToggle.isOn = startingValue;
    }

    // This is called by the OnChange event called by the slider
    public void SetValue()
    {
       aimAssistSetting.toggle = menuAimAssistToggle.isOn;
    }
}
