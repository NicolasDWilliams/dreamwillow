﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingScreenController : MonoBehaviour
{
    private List<string> quotes = new List<string>();
    public Image image;
    public Text Quote;
    // Start is called before the first frame update
    void Start()
    {
        quotes.Add("Who in the world am I? Ah, that's the great puzzle...");
        quotes.Add("Imagination is the only weapon in the war against reality...");
        quotes.Add("No no! The adventure's first, explanations take such a dreadful time...");
        quotes.Add("I'm not strange, weird, off, nor crazy, my reality is just different from yours...");
        quotes.Add("If you don't know where you are going any road can take you anywhere...");
        quotes.Add("I wonder if I've been changed in the night. Let me think. Was I the same when I got up in the morning?");
        quotes.Add("I knew who I was this morning, but I've changed a few times since then...");
        quotes.Add("He was part of my dream, of course -- but then I was part of his dream too...");
        quotes.Add("How puzzling all these changes are! I'm never sure what I'm going to be from one minute to another...");
        quotes.Add("The hurrier I go, the behinder I get...");
        quotes.Add("It would be so nice if something made sense for a change...");
    }

    public void UpdateLoadingScreen(int round)
    {
        image.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, round / 6.0f);
        image.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, round / 6.0f);
        int quoteNum = Random.Range(1, quotes.Count - 1);
        Quote.text = quotes[quoteNum];
        quotes.RemoveAt(quoteNum);
    }
}
