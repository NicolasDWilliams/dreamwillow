﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UISliderValueAdjustment : MonoBehaviour
{
    public FloatSettingVariable vibrationSetting;

    [SerializeField]
    private TextMeshProUGUI sliderValueText = null;

    void Start()
    {
        float startingValue = vibrationSetting.multiplier;
        sliderValueText.text = startingValue.ToString();
        GetComponent<Slider>().value = startingValue;
    }

    // This is called by the OnChange event called by the slider
    public void SetValue()
    {
        float newValue = GetComponent<Slider>().value;
        sliderValueText.text = newValue.ToString();
        vibrationSetting.multiplier = newValue;
    }
}
