﻿// handles the ui message popups for 3d text objects (in world space) in the game
// Created by Nicolas Williams, 10/30/2019
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIPopupMessage : MonoBehaviour
{
    public float fadeTime = 0.2f;
    public float displayDelay = 0.1f;

    [SerializeField] private bool shouldStartHidden = true;

    private bool isFading = false;
    private bool isTyping = false;
    private TextMeshPro textMesh;
    private string fullTextString;
    [SerializeField]
    private SpriteRenderer[] childImageRenderers;
    private Coroutine fadeCR;
    private Coroutine typeCR;

    private void Awake()
    {
        textMesh = GetComponent<TextMeshPro>();
    }

    public virtual void Start()
    {
        childImageRenderers = GetComponentsInChildren<SpriteRenderer>();
        if (shouldStartHidden)
        {
            HideContent();
            //store the fulltext, since the contents will be changed by typewriter	
            fullTextString = textMesh.text;
        }
    }

    public void ShowMessageTypeWriter()
    {
        if (isFading)
        {
            StopCoroutine(fadeCR);
            isFading = false;
        }
        if (!isTyping)
        {
            typeCR = StartCoroutine(typeWriter());
        }
    }

    public virtual void ShowMessage()
    {
        if (isFading)
        {
            StopCoroutine(fadeCR);
            isFading = false;
        }
        fadeCR = StartCoroutine(FadeAlpha(1f));
    }


    public virtual void HideMessage()
    {
        if (isTyping)
        {
            isTyping = false;
            StopCoroutine(typeCR);
        }
        if (isFading)
        {
            StopCoroutine(fadeCR);
            isFading = false;
        }
        fadeCR = StartCoroutine(FadeAlpha(0f));
    }


    // crossfades the alpha value of the faceColor from the initial alpha to the newAlphaValue over fadeTime
    private IEnumerator FadeAlpha(float newAlphaValue)
    {
        // don't start this coroutine if already at target alpha
        if ((textMesh.faceColor.a / 255f) == newAlphaValue) yield break;
        isFading = true;

        // reading the alpha this way returns number between [0,255]
        float initialAlphaValue = textMesh.faceColor.a / 255f;
        Color nextColor = textMesh.faceColor;

        // being slowly moving towards the target alpha value
        for (float timeElapsed = 0f; timeElapsed <= fadeTime; timeElapsed += Time.deltaTime)
        {
            float newFloat = Mathf.Lerp(initialAlphaValue, newAlphaValue, timeElapsed / fadeTime);
            nextColor.a = newFloat;
            textMesh.faceColor = nextColor;
            SetChildAlphas(newFloat);
            yield return null;
        }

        nextColor.a = newAlphaValue;
        textMesh.faceColor = nextColor;
        isFading = false;
    }


    // sets the alpha of all child sprites to specified value
    private void SetChildAlphas(float newAlphaValue)
    {
        if (childImageRenderers == null)
            return;
        foreach (SpriteRenderer sprRend in childImageRenderers)
        {
            Color newColor = sprRend.color;
            newColor.a = newAlphaValue;
            sprRend.color = newColor;
        }
    }


    // hides content of popup message and all children instantly
    private void HideContent()
    {
        Color hideColor = textMesh.faceColor;
        hideColor.a = 0;
        textMesh.faceColor = hideColor;
        SetChildAlphas(0f);
    }

    //Display the texts character by character	
    private IEnumerator typeWriter()
    {
        fullTextString = textMesh.text;
        isTyping = true;
        Color nextColor = textMesh.faceColor;
        //set the alpha to 1f at the beginning of typing	
        nextColor.a = 1f;
        textMesh.faceColor = nextColor;
        //each time display a one letter more substring until full	
        for (int i = 0; i < fullTextString.Length; ++i)
        {
            textMesh.text = fullTextString.Substring(0, i + 1);
            yield return new WaitForSeconds(0.05f);
        }
        isTyping = false;
    }
}
