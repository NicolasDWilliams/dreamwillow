﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICorpsePopupMessage : UIPopupMessage
{
    PlayerInventory inv;
    private bool onCollarMax = false;

    public void Start()
    {
        base.Start();
        inv = PlayerCharacter.Get().GetComponent<PlayerInventory>();
    }

    private void Update()
    {
        if (inv.collarsUsed >= inv.Collars && onCollarMax == false)
        {
            onCollarMax = true;
            this.GetComponentInChildren<SpriteRenderer>().gameObject.SetActive(false);
            //base.HideMessage();
            //this.GetComponentInChildren<SpriteRenderer>().color = Color.clear;
        }
    }

    public override void ShowMessage()
    {
        if (inv.collarsUsed < inv.Collars)
        {
            base.ShowMessage();
        }
    }

    public override void HideMessage()
    {
        if (inv.collarsUsed < inv.Collars)
        {
            base.HideMessage();
        }
    }

}
