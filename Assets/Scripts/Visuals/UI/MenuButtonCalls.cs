﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButtonCalls : MonoBehaviour
{
    public string sceneToLoadOnPlay = "Tutorial"; // Default is the tutorial
	public string sceneToLoadOnCredits = "Ending"; // Default is the ending

    public void PlayButton()
    {
        GameManager.StartGame(sceneToLoadOnPlay);
	}

	public void CreditsButton()
    {
        GameManager.StartGame(sceneToLoadOnCredits);
    }

    public void QuitButton()
    {
        Application.Quit();
    }
}
