﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;

public class UINavigationEvents : MonoBehaviour, IMoveHandler, ISubmitHandler, ICancelHandler
{
    public UnityEvent MoveUp;
    public UnityEvent MoveDown;
    public UnityEvent MoveLeft;
    public UnityEvent MoveRight;

    public UnityEvent Submit;
    public UnityEvent Cancel;

    public void OnMove(AxisEventData data) {
        switch (data.moveDir)
        {
            case MoveDirection.Up:
                MoveUp.Invoke();
                break;
            case MoveDirection.Down:
                MoveDown.Invoke();
                break;
            case MoveDirection.Left:
                MoveLeft.Invoke();
                break;
            case MoveDirection.Right:
                MoveRight.Invoke();
                break;
        }
    }

    public void OnSubmit(BaseEventData data) {
        Submit.Invoke();
    }

    public void OnCancel(BaseEventData data) {
        Cancel.Invoke();
    }
}
