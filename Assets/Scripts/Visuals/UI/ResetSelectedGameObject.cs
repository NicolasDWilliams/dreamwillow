﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetSelectedGameObject : MonoBehaviour
{
    private UnityEngine.EventSystems.EventSystem eventSystem;

    private void Awake() {
        eventSystem = GetComponent<UnityEngine.EventSystems.EventSystem>();
    }
    private void Update() {
        if(!eventSystem.currentSelectedGameObject){
            eventSystem.SetSelectedGameObject(eventSystem.firstSelectedGameObject);
        }
    }
}
