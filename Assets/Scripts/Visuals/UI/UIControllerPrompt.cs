﻿    // handles displaying & updating controller prompt images based on active controller type
// Created by Nicolas Williams, 11/12/2019
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIControllerPrompt : MonoBehaviour
{
    public GameInformation gameInformation;
    public ControllerPrompt controllerPrompt;
    private SpriteRenderer rend;
    private Image uiImage;
    [Tooltip("Controller prompt is on a UI image, as opposed to a sprite renderer")]
    [SerializeField] private bool useUIImage = false;


    private void Awake()
    {
        if (useUIImage)
        {
            uiImage = GetComponent<Image>();
        }
        else
        {
            rend = GetComponent<SpriteRenderer>();
        }
    }

    // updates the controller prompt image based on what the active controller is
    public void UpdatePromptSprite()
    {
        if (useUIImage)
        {
            uiImage.sprite = controllerPrompt[gameInformation.activeDeviceType];
        }
        else
        {
            rend.sprite = controllerPrompt[gameInformation.activeDeviceType];
        }
    }

    private void OnEnable()
    {
        UpdatePromptSprite();
    }
}
