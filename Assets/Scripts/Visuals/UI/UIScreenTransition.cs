﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScreenTransition : MonoBehaviour
{
    [Tooltip("The duration of uncovering the screen")]
    public float uncoverDuration = 1f;

    [Tooltip("Should the transition speed be independent of the world timescale?")]
    [SerializeField] private bool shouldIgnoreTimescale = true;

    [Tooltip("If true, screen will fade in on start.")]
    [SerializeField] private bool uncoverOnStart = true;
    
    [Tooltip("Delay for uncovering when triggered by load or on Start.")]
    public float uncoverOnStartDelay = 1f;

    [Tooltip("Image that blankets the screen for the transition")]
    private Image blanketImage;

    private void Awake()
    {
        blanketImage = GetComponent<Image>();

        // set alpha of color to full
        // this script uses canvas renderer alpha, not image color alpha
        Color color = blanketImage.color;
        color.a = 1;
        blanketImage.color = color;
    }
    private void Start()
    {
        GameManager.Events.OnLevelTransition.AddListener(HideScreen);
        
        if(uncoverOnStart) ShowDelayed();
    }

    private void OnDestroy()
    {
        // remove self as listener from GM when destroyed
        GameManager.Events.OnLevelTransition.RemoveListener(HideScreen);
    }

    private void OnEnable()
    {
        // prevent alpha reset
        blanketImage.canvasRenderer.SetAlpha(0);   
    }

    /// <summary>
    /// Reveals the screen by making the screen blanket image transparent
    /// </summary>
    public void ShowScreen()
    {
        blanketImage.canvasRenderer.SetAlpha(1);
        blanketImage.CrossFadeAlpha(0f, uncoverDuration, shouldIgnoreTimescale);
    }

    ///<summary>
    /// Shows screen after the load delay
    ///</summary>
    public void ShowDelayed()
    {
        blanketImage.canvasRenderer.SetAlpha(1);
        Invoke(nameof(ShowScreen), uncoverOnStartDelay);
    }

    /// <summary>
    /// Hides the screen by making the screen blanket image opaque
    /// </summary>
    public void HideScreen(float transitionDuration)
    {
        blanketImage.canvasRenderer.SetAlpha(0);
        blanketImage.CrossFadeAlpha(1f, transitionDuration, shouldIgnoreTimescale);
    }
}
