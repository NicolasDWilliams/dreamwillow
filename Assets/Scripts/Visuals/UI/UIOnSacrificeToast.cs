﻿/*
 * Script for triggering toast message when companion is sacrificed
 * @George "Gino" Bryja III 
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIOnSacrificeToast : MonoBehaviour
{
    private CompanionManager mgr;

    [SerializeField] private Animator myAnim; 


    private void Start()
    {
        // Find game object with player tag and get its Health script instance
        GameObject player = PlayerCharacter.Get();
        mgr = player.GetComponent<CompanionManager>();

        // Subscribe to health change event and alter UI if event detected
        mgr.OnCompanionSacrifice.AddListener(OnSacrificeToast);

    }

    private void OnDestroy()
    {
        mgr.OnCompanionSacrifice.RemoveListener(OnSacrificeToast);
    }

    public void OnSacrificeToast()
    {
        myAnim.SetTrigger("open");
    }
}
