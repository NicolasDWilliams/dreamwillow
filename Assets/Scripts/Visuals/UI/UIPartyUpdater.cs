﻿// Updates active game view party member UI (eg ally icon visibility) when events are triggered
// Zena Abulhab

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class UIPartyUpdater : MonoBehaviour
{
    private PlayerInventory inventory;
    // The full and empty mask sprites
    [SerializeField]
    private Sprite fullMaskSprite;
    [SerializeField]
    private Sprite emptyMaskSprite;
    [SerializeField]
    private Sprite noneMaskSprite;

    // List of transforms of all masks in order
    private List<RectTransform> Masks;
    // Start is called before the first frame update
    void Start()
    {
        // Initialize hearts transform list with all hearts in children
        // Assumes they are returned in order they are listed in editor
        Masks = new List<RectTransform>();
        foreach (RectTransform mask in
            gameObject.GetComponentInChildren<RectTransform>(true))
        {
            Masks.Add(mask);
        }


        // Find game object with player tag and get its Health script instance
        GameObject player = PlayerCharacter.Get();
        inventory = player.GetComponent<PlayerInventory>();

        // Subscribe to health change event and alter UI if event detected
        inventory.events.OnCollarsChange.AddListener(UpdateMasksDisplayed);

        UpdateMasksDisplayed(inventory.Collars);
    }

    public void UpdateMasksDisplayed(int newMasks)
    {
        // Make first [newHealth] hearts be full hearts
        for (int i = 0; i < inventory.Collars - inventory.collarsUsed; i++)
        {
            ChangeMaskToFull(Masks[i]);
        }

        for (int i = inventory.Collars - inventory.collarsUsed; i < inventory.CollarsMax; i++)
        {
            ChangeMaskToEmpty(Masks[i]);
        }

        // Make any remaining hearts empty
        for (int i = inventory.CollarsMax; i < Masks.Count; i++)
        {
            ChangeMaskToNone(Masks[i]);
        }
    }

    /// <summary>
    /// Makes a mask icon have the full mask sprite
    /// </summary>
    void ChangeMaskToFull(RectTransform mask)
    {
        mask.gameObject.GetComponent<Image>().sprite = fullMaskSprite;
    }

    /// <summary>
    /// Makes a mask icon have the empty mask sprite
    /// </summary>
    void ChangeMaskToEmpty(RectTransform mask)
    {
        mask.gameObject.GetComponent<Image>().sprite = emptyMaskSprite;
    }

    void ChangeMaskToNone(RectTransform mask)
    {
        mask.gameObject.GetComponent<Image>().sprite = noneMaskSprite;
    }

    /// <summary>
    /// Unsubscribe from health change event 
    /// </summary>
    void OnDestroy()
    {
        inventory.events.OnCollarsChange.RemoveListener(UpdateMasksDisplayed);
    }


}
