﻿// Originally written by Nikhil Ghosh
// Edited by Matt Rader 12/7/19 - Moved the effect into a coroutine

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class UISecondaryUpdater : MonoBehaviour
{
    private Image fillImage;
    private GameObject player;
    private AttackManager attackManager;

    // private bool isCharged = false;

    public Sprite unchargedSprite;
    public Sprite chargedSprite;

    public int minUnchargedAlpha = 50;
    public int maxUnchargedAlpha = 125;
    public int onChargeAlpha = 255;
    public int chargedAlpha = 205;
    public float flashTime = 0.4f;

    public UnityEvent OnSecondaryCharged;

    private float cooldownLength;

    // Start is called before the first frame update
    void Start()
    {
        player = PlayerCharacter.Get();
        attackManager = player.GetComponent<AttackManager>();

        fillImage = gameObject.GetComponent<Image>();

        if(unchargedSprite == null)
        {
            unchargedSprite = fillImage.sprite;
        }

        if (chargedSprite == null)
        {
            chargedSprite = fillImage.sprite;
        }
    }

    public void InvokeEffect() {
        StartCoroutine(cooldownTimerEffect());
    }

    IEnumerator cooldownTimerEffect () {
        ProjectileAttack currentAttack = attackManager.GetSecondaryProjectileAttack();
        cooldownLength = currentAttack.cooldown;
        float counter = 0.0f;
        float t = 0.0f;

        fillImage.enabled = true;

        while (counter < cooldownLength) {
            t += Time.deltaTime / cooldownLength;
            counter += Time.deltaTime;

            fillImage.fillAmount = t;
            fillImage.color = new Color(1f, 1f, 1f, Mathf.Lerp(IntToColorFloat(minUnchargedAlpha), IntToColorFloat(maxUnchargedAlpha), t));

            yield return null;
        }

        fillImage.sprite = chargedSprite;
        fillImage.fillAmount = 1f;
        fillImage.color = new Color(1f, 1f, 1f, IntToColorFloat(onChargeAlpha));
        Invoke("UnFlashImage", flashTime);
        OnSecondaryCharged.Invoke();
    }

    // Original code written by Nikhil
    // Update is called once per frame
    // void Update()
    // {
    //     ProjectileAttack currentAttack = attackManager.GetSecondaryProjectileAttack();
    //     if(currentAttack == null)
    //     {
    //         fillImage.enabled = false;
    //     }
    //     else
    //     {
    //         fillImage.enabled = true;
    //         float currentCharge = currentAttack.CooldownLeft;
            
    //         if(currentCharge > 0.999f)
    //         {
    //             Debug.Log("AMBER");
    //             if(!isCharged)
    //             {
    //                 fillImage.sprite = chargedSprite;
    //                 fillImage.fillAmount = 1f;
    //                 fillImage.color = new Color(1f, 1f, 1f, IntToColorFloat(onChargeAlpha));
    //                 Invoke("UnFlashImage", flashTime);
    //                 OnSecondaryCharged.Invoke();
    //             }
    //             isCharged = true;
    //         }
    //         else
    //         {
    //             fillImage.fillAmount = currentCharge;
    //             fillImage.color = new Color(1f, 1f, 1f, Mathf.Lerp(IntToColorFloat(minUnchargedAlpha), IntToColorFloat(maxUnchargedAlpha), currentCharge));
    //             if(isCharged)
    //             {
    //                 fillImage.sprite = unchargedSprite;
    //                 CancelInvoke("UnFlashImage");
    //             }
    //             isCharged = false;
    //         }
    //     }
    // }

    void UnFlashImage()
    {
        fillImage.color = new Color(1f, 1f, 1f, IntToColorFloat(chargedAlpha));
    }

    float IntToColorFloat(int i)
    {
        return (float)i / 255f;
    }
}
