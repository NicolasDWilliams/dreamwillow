﻿// handles setting variables and labels based on slider values
// created by Nicolas Williams, 11/19/2019
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIVolumeSlider : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI volumeText = null;
    [SerializeField] private AK.Wwise.RTPC rtpcValue = null;
    [Tooltip("Set target RTPC value to slider value * valueMultiplier")]
    [SerializeField] private float valueMultiplier = 10f;

    private void OnEnable()
    {
        // GetComponent<Slider>().value = rtpcValue.GetValue(this.gameObject) / valueMultiplier;
    }


    public void SetVolume()
    {
        float newVolume = GetComponent<Slider>().value;
        volumeText.text = newVolume.ToString();
        rtpcValue.SetGlobalValue(newVolume * valueMultiplier);
    }

}
