﻿// Allows both Animator Triggers and Bools to be set 
// through animation events, as animation events 
// can only pass one parameter
// 0myBool sets bool myBool to false,
// 1myBool sets bool myBool to true

//@George Castle

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class animationParametersInterface : MonoBehaviour
{
    private Animator myAnim;
    private void Start() {
        myAnim = gameObject.GetComponent<Animator>();
    }
    public void triggerAnimation(string trigger) {
        myAnim.SetTrigger(trigger);
    }

    public void SetBool(string name) {
        myAnim.SetBool(name.Substring(1, name.Length - 1), (int)char.GetNumericValue(name[0]) != 0);
    }

}
