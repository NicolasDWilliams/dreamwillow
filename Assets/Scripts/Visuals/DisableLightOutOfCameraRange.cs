﻿/* Disables light component when the main camera is outside a certain range. */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UnityEngine.Experimental.Rendering.LWRP.Light2D))]
public class DisableLightOutOfCameraRange : MonoBehaviour
{
    public float range = 30;

    private UnityEngine.Experimental.Rendering.LWRP.Light2D light2D;

    private void Awake() {
        light2D = GetComponent<UnityEngine.Experimental.Rendering.LWRP.Light2D>();
    }

    private void LateUpdate() {
        float sqrCamDistance = Vector3.SqrMagnitude(Camera.main.transform.position - transform.position);

        light2D.enabled = sqrCamDistance < Mathf.Pow(range, 2);
    }
}
