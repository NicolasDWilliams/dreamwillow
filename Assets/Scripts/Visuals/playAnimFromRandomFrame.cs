﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playAnimFromRandomFrame : MonoBehaviour
{
    [SerializeField] Animator myAnim;
    [SerializeField] string animName;

    void Start()
    {
        float random = Random.Range(0f, 1f);
        myAnim.Play(animName, 0, random);
    }

}
