﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class ChromAberOnHealthChange : MonoBehaviour
{
    // POST PROCESSING VARIABLES
    [SerializeField]
    private EffectIntensities bloomIntensity;

    [SerializeField]
    private EffectIntensities chromAberIntensity;

    [System.Serializable]
    private struct EffectIntensities
    {
        public float defaultIntensity;
        public float maxIntensity;
    }

    private PostProcessVolume postProcessingVolume;
    private ChromaticAberration chromAber;
    private Bloom bloom;

    // EFFECT LENGTH VARIABLES
    [SerializeField]
    private float effectFadeInLength = 0.15f;

    [SerializeField]
    private float effectFadeOutLength = 0.5f;

    void Awake()
    {
        //TODO: Eventually set bloom and chromaticaberration based on constants

        chromAber = ScriptableObject.CreateInstance<ChromaticAberration>();
        chromAber.enabled.Override(true);
        chromAber.intensity.Override(chromAberIntensity.defaultIntensity);

        bloom = ScriptableObject.CreateInstance<Bloom>();
        bloom.enabled.Override(true);
        bloom.intensity.Override(bloomIntensity.defaultIntensity);

        postProcessingVolume = PostProcessManager.instance.QuickVolume(LayerMask.NameToLayer("Camera"), 100f, chromAber, bloom);
    }

    public void TriggerEffect()
    {
        StartCoroutine(OnHitVisualEffect());
    }

    IEnumerator OnHitVisualEffect()
    {
        // Fade in bloom and CA
        #region Fade In
        {
            float t = 0.0f;
            float counter = 0.0f;

            while (counter < effectFadeInLength)
            {
                t += Time.deltaTime / effectFadeInLength;
                counter += Time.deltaTime;

                bloom.intensity.value = Mathf.Lerp(bloomIntensity.defaultIntensity, bloomIntensity.maxIntensity, t);
                chromAber.intensity.value = Mathf.Lerp(chromAberIntensity.defaultIntensity, chromAberIntensity.maxIntensity, t);

                yield return null;
            }
        }
        #endregion

        // Fade out bloom and CA
        #region Fade Out
        {
            float t = 0.0f;
            float counter = 0.0f;

            while (counter < effectFadeOutLength)
            {
                t += Time.deltaTime / effectFadeOutLength;
                counter += Time.deltaTime;

                bloom.intensity.value = Mathf.Lerp(bloomIntensity.maxIntensity, bloomIntensity.defaultIntensity, t);
                chromAber.intensity.value = Mathf.Lerp(chromAberIntensity.maxIntensity, chromAberIntensity.defaultIntensity, t);

                yield return null;
            }
        }
        #endregion
    }
}
