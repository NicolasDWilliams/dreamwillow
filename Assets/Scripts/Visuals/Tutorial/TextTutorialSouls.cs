﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextTutorialSouls : TextOnEnter
{
    [SerializeField] private List<GameObject> enemy;
    [SerializeField] private bool killedEnemy;
    [SerializeField] private bool resEnemy = false;
    [SerializeField] private GameObject player;
    [SerializeField] private PlayerInventory inventory;
    [SerializeField] private GameObject shootPrompt;
    [SerializeField] private GameObject resPrompt;
    [SerializeField] private int collarCount = 99;
    [SerializeField] private GameEvent lastEnemy;

    public override void Start()
    {
        base.Start();
        player = PlayerCharacter.Get();
        inventory = player.GetComponent<PlayerInventory>();
        //resPrompt.transform.position = enemy.transform.position;
        resPrompt.SetActive(false);
    }

    private void Update()
    {
        var killed = KilledAllEnemies();

        if (killed && resPrompt.activeSelf == false && inventory.collarsUsed != inventory.Collars)
        {
            shootPrompt.GetComponent<UIPopupMessage>().HideMessage();
            resPrompt.SetActive(true);
            resPrompt.GetComponentInChildren<UIControllerPrompt>().UpdatePromptSprite();
            resPrompt.GetComponentInChildren<UIPopupMessage>().ShowMessage();
            collarCount = inventory.collarsUsed;

        }

        if (killed && inventory.collarsUsed > collarCount && resEnemy == false)
        {
            resEnemy = true;
            resPrompt.GetComponentInChildren<UIPopupMessage>().HideMessage();
        }

        var idx = NumEnemiesRemaining();
        if (idx != -1)
        {
            var pos = enemy[idx].transform.position;
            Debug.Log(pos);
            resPrompt.transform.position = pos + new Vector3(-12f, 2f, 0);
        }
    }

    private IEnumerator DelayedChangeText(string message, float time)
    {
        yield return new WaitForSeconds(time);
        ChangeText(message);
    }

    private bool KilledAnyEnemies()
    {
        for (int i = 0; i < enemy.Count; i++)
        {
            if (enemy[i].activeSelf == false)
                return true;
        }
        return false;
    }

    private bool KilledAllEnemies()
    {
        for (int i = 0; i < enemy.Count; i++)
        {
            if (enemy[i].activeSelf == true)
                return false;
        }
        return true;
    }

    private int NumEnemiesRemaining()
    {
        int idx = -1;
        int count = 0;
        for (int i = 0; i < enemy.Count; i++)
        {
            if (enemy[i].activeSelf)
            {
                idx = i;
                count += 1;
            }
        }

        return count == 1 ? idx : -1;
    }
}
