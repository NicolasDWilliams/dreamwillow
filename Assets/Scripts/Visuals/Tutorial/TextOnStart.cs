﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextOnStart : MonoBehaviour
{
    [SerializeField] private UIPopupMessage text;

    private void Start()
    {
        text = GetComponentInChildren<UIPopupMessage>();
        text.ShowMessage();
    }
}
