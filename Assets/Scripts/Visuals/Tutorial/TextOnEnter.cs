﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class TextOnEnter : MonoBehaviour
{
    public UIPopupMessage[] textList;
    public string[] textFullList;
    private TextMeshPro tmp;
    private Collider2D collider;
    public bool textIsChanging;
    public bool typeWrite = false;
    public bool usesDialogeSfx = false;

    public UnityEvent ShopkeeperDialogue;

    public virtual void Start()
    {
        textList = GetComponentsInChildren<UIPopupMessage>();
        textFullList = new string[textList.Length];
        tmp = GetComponentInChildren<TextMeshPro>();
        collider = GetComponent<Collider2D>();

        for (int i = 0; i < textList.Length; i++)
        {
            textFullList[i] = textList[i].GetComponent<TextMeshPro>().text;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            for (int i = 0; i < textList.Length; i++)
            {
                if (typeWrite)
                {
                    textList[i].GetComponent<TextMeshPro>().text = textFullList[i];
                    textList[i].ShowMessageTypeWriter();
                }
                else
                {
                    textList[i].ShowMessage();
                }
            }

            if (usesDialogeSfx)
            {
                ShopkeeperDialogue.Invoke();
            }
        }
    }

    public void ChangeText(string message)
    {
        if (tmp.text == message || textIsChanging)
            return;
        StartCoroutine(FadeTextOut(message));
    }

    public IEnumerator FadeTextOut(string message)
    {
        textIsChanging = true;
        foreach (UIPopupMessage popupMessage in textList)
        {
            popupMessage.HideMessage();
        }
        // Wait for fade to finish
        yield return new WaitForSeconds(textList[0].fadeTime);
        StartCoroutine(FadeTextIn(message));
        tmp.text = message;
    }

    public IEnumerator FadeTextIn(string message)
    {
        foreach (UIPopupMessage popupMessage in textList)
        {
            popupMessage.ShowMessage();
        }
        // Wait for fade to finish
        yield return new WaitForSeconds(textList[0].fadeTime);
        textIsChanging = false;
    }

}
