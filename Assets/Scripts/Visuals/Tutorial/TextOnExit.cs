﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextOnExit : MonoBehaviour
{
	[SerializeField] public UIPopupMessage text;

	public virtual void Start () {
		text = GetComponentInChildren<UIPopupMessage>();
	}

	private void OnTriggerExit2D (Collider2D collision) {
		if (collision.gameObject.CompareTag("Player"))
        {
            text.HideMessage();
        }
	}
}
