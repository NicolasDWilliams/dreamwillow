﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextTutorialShoot : TextOnEnter
{
    [SerializeField] private GameObject enemy;
    [SerializeField] private bool killedEnemy;
    [SerializeField] private bool resEnemy = false;
    [SerializeField] private GameObject player;
    [SerializeField] private PlayerInventory inventory;
    [SerializeField] private GameObject shootPrompt;
    [SerializeField] private GameObject resPrompt;

    public override void Start()
    {
        base.Start();
        player = PlayerCharacter.Get();
        inventory = player.GetComponent<PlayerInventory>();
        //resPrompt.transform.position = enemy.transform.position;
        resPrompt.SetActive(false);
    }

    private void Update()
    {
        killedEnemy = enemy.activeSelf == false;

        if (killedEnemy && resPrompt.activeSelf == false)
        {
            shootPrompt.GetComponent<UIPopupMessage>().HideMessage();
            resPrompt.SetActive(true);
            resPrompt.GetComponentInChildren<UIControllerPrompt>().UpdatePromptSprite();
            resPrompt.GetComponentInChildren<UIPopupMessage>().ShowMessage();
        }

        if (killedEnemy && inventory.collarsUsed > 0 && resEnemy == false)
        {
            resEnemy = true;
            resPrompt.GetComponentInChildren<UIPopupMessage>().HideMessage();
        }

    }
}
