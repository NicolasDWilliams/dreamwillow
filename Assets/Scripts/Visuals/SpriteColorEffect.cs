﻿/* Controls a color-changing effect for sprites, 
 * where the sprite color is tinted towards a different color.
 * Affects all sprites on this gameobject and children.
 * This assumes that the default color for all sprites is white.
 * -Max Perraut
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteColorEffect : MonoBehaviour
{

    public Color effectColor;
    public float duration;

    [Tooltip("Intensity of the effect over the duration")]
    public AnimationCurve intensityCurve;


    private float effectEndTime = 1f;
    private SpriteRenderer[] sprites;
    private bool playing = false;

    private void Awake()
    {
        sprites = GetComponentsInChildren<SpriteRenderer>();
    }

    private void Update()
    {
        if (playing)
        {
            if(Time.time < effectEndTime)
            {
                //Sample intensity curve based on how far through the duration we are
                float effectProgress = 1 - ((effectEndTime - Time.time) / duration);
                float currentIntensity = intensityCurve.Evaluate(effectProgress);

                //Tint sprite color based on intensity
                Color currentColor = Color.Lerp(Color.white, effectColor, currentIntensity);

                //Apply currentColor to each sprite
                foreach (SpriteRenderer sr in sprites)
                {
                    sr.color = currentColor;
                }
            }
            else
            {
                Stop();
            }
        }
    }


    /// <summary>
    /// Preform the effect
    /// </summary>
    public void Play()
    {
        playing = true;
        effectEndTime = Time.time + duration;
    }

    /// <summary>
    /// Preform the effect after setting the duration
    /// </summary>
    public void PlayWithDuration(float newDuration)
    {
        duration = newDuration;
        Play();
    }

    /// <summary>
    /// Stops the effect
    /// </summary>
    public void Stop()
    {
        playing = false;

        //Reset color for each sprite
        foreach (SpriteRenderer sr in sprites)
        {
            sr.color = Color.white;
        }
    }
}
