﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class enableParticle : MonoBehaviour
{
    public UnityEvent onParticleSystemStart;

    [SerializeField] GameObject myObject;
    public void enableObject() {
        PlayerInventory playerInventory = PlayerCharacter.Get().GetComponent<PlayerInventory>();

        if (playerInventory.collarsUsed < playerInventory.CollarsMax) {
            myObject.SetActive(true);
            onParticleSystemStart.Invoke();
        }
    }
}
