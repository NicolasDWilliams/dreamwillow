﻿/** Watches input through Rewired and reports player input through events
 * @Max Perraut
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Rewired;


public enum DeviceType
{
    Dualshock4,
    XboxOne,
    Xbox360,
    MouseAndKeyboard,
}

public class PlayerInput : MonoBehaviour
{
    public bool allowMouseAim = true;
    public float mouseAimSensitivity = 0.7f;
    public bool lockCursorOnAwake = false;

    public Vector2 Move { get; private set; } = Vector2.zero;
    public Vector2 Aim { get; private set; } = Vector2.right;
    public ButtonInput Attack;
    public ButtonInput SecondaryAttack;
    public ButtonInput Interact;
    public ButtonInput Heal;
    public Texture2D cursorSprite;

    // controller input
    public GameEvent deviceSwitchEvent;
    public GameInformation gameInformation;
    private Controller activeDevice = null;
    private bool usingController = false;



    //Definitions of Events with params

    [System.Serializable]
    public class Axis2DEvent : UnityEvent<Vector2> { }

    [Tooltip("Called every frame that move input changes.")]
    public Axis2DEvent OnMove;

    [Tooltip("Called every frame that aim input changes.")]
    public Axis2DEvent OnAim;

    [System.Serializable]
    public class ButtonInput
    {
        public bool isPressed { get; private set; } = false;
        public UnityEvent OnPress;
        public UnityEvent OnRelease;
        public UnityEvent WhilePress;

        public void Poll(bool inputValue)
        {
            if (inputValue != isPressed)
            {
                if (inputValue)
                {
                    isPressed = true;
                    OnPress.Invoke();
                }
                else
                {
                    isPressed = false;
                    OnRelease.Invoke();
                }
            }

            if (isPressed) WhilePress.Invoke();
        }
    }

    private Player player;

    private void Awake()
    {
        Vector2 cursorOffset = new Vector2(cursorSprite.width / 2f, cursorSprite.height / 2f);
        // if (allowMouseAim) Cursor.lockState = CursorLockMode.Locked;
        Cursor.SetCursor(cursorSprite, cursorOffset, CursorMode.ForceSoftware);
    }

    private void Start()
    {
        // must be in start so Rewired can initialize in Awake
        player = ReInput.players.GetPlayer(0);
    }

    private void Update()
    {
        CheckDevice();

        //Debug.Log(player.GetAxis2D("Mouse Aim Horizontal", "Mouse Aim Vertical"));
        PollMove();
        PollAim();
        Attack.Poll(player.GetButton("Attack"));
        SecondaryAttack.Poll(player.GetButton("Secondary Attack"));
        Interact.Poll(player.GetButton("Interact"));
        Heal.Poll(player.GetButton("Heal"));
    }

    // checks and announces whether or not the active device has changed
    private void CheckDevice()
    {
        Controller currentDevice = player.controllers.GetLastActiveController();
        if (currentDevice != activeDevice)
        { // device switched
            activeDevice = currentDevice;
            usingController = activeDevice.type == ControllerType.Joystick;
            Cursor.lockState = usingController ? CursorLockMode.Locked : CursorLockMode.None;
            Cursor.visible = !usingController;
            UpdateControllerType();
            deviceSwitchEvent.Raise();
        }
    }


    // sets the value of the current controller type (brand-specific: dualshock, xbox, etc)
    private void UpdateControllerType()
    {
        // if the keyboard is being used right now, then just set it to keyboard and return
        // NOTE: required because for loop below will privelege controllers over keyboard
        //       even if keyboard is currently in use
        if (!usingController)
        {
            gameInformation.activeDeviceType = DeviceType.MouseAndKeyboard;
            return;
        }

        string[] joysticks = Input.GetJoystickNames();

        for (int i = 0; i < joysticks.Length; ++i)
        {
            // ! TODO: Add support for Dualshock 3, wired xbox one contollers, and xbox 360 controllers
            // dualshock 4
            if (joysticks[i] == "Wireless Controller")
            {
                gameInformation.activeDeviceType = DeviceType.Dualshock4;
            }
            else if (joysticks[i] == "Controller (XBOX 360 For Windows)")
            {
                gameInformation.activeDeviceType = DeviceType.Xbox360;
            }
            // xbox one (wired or bluetooth, respectively)
            else if (joysticks[i].Length == 33 || joysticks[i].Length == 22)
            {
                gameInformation.activeDeviceType = DeviceType.XboxOne;
            }
            // use keyboard templates if unidentified controller
            else
            {
                gameInformation.activeDeviceType = DeviceType.MouseAndKeyboard;
            }
        }
    }



    private void PollMove()
    {
        Vector2 move = player.GetAxis2D("Move Horizontal", "Move Vertical").normalized;

        // This conditional has been commented out so the Move function is invoked every frame. - Matt Rader
        // TODO: Can "Move = move" be removed now that the conditional is no longer needed?
        //if (move != Move)
        //{
            Move = move;
            OnMove.Invoke(move);
        //}
    }

    private void PollAim()
    {
        // Mouse + Keyboard aiming
        if (allowMouseAim && !usingController)
        {
            // find cursor position & convert to world space
            Vector2 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            // TODO: Fix Player > AimPivot, which currently needs to be 1 unit up (why there is a Vector2.up in code below) -Nico
            Vector2 dirToCursor = cursorPos - ((Vector2)transform.position + Vector2.up);
            Aim = dirToCursor;
        }

        //If normal aim input changes, update Aim
        if (player.GetAxisDelta("Aim Horizontal") != 0 ||
           player.GetAxisDelta("Aim Vertical") != 0)
        {
            Aim = player.GetAxis2D("Aim Horizontal", "Aim Vertical").normalized;
        }

        //If mouse aim is allowed, modify aim based on mouse input

        OnAim.Invoke(Aim);
    }
}
