﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class ControllerVibration : MonoBehaviour {
	public FloatSettingVariable vibrationSetting;

	// Player information received from Rewired
	private Player player;

	// Set vibration in all Joysticks assigned to the Player
	static int motorIndex = 0; // the first motor

	public VibrationSettings shooting;
	public VibrationSettings damage;
	public VibrationSettings interact;
	public VibrationSettings healing;

	[System.Serializable]
	public class VibrationSettings {
		[Tooltip("This sets the motor speed for the controller")]
		public float vibrationIntensity;

		[Tooltip("Length of the vibration in seconds")]
		public float vibrationDuration;

		public void TriggerVibration (Player player, float intensityMultipler) {
			Controller currentDevice = player.controllers.GetLastActiveController();

			if (currentDevice.type == ControllerType.Joystick) {
				player.SetVibration(motorIndex, vibrationIntensity * intensityMultipler, vibrationDuration);
			}
		}
		
	}

	private void Start () {
		player = ReInput.players.GetPlayer(0);
	}

	public void ProjectileControllerVibration () {
		shooting.TriggerVibration(player, vibrationSetting.multiplier);
	}

	public void DamageControllerVibration () {
		damage.TriggerVibration(player, vibrationSetting.multiplier);
	}

	public void InteractControllerVibration () {
		interact.TriggerVibration(player, vibrationSetting.multiplier);
	}

	public void HealingControllerVibration () {
		healing.TriggerVibration(player, vibrationSetting.multiplier);
	}
}
