﻿/** 
 * Despawns a gameobject after a specified amount of time.
 * @Alex Kisil
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Despawn : MonoBehaviour
{

    [SerializeField] bool autoDespawn = false;
    [SerializeField] float timeToAutoDespawn = 5f;
    [SerializeField] float earlyDespawnTime = 3f;
    

    // Start is called before the first frame update
    void Start()
    {
        if (autoDespawn) { StartCoroutine(Predespawn(timeToAutoDespawn)); }
    }

    public void EarlyDespawn()
    {
        StartCoroutine(Predespawn(earlyDespawnTime));
    }

    IEnumerator Predespawn(float despawnTime)
    {
        while (true)
        {
            // Suspend execution for despawnTime seconds
            yield return new WaitForSeconds(despawnTime);

            Destroy(gameObject);
        }
    }
}
