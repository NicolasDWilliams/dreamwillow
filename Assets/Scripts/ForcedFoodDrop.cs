﻿/*
 * Script to force food object to spawn in tutorial
 * since Default Food Drop Chance was being forced to be true always
 * 
 * George Bryja III (ginoecb)
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForcedFoodDrop : MonoBehaviour
{
    [SerializeField] private GameObject enemy;
    [SerializeField] private GameObject food;
    [SerializeField] private bool hasSpawnedFood;

    private void Update()
    {
        if (hasSpawnedFood == false && enemy.activeSelf == false)
        {
            food.SetActive(true);
            hasSpawnedFood = true;
        }
    }

}
