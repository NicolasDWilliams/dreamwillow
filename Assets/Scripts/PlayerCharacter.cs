﻿/* Controls singleton behavior and other player-specific behavior
 * - Max Perraut
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CompanionManager))]
public class PlayerCharacter : MonoBehaviour
{
    private static GameObject _instance;

    public CompanionManager companionManager;
    public GameEvent enterCombatEvent;
    public GameEvent exitCombatEvent;

    private int numEnemiesInCombat = 0;

    private void Awake()
    {
        if(_instance != null)
        {
            Destroy(gameObject);
            Debug.LogWarning("Duplicate player character detected and removed");
            return;
        }

        _instance = gameObject;
        DontDestroyOnLoad(gameObject);
        companionManager = GetComponent<CompanionManager>();
    }

    private void Start() {
        //Destroy the player when the game restarts
        GameManager.Events.OnRestart.AddListener(DestroyPlayer);
        numEnemiesInCombat = 0;
    }

    public void AddEnemyToCombat()
    {
        // Hack to get rid of weird bug where numEnemiesInCombat would always start below 0.
        if (numEnemiesInCombat < 0)
        {
            numEnemiesInCombat = 0;
        }

        if (numEnemiesInCombat == 0)
        {
            enterCombatEvent.Raise();
            //Debug.Log("Entering combat.");
        }

        numEnemiesInCombat++;
        //Debug.Log("Add enemy to combat. Enemy number: " + numEnemiesInCombat);
    }

    public void RemoveEnemyFromCombat()
    {
        //Debug.Log("Remove enemy from combat. Enemy number: " + numEnemiesInCombat);
        if (numEnemiesInCombat == 1)
        {
            exitCombatEvent.Raise();
            //Debug.Log("Exiting combat.");
        }

        numEnemiesInCombat--;
    }

    private void OnDestroy()
    {
        if (_instance == gameObject) _instance = null;

        GameManager.Events.OnRestart.RemoveListener(DestroyPlayer);
    }

    public static GameObject Get()
    {
        if (_instance == null) Debug.LogError("Attempted to access Player Character but none are present.");

        return _instance;
    }

    public void PlayerDeath(){
        // Try to sacrifice a companion
        if(!companionManager.AttemptCompanionSacrifice()){
            // If it fails, game over
            GameManager.GameOver();
        }
    }

    private void DestroyPlayer(){
        Destroy(gameObject);
    }
}
