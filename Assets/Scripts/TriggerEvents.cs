﻿/* Invokes events for trigger messages
 * Each event passes the gameobject of the collider that caused the trigger
 * -Max Perraut
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEvents : MonoBehaviour
{
    [System.Serializable]
    public class TriggerEvent : UnityEngine.Events.UnityEvent<GameObject> { }

    public TriggerEvent OnTriggerEnter;
    public TriggerEvent OnTriggerStay;
    public TriggerEvent OnTriggerExit;

    [Tooltip("If true, only invoke events for object with tag <triggerTag>.")]
    public bool triggerOnlyForTag = false;
    public string triggerTag = "";

    //Check given object against any filters
    private bool Filter(GameObject checkObj)
    {
        return !triggerOnlyForTag || checkObj.CompareTag(triggerTag);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (Filter(collision.gameObject))
        {
            OnTriggerEnter.Invoke(collision.gameObject);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (Filter(collision.gameObject))
        {
            OnTriggerStay.Invoke(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (Filter(collision.gameObject))
        {
            OnTriggerExit.Invoke(collision.gameObject);
        }
    }
}
