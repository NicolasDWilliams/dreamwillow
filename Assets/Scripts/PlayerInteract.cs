﻿/** 
 * Manages interactions between the player and any interactable GameObjects.
 * @Alex Kisil
 * 
 * Modified by George Bryja III (ginoecb)
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerInteract : MonoBehaviour
{
	//  EVENTS
	public UnityEvent onSuccesfulInteract;

    // PARAMETERS
    [SerializeField] float interactRange = 3f;
    [SerializeField] private Color interactColor;

    // EXTERNAL REFERENCES
    private GameObject nearestInteractable;
    private Interactable nearestInteractableComponent;
    private Collider2D nearestInteractableCollider;

    // STATE
    private List<Collider2D> colliders = new List<Collider2D>();

    // Start is called before the first frame update
    void Start()
    {
        nearestInteractable = null;
        nearestInteractableComponent = null;
    }

    public void AddToColliders(GameObject objectToAdd)
    {
        colliders.Add(objectToAdd.GetComponent<Collider2D>());
    }

    public void SubtractFromColliders(GameObject objectToSubtract)
    {
        colliders.Remove(objectToSubtract.GetComponent<Collider2D>());
        FindNearestInteractable();
    }

    // Find the nearest interactable object within a given radius.
    public void FindNearestInteractable()
    {
        Collider2D bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector2 currentPosition = transform.position;

        foreach (Collider2D target in colliders)
        {
            Vector2 directionToTarget = (Vector2)target.gameObject.transform.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;

            if (dSqrToTarget < closestDistanceSqr && directionToTarget.magnitude < interactRange && target.gameObject.GetComponent<Interactable>())
            {
                closestDistanceSqr = dSqrToTarget;


                bestTarget = target;
            }
        }

        if (bestTarget)
        {
            // Case where target switches from one interactable to another
            if (nearestInteractable && nearestInteractableCollider != bestTarget)
            {
                // bestTarget is no longer being focused on
                nearestInteractableComponent.BeingUnfocused();

                //TODO: Change in the future
                nearestInteractable.GetComponent<SpriteRenderer>().color = Color.white;
            }
            // Case where target switches from nothing to an interactable
            if (!nearestInteractable || (nearestInteractable && nearestInteractableCollider != bestTarget))
            {
                nearestInteractable = bestTarget.gameObject;
                nearestInteractableComponent = nearestInteractable.GetComponent<Interactable>();
                nearestInteractableCollider = nearestInteractable.GetComponent<Collider2D>();
                nearestInteractableComponent.BeingFocused();

                //TODO: Change in the future
                nearestInteractable.GetComponent<SpriteRenderer>().color = interactColor;
            }
        }
        // Case where there are no longer any interactables in range
        else if (!bestTarget && nearestInteractable)
        {
            // bestTarget is no longer being focused on
            nearestInteractableComponent.BeingUnfocused();

            //TODO: Change in the future
            nearestInteractable.GetComponent<SpriteRenderer>().color = Color.white;

            nearestInteractable = null;
            nearestInteractableComponent = null;
            nearestInteractableCollider = null;
        }

    }

    // Call the interactable's Interact function to invoke a unity event for more
    // specific behavior.
    public void InteractWithObject()
    {
        //Debug.Log("Trying to interact.");

        if (nearestInteractableComponent)
        {
			//Debug.Log("Interacting with something.");
			onSuccesfulInteract.Invoke();

			nearestInteractableComponent.InteractWithPlayer();
        }
    }

    public void SetInteractRange(float value)
    {
        interactRange = value;
    }
}
