﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

// The GameManager is a class that handles the 

public class GameManager : MonoBehaviour
{

    //Game managment parameters

    [Tooltip("Scene name of each level of the game")]
    public List<string> levelSequence;

	[Tooltip("The index for the level in levelSequqnce the game should restart at")]
	public int levelRestartIndex = 1;

    [SerializeField]
    [Tooltip("Index of current level (does not include shops). If in the shop, this is the index of the last level.")]
    private int currentLevel = 0;

    [SerializeField]
    private string tutorialName = "Tutorial";
    private int tutorialIndex = 0;

    [SerializeField]
    private string shopSceneName = "ShopScene";

	[SerializeField]
	private string endingSceneName = "Ending";

	[SerializeField]
	private string mainMenuSceneName = "MainMenu";

	[Tooltip("Time before loading of new scene during transition.")]
    public float levelTransitionTime = 3f;

    [Tooltip("Length of Game Over sequence.")]
    public float gameOverTime = 2f;

    [Tooltip("Raised when the game is paused")]
    public GameEvent gamePauseEvent;
    [Tooltip("Raised when the game is resumed")]
    public GameEvent gameResumeEvent;

    [System.Serializable]
    public class GameManagerEvents
    {
        [System.Serializable]
        public class TimeEvent : UnityEvent<float> { }

        [Tooltip("Invoked at the start of a level transition. Passed the duration of the transition.")]
        public TimeEvent OnLevelTransition;

        [Tooltip("Invoked after a new level is loaded.")]
        public UnityEvent OnLevelLoaded;

        [Tooltip("Invoked when the game manager restarts the game")]
        public UnityEvent OnRestart;

        public UnityEvent OnGameOver;

		[Tooltip("Invoked when the game manager returns to the main menu")]
		public UnityEvent OnReturnToMainMenu;
	}

    public GameManagerEvents events;

    public static GameManagerEvents Events { get { return instance.events; } }

    // Variables for the Singleton Instance of this Game Manager. Written by Nikhil Ghosh
    private static GameManager instance;
    public static GameManager Instance { get { return instance; } }

    public static bool gamePaused { get; private set; } = false; // True if the game is paused, false otherwise.

    //True while in a transitional state
    public static bool inTransition { get; private set; } = false;

    // Sets the Singleton Instance up. Written by Nikhil Ghosh
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

	//////////////////////////////////////
	///SCENE CONTROL

    /// <summary>
	/// Start the game (from main menu)
	/// </summary>
	public static void StartGame(string startSceneName) {
		instance.StartCoroutine(instance.DoSceneTransition(startSceneName));
	}

	/// <summary>
	/// Returns the game to the main menu
	/// </summary>
	public static void ReturnToMainMenu () {
		Events.OnReturnToMainMenu.Invoke();
		Events.OnRestart.Invoke();

		// set level back to first
		instance.currentLevel = 0;

		// load first level
		SceneManager.LoadScene(instance.mainMenuSceneName);

		if (gamePaused) {
			UnpauseGame();
		}
	}

	/// <summary>
	/// Restarts the game
	/// </summary>
	public static void RestartGame()
    {
        Events.OnRestart.Invoke();

        if (SceneManager.GetActiveScene().name == instance.tutorialName) {
            // Reload the tutorial
            SceneManager.LoadScene(instance.levelSequence[instance.tutorialIndex]);

            // set level back to tutorial
            instance.currentLevel = instance.tutorialIndex;
        }
        else {
            // load first level
            SceneManager.LoadScene(instance.levelSequence[instance.levelRestartIndex]);

            // set level back to tutorial
            instance.currentLevel = instance.levelRestartIndex;
        }

        UnpauseGame();
    }

    /// <summary>
    /// Exits level to shop
    /// </summary>
    public static void ExitLevel()
    {
        if (!inTransition)
        {
			// If we are in the ending then go to level 1
			if (SceneManager.GetActiveScene().name == instance.endingSceneName) {
				instance.StartCoroutine(instance.DoSceneTransition(instance.levelSequence[instance.levelRestartIndex]));
                instance.currentLevel = 1;  
			}
			// If we are on the last level then skip the shop and go to the ending
			else if (instance.currentLevel == (instance.levelSequence.Count - 1)) {
				instance.StartCoroutine(instance.DoSceneTransition(instance.endingSceneName));
			}
			// If any level but the last or ending
			else if (instance.currentLevel <= (instance.levelSequence.Count - 2)) {
				instance.StartCoroutine(instance.DoSceneTransition(instance.shopSceneName));
			}
			else {
				Debug.LogError("No appropriate level to transition to.");
			}
		}
        else Debug.LogError("Attempted to exit level while a transition was already in progress.");
    }

    /// <summary>
    /// Exits shop to next level
    /// </summary>
    public static void ExitShop()
    {
        if (!inTransition)
        {
            // increment current level if not last in sequence
            if (instance.currentLevel < instance.levelSequence.Count - 1) instance.currentLevel++;

            // get name of scene for next level
            string nextLevel = instance.levelSequence[instance.currentLevel];

            // start transition
            instance.StartCoroutine(instance.DoSceneTransition(nextLevel));
        }
        else Debug.LogError("Attempted to exit shop while a transition was already in progress.");
    }

    // Preform a transition to a new scene
    private IEnumerator DoSceneTransition(string sceneName)
    {
        Debug.Log(string.Format("Starting transition to scene \"{0}\"", sceneName));

        inTransition = true;

        events.OnLevelTransition.Invoke(levelTransitionTime);

        PauseGame();

        yield return new WaitForSecondsRealtime(levelTransitionTime);

        SceneManager.LoadScene(sceneName);

        UnpauseGame();

        events.OnLevelLoaded.Invoke();

        inTransition = false;

        Debug.Log(string.Format("Completed transition to scene \"{0}\"", sceneName));
    }

    public static void GameOver()
    {
        PauseGame();
        Events.OnGameOver.Invoke();
    }

    // Quits the game. Note that this only works on builds, not the editor versions. Written by Nikhil Ghosh
    public static void QuitGame()
    {
        // Add Save Features HERE if necessary
        Application.Quit();
    }

    /*
     * Pauses the game by setting the Time Scale to 0 and stops the timers in the Game Manager. Use this function to pause the game.
     * Note that this function only shuts down things that require the Time class (e.g. anything using Time.deltaTime or Time.fixedDeltaTime).
     * Written by Nikhil Ghosh.
     */
    public static void PauseGame()
    {
        instance.gamePauseEvent.Raise();
        gamePaused = true;
        Time.timeScale = 0f;
    }

    /*
     * Unpauses the game by setting the time scale to 1 and resuming the timers in the Game Manager. Use this function to pause the game.
     */
    public static void UnpauseGame()
    {
        instance.gameResumeEvent.Raise();
        gamePaused = false;
        Time.timeScale = 1f;
    }

    /*
     * Returns a string of the formatted time. 
     * Note that time is in seconds and millisecondsDisplayed determines if milliseconds will be shown on the output (01:12 vs 01:12:025)
     * For instance, TimeToText(125.5469, true) = "2:05.546" and TimeToText(125.5469) = "2:05"
     * Written by Nikhil Ghosh
     */
    public string TimeToText(float time, bool millisecondsDisplayed = false)
    {
        string result = "";

        int hours = (int)Mathf.Floor(time / 3600f);
        if (hours > 0)
        {
            result += hours + ":";
        }

        int minutes = (int)Mathf.Floor(time / 60f);
        if (minutes >= 10)
        {
            result += minutes;
        }
        else
        {
            result += "0" + minutes;
        }

        result += ":";

        int seconds = ((int)Mathf.Floor(time)) % 60;
        if (seconds >= 10)
        {
            result += seconds;
        }
        else
        {
            result += "0" + seconds;
        }

        if (millisecondsDisplayed)
        {
            int milliseconds = (int)Mathf.Floor((time - Mathf.Floor(time)) * 1000);
            result += ".";
            if (milliseconds >= 100)
            {
                result += milliseconds;
            }
            else if (milliseconds >= 10)
            {
                result += "0" + milliseconds;
            }
            else
            {
                result += "00" + milliseconds;
            }
        }

        return result;
    }
}
