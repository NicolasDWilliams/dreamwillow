﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Tilemaps;

public class DespawnIfOnWater : MonoBehaviour
{

    [SerializeField] UnityEvent Despawn;
    // Start is called before the first frame update
    void Start()
    {
        Tilemap waterTileMap = GameObject.FindWithTag("Water").GetComponent<Tilemap>();
        if (waterTileMap.GetTile(waterTileMap.WorldToCell(transform.position)) != null)
        {
            Despawn.Invoke();
        }
    }
}
