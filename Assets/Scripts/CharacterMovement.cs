﻿/** 
 * Controls basic character movement.
 * Created by @Alex Kisil
 * 09/27/2019
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]

public class CharacterMovement : MonoBehaviour
{
    [SerializeField] float maxMoveSpeed = 10f;
    [SerializeField] float dampingFactor = 5f;
    private Collider2D characterCollider;
    private Rigidbody2D characterRB;
    private Vector2 characterVelocity;
    private Vector2 knockbackVelocity;

    private Vector2 velocitySmoothingVec;

    [SerializeField] private float accelerationTime;

    private void Awake()
    {
        characterCollider = GetComponent<Collider2D>();
        characterRB = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {
        characterVelocity = Vector2.zero;
    }

    public float GetSpeed()
    {
        return maxMoveSpeed;
    }

    public Vector2 GetVelocity()
    {
        return characterVelocity;
    }

    public void SetMoveSpeed(float newMoveSpeed)
    {
        maxMoveSpeed = newMoveSpeed;
    }

    public float GetTimeForDistance(float distanceToMove)
    {
        return distanceToMove / maxMoveSpeed;
    }

    // Sets the velocity of the character by taking in a direction whose magnitude
    // is equal to the fraction of the character's max speed and multiplying it by
    // max speed
    public void Move(Vector2 inputDirection)
    {
        //characterVelocity.x = Mathf.Clamp(inputDirection.x, -.5f, .5f) * maxMoveSpeed;
        //characterVelocity.y = Mathf.Clamp(inputDirection.y, -.5f, .5f) * maxMoveSpeed;
        //characterVelocity = inputDirection.normalized * maxMoveSpeed;
        Vector2 targetVelocity = inputDirection.normalized * maxMoveSpeed;
        characterVelocity = Vector2.SmoothDamp(characterVelocity, targetVelocity, ref velocitySmoothingVec, accelerationTime);
    }

    public void ApplyKnockback(Vector2 knockback)
    {
        knockbackVelocity += knockback;
    }

    private void FixedUpdate()
    {
        if (knockbackVelocity.sqrMagnitude < 0.01f)
        {
            transform.Translate(characterVelocity * Time.fixedDeltaTime);
        }
        else
        {
            Debug.Log(knockbackVelocity);
            transform.Translate(knockbackVelocity * Time.fixedDeltaTime);
            if (knockbackVelocity.magnitude < dampingFactor * Time.fixedDeltaTime)
            {
                knockbackVelocity = Vector2.zero;
            }
            else
            {
                knockbackVelocity = knockbackVelocity.normalized * (knockbackVelocity.magnitude - (dampingFactor * Time.fixedDeltaTime));
            }
        }
    }

}

