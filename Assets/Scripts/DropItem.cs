﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItem : MonoBehaviour
{
    // Dropdown menu for item type.
    // TODO : Add more item types if they are implemented.
    [System.Serializable]
    public enum itemType
    {
        potion,
        collar,
    }
    [SerializeField] private itemType itemtype;
    private PlayerInventory inventory;
    private string item;
    private void Start()
    {
        inventory = PlayerCharacter.Get().GetComponent<PlayerInventory>();
        // Switch cases for item types.
        // TODO: Add more if different items are implemented.
        itemtype = itemType.potion;
        switch (itemtype)
        {
            case itemType.potion:
                item = "potion";
                break;
            case itemType.collar:
                item = "collar";
                break;
            default:
                break;
        }

    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (item == "potion")
        {
            inventory.HealthPotionGain(1);
        }
        else if (item == "collar")
        {
            inventory.CollarGain(1);
        }
        Destroy(this.gameObject);
    }
}
