﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableAnimWhenOutOfView : MonoBehaviour
{
	Animator myAnim;

	void Start () {

		myAnim = gameObject.GetComponent<Animator>();
		myAnim.enabled = false;
	}

	private void OnTriggerEnter2D (Collider2D other) {
		if (other.tag == "MainCamera") {
			if (!myAnim.enabled) {
				myAnim.enabled = true;
			}
		}
	}

	private void OnTriggerExit2D (Collider2D other) {
		if (other.tag == "MainCamera") {
			if (myAnim.enabled) {
				myAnim.enabled = false;
			}
		}
	}
}
