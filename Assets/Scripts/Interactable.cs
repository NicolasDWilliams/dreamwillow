﻿/** 
 * Gives the general ability to be interacted with by the player through the PlayerInteract
 * script. Unity events are invoked upon consideration and interaction to allow
 * for more specific behavior.
 * @Alex Kisil
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Interactable : MonoBehaviour
{
    public UnityEvent onFocused;
    public UnityEvent onUnfocused;
    public UnityEvent onInteraction;

    public void InteractWithPlayer()
    {
        onInteraction.Invoke();
    }

    public void BeingFocused()
    {
        //Debug.Log("Something's being considered");
        onFocused.Invoke();
    }

    public void BeingUnfocused()
    {
        //Debug.Log("Something's being considered");
        onUnfocused.Invoke();
    }
}
