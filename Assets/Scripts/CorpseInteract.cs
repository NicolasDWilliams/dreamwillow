﻿/** 
 * Script of listeners to interaction events invoked by Interactable.cs
 * @Alex Kisil
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CorpseInteract : MonoBehaviour
{
    [SerializeField] public GameObject companionToSpawn;

    private PlayerInventory playerInventory;

    public UnityEvent onResurrect;

    private CompanionManager companionManager;

    private void Start()
    {
        companionManager = PlayerCharacter.Get()?.gameObject.GetComponent<CompanionManager>();
    }

    public void SpawnCompanion()
    {
        playerInventory = PlayerCharacter.Get().GetComponent<PlayerInventory>();

        if (playerInventory.Collars > 0)
        {
            GameObject companion = Instantiate(companionToSpawn, transform.position, transform.rotation) as GameObject;
            CompanionController companionController = companion.GetComponent<CompanionController>();
            companionController.innerRadius = companionController.innerRadius + (companionManager.getNumCompanions() * .2f);
            companionController.SetFollowRadius(companionController.GetFollowRadius() + (companionManager.getNumCompanions() * .2f));

            playerInventory.CollarLose(1);
            companionManager.addCompanion();

            onResurrect.Invoke();
            Destroy(gameObject);
        }
    }
}
