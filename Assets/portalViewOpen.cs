﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class portalViewOpen : MonoBehaviour
{
    [SerializeField] Animator myAnim;

    public UnityEngine.Events.UnityEvent OnOpen;

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Player") {
            myAnim.SetTrigger("open");
            OnOpen.Invoke();
        }
    }
}
