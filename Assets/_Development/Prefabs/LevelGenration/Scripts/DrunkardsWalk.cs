using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[System.Serializable]
public class DrunkardsWalk
{
    enum LevelTile { empty, floor, wall, bottomWall };
    LevelTile[,] grid;

    struct RandomWalker
    {
        public Vector2 dir;
        public Vector2 pos;
    }

    List<RandomWalker> walkers;

    MapGenerationTileInfo tilesAndTileMaps;
    MiscMapInformation mapInfo;

    //Drunkard's Walk Algorithm Modifiers
    [Tooltip("Percentage of the level to fill. ex. 0.2 == 20%")]
    public float percentToFill = 0.8f;
    public float chanceWalkerChangeDir = 0.33f;
    public float chanceWalkerSpawn = 0.025f;
    public float chanceWalkerDestoy = 0.05f;
    [Tooltip("Max amount of walkers that can be spawned")]
    public int maxWalkers = 10;
    [Tooltip("Max steps that can be taken")]
    public int iterationSteps = 100000;

    public void StartMapGeneration(MapGenerationTileInfo mapTilesAndTileMaps, MiscMapInformation miscMapInformation)
    {
        tilesAndTileMaps = mapTilesAndTileMaps;
        mapInfo = miscMapInformation;
        GenerateMapDrunkinly();
    }

    #region Drunkards Walk
    void GenerateMapDrunkinly()
    {
        Setup();
        CreateFloors();
        CreateWalls();
        SpawnLevel();
    }

    void Setup()
    {
        // prepare grid
        grid = new LevelTile[mapInfo.levelWidth, mapInfo.levelHeight];
        for (int x = 0; x < mapInfo.levelWidth - 1; x++)
        {
            for (int y = 0; y < mapInfo.levelHeight - 1; y++)
            {
                grid[x, y] = LevelTile.empty;
            }
        }

        //generate first walker
        walkers = new List<RandomWalker>();
        RandomWalker walker = new RandomWalker();
        walker.dir = RandomDirection();

        Vector2 pos = new Vector2((UnityEngine.Random.Range(mapInfo.mapBorderSize, mapInfo.levelWidth)), UnityEngine.Random.Range(mapInfo.mapBorderSize, mapInfo.levelHeight));
        // Make sure the walker starts on a multiple of 4
        pos.x = pos.x - (pos.x % 4);
        pos.y = pos.y - (pos.y % 4);
        // Make sure that by ensuring the walker position starts on a multiple of 4 it hasn't pushed it over the groundToBorderPadding
        pos.x = Mathf.Clamp(pos.x, mapInfo.mapBorderSize, mapInfo.levelWidth - mapInfo.mapBorderSize);
        pos.y = Mathf.Clamp(pos.y, mapInfo.mapBorderSize, mapInfo.levelHeight - mapInfo.mapBorderSize);

        walker.pos = pos;
        walkers.Add(walker);
    }

    void CreateFloors()
    {
        int iterations = 0;
        int floorTileCount = 0;
        do
        {
            //create floor at position of every Walker
            foreach (RandomWalker walker in walkers)
            {
                for (int xOffset = 0; xOffset < 2; ++xOffset)
                {
                    for (int yOffset = 0; yOffset < 2; ++yOffset)
                    {
                        grid[(int)walker.pos.x + xOffset, (int)walker.pos.y + yOffset] = LevelTile.floor;
                        floorTileCount++;
                    }
                }
            }

            //chance: destroy Walker
            int numberChecks = walkers.Count;
            for (int i = 0; i < numberChecks; i++)
            {
                if (UnityEngine.Random.value < chanceWalkerDestoy && walkers.Count > 1)
                {
                    walkers.RemoveAt(i);
                    break;
                }
            }

            //chance: Walker pick new direction
            for (int i = 0; i < walkers.Count; i++)
            {
                if (UnityEngine.Random.value < chanceWalkerChangeDir)
                {
                    RandomWalker thisWalker = walkers[i];
                    thisWalker.dir = RandomDirection();
                    walkers[i] = thisWalker;
                }
            }

            //chance: spawn new Walker
            numberChecks = walkers.Count;
            for (int i = 0; i < numberChecks; i++)
            {
                if (UnityEngine.Random.value < chanceWalkerSpawn && walkers.Count < maxWalkers)
                {
                    RandomWalker walker = new RandomWalker();
                    walker.dir = RandomDirection();
                    walker.pos = walkers[i].pos;
                    walkers.Add(walker);
                }
            }

            //move Walkers
            for (int i = 0; i < walkers.Count; i++)
            {
                RandomWalker walker = walkers[i];
                walker.pos += (walker.dir * 4);
                walkers[i] = walker;
            }

            //avoid boarder of grid
            for (int i = 0; i < walkers.Count; i++)
            {
                RandomWalker walker = walkers[i];
                walker.pos.x = Mathf.Clamp(walker.pos.x, mapInfo.mapBorderSize, mapInfo.levelWidth - mapInfo.mapBorderSize);
                walker.pos.y = Mathf.Clamp(walker.pos.y, mapInfo.mapBorderSize, mapInfo.levelHeight - mapInfo.mapBorderSize);
                walkers[i] = walker;
            }

            //check to exit loop
            if (((float)floorTileCount / (float)grid.Length) > percentToFill)
            {
                break;
            }

            iterations++;
        } while (iterations < iterationSteps);
    }

    void CreateWalls()
    {
        for (int x = 0; x < mapInfo.levelWidth - 1; x++)
        {
            for (int y = 0; y < mapInfo.levelHeight - 1; y++)
            {
                if ((grid[x, y] != LevelTile.floor) && (x % 4 == 0) && (y % 4 == 0))
                {
                    grid[x, y] = LevelTile.wall;
                }
            }
        }
    }

    void SpawnLevel()
    {
        for (int x = 0; x < mapInfo.levelWidth; x++)
        {
            for (int y = 0; y < mapInfo.levelHeight; y++)
            {
                tilesAndTileMaps.groundTileMap.SetTile(new Vector3Int((x / 2), (y / 2), 0), tilesAndTileMaps.floorTiles[UnityEngine.Random.Range(0, tilesAndTileMaps.floorTiles.Length)]);

                switch (grid[x, y])
                {
                    case LevelTile.empty:
                        break;

                    case LevelTile.floor:
                        if (UnityEngine.Random.value <= mapInfo.detailTilePercentToFill)
                        {
                            tilesAndTileMaps.detailTileMap.SetTile(new Vector3Int((x / 2), (y / 2), 0), tilesAndTileMaps.detailTiles[UnityEngine.Random.Range(0, tilesAndTileMaps.detailTiles.Length)]);
                        }

                        break;

                    case LevelTile.wall:
                        tilesAndTileMaps.wallTileMap.SetTile(new Vector3Int((x / 4), (y / 4), 0), tilesAndTileMaps.wallTiles[UnityEngine.Random.Range(0, tilesAndTileMaps.wallTiles.Length)]);
                        break;
                }
            }
        }
    }

    Vector2 RandomDirection()
    {
        int choice = Mathf.FloorToInt(UnityEngine.Random.value * 3.99f);
        switch (choice)
        {
            case 0:
                return Vector2.down;
            case 1:
                return Vector2.left;
            case 2:
                return Vector2.up;
            default:
                return Vector2.right;
        }
    }

    // Check that every tile around the proposed spawnPoint MUST be a ground tile to prevent the player from spawning in or next to a wall.
    bool ValidPlacement(Vector3 placementToCheck, int checkRadius)
    {
        int placementCheckX = (int)placementToCheck.x;
        int placementCheckY = (int)placementToCheck.y;

        for (int x = placementCheckX - checkRadius; x < placementCheckX + checkRadius; ++x)
        {
            for (int y = placementCheckY - checkRadius; y < placementCheckY + checkRadius; ++y)
            {
                if (grid[x, y] != LevelTile.floor)
                {
                    return false;
                }
            }
        }

        return true;
    }
    #endregion
}