using UnityEngine;
using System.Collections;
using UnityEngine.Tilemaps;

[System.Serializable]
public class MapGenerationTileInfo
{
    [Header("Tile Arrays")]
    public Tile[] floorTiles;
    public RuleTile[] wallTiles;
    public Tile[] detailTiles;
    public Tile[] grassTiles;

    [Header("Tilemaps")]
    public Tilemap groundTileMap;
    public Tilemap wallTileMap;
    public Tilemap detailTileMap;
    public Tilemap grassTileMap;
}