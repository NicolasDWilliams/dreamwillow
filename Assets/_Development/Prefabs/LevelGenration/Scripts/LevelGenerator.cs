﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Tilemaps;
using Cinemachine;
using Pathfinding;
#if UNITY_5_5_OR_NEWER
using UnityEngine.Profiling;
#endif

// TODO
// - Variable spawn rate for different enemies
// - Variable spawn rate for different details

public class LevelGenerator : MonoBehaviour
{
    public enum mapGenAlgo { cellularAutomata, drunkardWalk };

    public mapGenAlgo mapGenerationAlgorithm;
    //public bool createNewMapOnClick = true;

    [SerializeField]
    private MapGenerationTileInfo mapTilesAndTileMaps;

    [Space]

    [SerializeField]
    private MiscMapInformation miscMapInformation;

    [Space]

    [SerializeField]
    private DrunkardsWalk drunkardsWalk;

    [Space]

    [SerializeField]
    private CellularAutomata cullularAutomata;

    public void CreateNewMap()
    {
        ClearTileMaps();

        if (mapGenerationAlgorithm == mapGenAlgo.cellularAutomata)
        {
            cullularAutomata.StartMapGeneration(mapTilesAndTileMaps, miscMapInformation);
        }
        else if (mapGenerationAlgorithm == mapGenAlgo.drunkardWalk)
        {
            drunkardsWalk.StartMapGeneration(mapTilesAndTileMaps, miscMapInformation);
        }

    }

    public void ClearTileMaps()
    {
        mapTilesAndTileMaps.groundTileMap.ClearAllTiles();
        mapTilesAndTileMaps.wallTileMap.ClearAllTiles();
        mapTilesAndTileMaps.detailTileMap.ClearAllTiles();
        mapTilesAndTileMaps.grassTileMap.ClearAllTiles();
    }
}
