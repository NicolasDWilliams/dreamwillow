using UnityEngine;
using System.Collections;

[System.Serializable]
public class MiscMapInformation
{
    public int levelWidth = 128;
    public int levelHeight = 128;
    public int mapBorderSize = 20;
    public float detailTilePercentToFill = 0.15f;
}