using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[System.Serializable]
public class CellularAutomata
{
    int[,] map;

    MapGenerationTileInfo tilesAndTileMaps;
    MiscMapInformation mapInfo;

    [Range(0, 100)]
    public int fillPercent;
    public string seed;
    public bool useRandomSeed;
    public int smoothingIterations = 5;

    public void StartMapGeneration(MapGenerationTileInfo mapTilesAndTileMaps, MiscMapInformation miscMapInformation)
    {
        Debug.Log("Start Map Generation");
        tilesAndTileMaps = mapTilesAndTileMaps;
        mapInfo = miscMapInformation;
        GenerateMapCellular();
    }

    #region Cellular Automata Algorithm
    void GenerateMapCellular()
    {
        Debug.Log("Start Cellular Automata");
        map = new int[mapInfo.levelWidth, mapInfo.levelHeight];
        RandomFillMap();

        for (int i = 0; i < smoothingIterations; ++i)
        {
            SmoothMap();
        }

        ProcessMap();

        int[,] borderedMap = new int[mapInfo.levelWidth + mapInfo.mapBorderSize * 2, mapInfo.levelHeight + mapInfo.mapBorderSize * 2];

        for (int x = 0; x < borderedMap.GetLength(0); x += 4)
        {
            for (int y = 0; y < borderedMap.GetLength(1); y += 4)
            {
                if (x >= mapInfo.mapBorderSize && x < mapInfo.levelWidth + mapInfo.mapBorderSize && y >= mapInfo.mapBorderSize && y < mapInfo.levelHeight + mapInfo.mapBorderSize)
                {
                    borderedMap[x, y] = map[x - mapInfo.mapBorderSize, y - mapInfo.mapBorderSize];
                }
                else
                {
                    borderedMap[x, y] = 1;
                }

                for (int a = x; a < x + 4; ++a)
                {
                    for (int b = y; b < y + 4; ++b)
                    {
                        tilesAndTileMaps.groundTileMap.SetTile(new Vector3Int((a / 2), (b / 2), 0), tilesAndTileMaps.floorTiles[UnityEngine.Random.Range(0, tilesAndTileMaps.floorTiles.Length)]);
                    }
                }

                switch (borderedMap[x, y])
                {
                    case 1:
                        tilesAndTileMaps.wallTileMap.SetTile(new Vector3Int((x / 4), (y / 4), 0), tilesAndTileMaps.wallTiles[UnityEngine.Random.Range(0, tilesAndTileMaps.wallTiles.Length)]);
                        break;

                    case 0:
                        for (int a = x; a < x + 4; ++a)
                        {
                            for (int b = y; b < y + 4; ++b)
                            {
                                if (UnityEngine.Random.value <= mapInfo.detailTilePercentToFill)
                                {
                                    tilesAndTileMaps.detailTileMap.SetTile(new Vector3Int((a / 2), (b / 2), 0), tilesAndTileMaps.detailTiles[UnityEngine.Random.Range(0, tilesAndTileMaps.detailTiles.Length)]);
                                }
                            }
                        }
                        break;

                }
            }
        }
    }

    void RandomFillMap()
    {
        if (useRandomSeed)
        {
            seed = System.DateTime.Now.ToString();
        }

        System.Random pseudoRandom = new System.Random(seed.GetHashCode());

        for (int x = 0; x < mapInfo.levelWidth; x += 4)
        {
            for (int y = 0; y < mapInfo.levelHeight; y += 4)
            {
                if (x == 0 || x == mapInfo.levelWidth - 1 || y == 0 || y == mapInfo.levelHeight - 1)
                {
                    map[x, y] = 1;
                }
                else
                {
                    map[x, y] = (pseudoRandom.Next(0, 100) < fillPercent) ? 1 : 0;
                }
            }
        }
    }

    void SmoothMap()
    {
        int[,] tempMap = new int[mapInfo.levelWidth, mapInfo.levelHeight];
        tempMap = map;

        for (int x = 0; x < mapInfo.levelWidth; x += 4/*++x*/)
        {
            for (int y = 0; y < mapInfo.levelHeight; y += 4/*++y*/)
            {
                int neighborWallTiles = GetNumWallsAround(x, y);

                if (neighborWallTiles > 4)
                {
                    tempMap[x, y] = 1;
                }
                else if (neighborWallTiles < 4)
                {
                    tempMap[x, y] = 0;
                }
            }
        }

        map = tempMap;
    }

    int GetNumWallsAround(int xPos, int yPos)
    {
        int wallCount = 0;

        for (int neighborX = xPos - (1 * 4); neighborX <= xPos + (1 * 4); neighborX += 4)
        {
            for (int neighborY = yPos - (1 * 4); neighborY <= yPos + (1 * 4); neighborY += 4)
            {
                if (neighborX >= 0 && neighborX < mapInfo.levelWidth && neighborY >= 0 && neighborY < mapInfo.levelHeight)
                {
                    if (neighborX != xPos || neighborY != yPos)
                    {
                        wallCount += map[neighborX, neighborY];
                    }
                }
                else
                {
                    wallCount++;
                }
            }
        }

        return wallCount;
    }

    void ProcessMap()
    {
        List<List<Coordinate>> wallRegions = GetRegions(1);
        int wallThresholdSize = 50;

        foreach (List<Coordinate> wallRegion in wallRegions)
        {
            if (wallRegion.Count < wallThresholdSize)
            {
                foreach (Coordinate tile in wallRegion)
                {
                    map[tile.xPos, tile.yPos] = 0;
                }
            }
        }

        List<List<Coordinate>> roomRegions = GetRegions(0);
        int roomThresholdSize = 50;
        List<Room> remainingRooms = new List<Room>();

        foreach (List<Coordinate> roomRegion in roomRegions)
        {
            if (roomRegion.Count < roomThresholdSize)
            {
                foreach (Coordinate tile in roomRegion)
                {
                    map[tile.xPos, tile.yPos] = 1;
                }
            }
            else
            {
                remainingRooms.Add(new Room(roomRegion, map));
            }
        }

        // Sort the remaining rooms by size descending
        remainingRooms.Sort();

        // The largest room will be the main room
        remainingRooms[0].isMainRoom = true;
        remainingRooms[0].isAccessibleFromMainRoom = true;

        ConnectClosestRooms(remainingRooms);
    }

    void ConnectClosestRooms(List<Room> allRooms, bool forceAccessibilityFromMainRoom = false)
    {
        List<Room> roomListA = new List<Room>();
        List<Room> roomListB = new List<Room>();

        if (forceAccessibilityFromMainRoom)
        {
            foreach (Room room in allRooms)
            {
                if (room.isAccessibleFromMainRoom)
                {
                    roomListB.Add(room);
                }
                else
                {
                    roomListA.Add(room);
                }
            }
        }
        else
        {
            roomListA = allRooms;
            roomListB = allRooms;
        }

        int bestDistance = 0;
        Coordinate bestTileA = new Coordinate();
        Coordinate bestTileB = new Coordinate();
        Room bestRoomA = new Room();
        Room bestRoomB = new Room();
        bool connectionFound = false;

        foreach (Room roomA in roomListA)
        {
            if (!forceAccessibilityFromMainRoom)
            {
                connectionFound = false;

                // If there is already a connection to this room continue to next roomA
                if (roomA.connectedRooms.Count > 0)
                {
                    continue;
                }
            }

            foreach (Room roomB in roomListB)
            {
                if (roomA == roomB || roomA.IsConnected(roomB))
                {
                    continue;
                }

                for (int tileIndexA = 0; tileIndexA < roomA.edgeTiles.Count; ++tileIndexA)
                {
                    for (int tileIndexB = 0; tileIndexB < roomB.edgeTiles.Count; ++tileIndexB)
                    {
                        Coordinate tileA = roomA.edgeTiles[tileIndexA];
                        Coordinate tileB = roomB.edgeTiles[tileIndexB];

                        int distanceBetweenRooms = (int)(Mathf.Pow(tileB.xPos - tileA.xPos, 2) + Mathf.Pow(tileB.yPos - tileA.yPos, 2));

                        if (distanceBetweenRooms < bestDistance || !connectionFound)
                        {
                            bestDistance = distanceBetweenRooms;
                            connectionFound = true;
                            bestTileA = tileA;
                            bestTileB = tileB;
                            bestRoomA = roomA;
                            bestRoomB = roomB;
                        }
                    }
                }
            }

            if (connectionFound && !forceAccessibilityFromMainRoom)
            {
                CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
            }
        }

        if (connectionFound && forceAccessibilityFromMainRoom)
        {
            CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
            ConnectClosestRooms(allRooms, true);
        }

        if (!forceAccessibilityFromMainRoom)
        {
            ConnectClosestRooms(allRooms, true);
        }
    }

    void CreatePassage(Room roomA, Room roomB, Coordinate tileA, Coordinate tileB)
    {
        Room.ConnectedRooms(roomA, roomB);

        List<Coordinate> line = GetLine(tileA, tileB);

        foreach (Coordinate c in line)
        {
            for (int x = c.xPos; x < c.xPos + 4; ++x)
            {
                for (int y = c.yPos; y < c.yPos + 4; ++y)
                {
                    map[x, y] = 0;
                }
            }
        }
    }

    List<Coordinate> GetLine(Coordinate from, Coordinate to)
    {
        List<Coordinate> line = new List<Coordinate>();

        int x = from.xPos;
        int y = from.yPos;

        int dx = to.xPos - from.xPos;
        int dy = to.yPos - from.yPos;

        bool inverted = false;
        int step;
        int gradientStep;

        int longest = Mathf.Abs(dx);
        int shortest = Mathf.Abs(dy);

        if (longest < shortest)
        {
            inverted = true;
            longest = Mathf.Abs(dy);
            shortest = Mathf.Abs(dx);

            step = Math.Sign(dy);
            gradientStep = Math.Sign(dx);
        }

        else
        {
            step = Math.Sign(dx);
            gradientStep = Math.Sign(dy);
        }

        int gradientAccumulation = (longest / 2);

        for (int i = 0; i < longest; ++i)
        {
            line.Add(new Coordinate(x, y));

            if (inverted)
            {
                y += (step);
            }
            else
            {
                x += (step);
            }

            gradientAccumulation += shortest;

            if (gradientAccumulation >= longest)
            {
                if (inverted)
                {
                    x += (gradientStep);
                }
                else
                {
                    y += (gradientStep);
                }

                gradientAccumulation -= longest;
            }
        }

        return line;
    }

    Vector3 CoordinateToWorldPoint(Coordinate tile)
    {
        return new Vector3(tile.xPos + mapInfo.mapBorderSize, tile.yPos + mapInfo.mapBorderSize, 0f);
    }

    List<List<Coordinate>> GetRegions(int tileType)
    {
        List<List<Coordinate>> regions = new List<List<Coordinate>>();
        int[,] mapFlags = new int[mapInfo.levelWidth, mapInfo.levelHeight];

        for (int x = 0; x < mapInfo.levelWidth; x += 4)
        {
            for (int y = 0; y < mapInfo.levelHeight; y += 4)
            {
                if (mapFlags[x, y] == 0 && map[x, y] == tileType)
                {
                    List<Coordinate> newRegion = GetRegionTiles(x, y);
                    regions.Add(newRegion);

                    foreach (Coordinate tile in newRegion)
                    {
                        mapFlags[tile.xPos, tile.yPos] = 1;
                    }
                }
            }
        }

        return regions;
    }

    List<Coordinate> GetRegionTiles(int startX, int startY)
    {
        List<Coordinate> tiles = new List<Coordinate>();
        int[,] mapFlags = new int[mapInfo.levelWidth, mapInfo.levelHeight];
        int tileType = map[startX, startY];

        Queue<Coordinate> queue = new Queue<Coordinate>();
        queue.Enqueue(new Coordinate(startX, startY));
        mapFlags[startX, startY] = 1;

        while (queue.Count > 0)
        {
            Coordinate tile = queue.Dequeue();

            tiles.Add(tile);

            for (int x = tile.xPos - (1 * 4); x <= tile.xPos + (1 * 4); x += 4)
            {
                for (int y = tile.yPos - (1 * 4); y <= tile.yPos + (1 * 4); y += 4)
                {
                    if (CoordinateInMapRange(x, y) && ((x == tile.xPos) || (y == tile.yPos)))
                    {
                        if (mapFlags[x, y] == 0 && map[x, y] == tileType)
                        {
                            mapFlags[x, y] = 1;
                            queue.Enqueue(new Coordinate(x, y));
                        }
                    }
                }
            }
        }

        return tiles;
    }

    struct Coordinate
    {
        public int xPos;
        public int yPos;

        public Coordinate(int x, int y)
        {
            xPos = x;
            yPos = y;
        }
    }

    class Room : IComparable<Room>
    {
        public List<Coordinate> tiles;
        public List<Coordinate> edgeTiles; //Could this be used for the minimap?
        public List<Room> connectedRooms;
        public int roomSize;
        public bool isAccessibleFromMainRoom;
        public bool isMainRoom;

        public Room()
        {
        }

        public Room(List<Coordinate> roomTiles, int[,] map)
        {
            tiles = roomTiles;
            roomSize = tiles.Count;
            connectedRooms = new List<Room>();

            edgeTiles = new List<Coordinate>();
            foreach (Coordinate tile in tiles)
            {
                for (int x = tile.xPos - (1 * 4); x <= tile.xPos + (1 * 4); x += 4)
                {
                    for (int y = tile.yPos - (1 * 4); y <= tile.yPos + (1 * 4); y += 4)
                    {
                        if ((x == tile.xPos || y == tile.yPos) && x >= 0 && x < map.GetLength(0) && y >= 0 && y < map.GetLength(1))
                        {
                            if (map[x, y] == 1)
                            {
                                edgeTiles.Add(tile);
                            }
                        }
                    }
                }
            }
        }

        public void SetAccessibleFromMainRoom()
        {
            if (!isAccessibleFromMainRoom)
            {
                isAccessibleFromMainRoom = true;

                foreach (Room connectedRoom in connectedRooms)
                {
                    connectedRoom.SetAccessibleFromMainRoom();
                }
            }
        }

        public static void ConnectedRooms(Room roomA, Room roomB)
        {
            if (roomA.isAccessibleFromMainRoom)
            {
                roomB.SetAccessibleFromMainRoom();
            }
            else if (roomB.isAccessibleFromMainRoom)
            {
                roomA.SetAccessibleFromMainRoom();
            }

            roomA.connectedRooms.Add(roomB);
            roomB.connectedRooms.Add(roomA);
        }

        public bool IsConnected(Room otherRoom)
        {
            return connectedRooms.Contains(otherRoom);
        }

        public int CompareTo(Room otherRoom)
        {
            return otherRoom.roomSize.CompareTo(roomSize);
        }
    }

    bool CoordinateInMapRange(int x, int y)
    {
        return x >= 0 && x < mapInfo.levelWidth && y >= 0 && y < mapInfo.levelHeight;
    }
    #endregion
}
