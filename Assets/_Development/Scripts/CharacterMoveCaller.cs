﻿/** Feel free to scrap this script, not following style guide at all here
 *  just being used to show off / test animations
 * @George Castle
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CharacterMoveCaller : MonoBehaviour
{
    private Animator myAnim;
    private CharacterMovement charMove;

    private void Awake()
    {
        charMove = GetComponent<CharacterMovement>();
    }

    void Start()
    {
        myAnim = gameObject.GetComponent<Animator>();
    }

    void Update()
    {
    
        Vector2 moveVec = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        charMove.Move(moveVec);

        if (Input.GetButtonDown("Fire1"))
        {
            myAnim.SetTrigger("Attack");
        }
        updateAnimator();
    }
    void updateAnimator()
    {
        if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0.1 || Mathf.Abs(Input.GetAxis("Vertical")) > 0.1)
        {
            myAnim.SetBool("IsMoving", true);
        }
        else
        {
            myAnim.SetBool("IsMoving", false);
        }

        if (Input.GetAxis("Horizontal") < -0.1)
        {
            transform.localScale = new Vector3(-0.7f, 0.7f, 1);
        }
        if (Input.GetAxis("Horizontal") > 0.1)
        {
            transform.localScale = new Vector3(0.7f, 0.7f, 1);
        }
    }
}
