﻿/**
 * Placeholder input receiver attached to quit button in pause menu.
 * Calls the GameManager's QuitGame method.
 * @Author Zena Abulhab
 */

using UnityEngine;

public class UIQuitToggle : MonoBehaviour
{
    public void OnQuitPress()
    {
        GameManager.QuitGame();
    }
}
