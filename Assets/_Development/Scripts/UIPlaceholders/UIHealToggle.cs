﻿/**
 * Placeholder button input receiver script to make heal button work
 * without dragging in Player
 * @Author Zena Abulhab
 */

using UnityEngine;

public class UIHealToggle : MonoBehaviour
{
    private Health health;

    // Start is called before the first frame update
    void Start()
    {
        // Find game object with player tag and get its Health script instance
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        health = player.GetComponent<Health>();
    }

    public void ApplyOneHeal()
    {
        health.Heal(1);
    }
}
