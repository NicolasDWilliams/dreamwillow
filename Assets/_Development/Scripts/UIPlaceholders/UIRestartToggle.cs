﻿/**
 * Placeholder input receiver attached to restart button in pause menu.
 * Calls the GameManager's RestartScene method.
 * @Author Zena Abulhab
 */

using UnityEngine;

public class UIRestartToggle : MonoBehaviour
{
    public void OnRestartPress()
    {
        GameManager.RestartGame();
    }
}
