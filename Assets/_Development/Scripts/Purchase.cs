﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Purchase : MonoBehaviour
{
    // Start is called before the first frame update
    private PlayerInventory inventory;
    private ShopKeep Shop;
    [SerializeField] private GameObject Door;
    void Start()
    {
        inventory = PlayerCharacter.Get().GetComponent<PlayerInventory>();
        Shop = gameObject.transform.parent.gameObject.transform.parent.gameObject.transform.parent.gameObject.GetComponent<ShopKeep>();
        Door = Shop.gameObject.GetComponentInChildren<Door>().gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PurchaseItems()
    {

        inventory.MoneyLose(Shop.moneyOwed);

        Shop.moneyOwed = 0;
        Shop.bought = true;

        // Despawn door
        Door.GetComponent<BoxCollider2D>().enabled = false;
        Door.GetComponent<SpriteRenderer>().enabled = false;
    }

    public void exit()
    {
        //Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1f;
        gameObject.GetComponentInParent<ShopUI>().menuOn = false;

    }
}
