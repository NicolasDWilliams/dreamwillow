﻿/** Feel free to scrap this script, not following style guide at all here
 *  just being used to show off / test animations
 * @George Castle
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public Vector2 moveDir;
    public GameObject attacker;
    [SerializeField] float moveSpeed;
    [SerializeField] GameObject hitParticlePrefab;
    [SerializeField] Transform arrowTip;
    private float angle;

    private void Start() {
        //set the rotation of the projectile to match 
        //the direction the projectile is moving
        angle = Vector2.SignedAngle(new Vector2(0,1), moveDir);
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
    }
 
    void Update()
    {
        //moves the projectile along the moveDir line
        gameObject.transform.position = new Vector3(gameObject.transform.position.x + (moveDir.x * moveSpeed * Time.deltaTime), gameObject.transform.position.y + (moveDir.y * moveSpeed * Time.deltaTime), gameObject.transform.position.z);
    }

    private void OnTriggerEnter2D(Collider2D other) {
        //if the projectile hits a wall, spawn a hit prefab
        if (other.tag == "Walls") {
            Instantiate(hitParticlePrefab, arrowTip.position, Quaternion.Euler(new Vector3(0, 0, angle)));
        }

        //if the projectile hits the wall or the player, destroy the projectile
        if (other.tag == "Player" || other.tag == "Walls") {
            Destroy(gameObject);
        }
        if (other.tag == "Enemy") {
            Destroy(other.gameObject.transform.parent.gameObject);
        }
    }
}
