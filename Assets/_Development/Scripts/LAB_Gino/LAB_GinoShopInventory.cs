﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LAB_GinoShopInventory : MonoBehaviour
{
    public static LAB_GinoShopInventory _inv;

    [SerializeField] public int MoneyOwed;

    [SerializeField] private GameObject Item0;
    [SerializeField] private GameObject Item1;
    [SerializeField] private GameObject Item2;
    private bool ShopItem0IsOld;
    private bool ShopItem1IsOld;
    [Space]
    [SerializeField] private GameObject Item0Old;
    [SerializeField] private GameObject Item0New;
    [SerializeField] private GameObject Item1Old;
    [SerializeField] private GameObject Item1New;

    [Space]
    [SerializeField] private int Item0Price;
    [SerializeField] private int Item1Price;

    private void Awake()
    {
        _inv = this;
        Item0Price = Item0.GetComponentInChildren<LAB_GinoItemForSale>().price;
        Item1Price = Item1.GetComponentInChildren<LAB_GinoItemForSale>().price;
    }

    public void SwapItem0(LAB_GinoPlayerInventory player)
    {
        GameObject item = player.Item0;
        player.Item0 = this.Item0;
        this.Item0 = item;
        
        if (ShopItem0IsOld)
        {
            Item0Old.SetActive(false);
            Item0New.SetActive(true);
            ShopItem0IsOld = false;
            MoneyOwed -= Item0Price;
        }
        else
        {
            Item0Old.SetActive(true);
            Item0New.SetActive(false);
            ShopItem0IsOld = true;
            MoneyOwed += Item0Price;
        }
    }

    public void SwapItem1(LAB_GinoPlayerInventory player)
    {
        GameObject item = player.Item1;
        player.Item1 = this.Item1;
        this.Item1 = item;
        
        if (ShopItem1IsOld)
        {
            Item1Old.SetActive(false);
            Item1New.SetActive(true);
            ShopItem1IsOld = false;
            MoneyOwed -= Item1Price;
        }
        else
        {
            Item1Old.SetActive(true);
            Item1New.SetActive(false);
            ShopItem1IsOld = true;
            MoneyOwed += Item1Price;
        }
    }
}
