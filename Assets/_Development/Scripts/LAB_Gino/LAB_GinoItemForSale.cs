﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LAB_GinoItemForSale : MonoBehaviour
{
    public readonly int price = 300;
    [SerializeField] private bool hasTraded;
    [SerializeField] private int itemID;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && hasTraded == false)
        {
            //Debug.LogWarning("Traded for Item0");
            if (itemID == 0)
            {
                LAB_GinoShopInventory._inv.SwapItem0(collision.GetComponent<LAB_GinoPlayerInventory>());
            }
            else if (itemID == 1)
            {
                LAB_GinoShopInventory._inv.SwapItem1(collision.GetComponent<LAB_GinoPlayerInventory>());
            }

            hasTraded = true;
            collision.GetComponent<LAB_GinoPlayerInventory>().CheckPurchase();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && hasTraded)
        {
            //Debug.LogWarning("Trading unlocked");
            hasTraded = false;
        }
    }
}
