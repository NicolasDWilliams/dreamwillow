﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LAB_GinoMoney : MonoBehaviour
{
    [SerializeField] private int value;

    private void OnTriggerEnter2D(Collider2D other)
    {
        // If the player collides with the money object, invoke MoneyCollect unity event and destroy object.
        if (other.GetComponent<PlayerInventory>() != null)
        {

            other.GetComponent<PlayerInventory>().MoneyGain(value);

            Destroy(gameObject);
        }

    }

}
