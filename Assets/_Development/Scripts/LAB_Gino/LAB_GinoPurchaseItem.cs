﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LAB_GinoPurchaseItem : MonoBehaviour
{
    // List of children gameObjects, which are the items for sale.
    [SerializeField] private List<GameObject> itemsForSale = new List<GameObject>();
    // List of bools, corresponding to whether each item has been picked up or not. 
    private List<bool> pickedUp = new List<bool>();
    // Total money owed, equal to total cost of items currently picked up.
    [SerializeField] public int moneyOwed = 0;
    // Player
    private GameObject player;

    [SerializeField] private GameObject door;

    void Start()
    {
        player = PlayerCharacter.Get();

        // Add items for sale to itemsForSale List.
        foreach (Transform child in transform)
        {

            if (child.GetComponent<ShopItem>() != null)
            {

                itemsForSale.Add(child.gameObject);
                pickedUp.Add(false);
            }

        }

    }

    void Update()
    {
        // If item has been picked up, set correpsonding pickedUp to true;
        for (int i = 0; i < itemsForSale.Count; ++i)
        {
            if (itemsForSale[i].GetComponent<ShopItem>().pickedUpItem != "nothing" && !pickedUp[i])
            {

                pickedUp[i] = true;
            }

        }

    }

    // When colliding with the wall, check if player has more money than moneyOwed. If player
    // has enough money, turn boxcollider and spriterenderer off, and subtract moneyOwed from
    // player's MoneyCount.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (moneyOwed <= player.GetComponent<PlayerInventory>().Money)
        {
            SetDoor(door, false);
            //this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            //this.gameObject.GetComponent<SpriteRenderer>().enabled = false;

            player.GetComponent<PlayerInventory>().MoneyLose(moneyOwed);
            // Destroy item once it has been purchased.
            if (moneyOwed != 0)
            {
                for (int i = 0; i < itemsForSale.Count; ++i)
                {
                    if (pickedUp[i])
                    {
                        Destroy(itemsForSale[i]);
                        itemsForSale.RemoveAt(i);
                    }
                }
            }
            moneyOwed = 0;

        }
        else
        {
            // If plyaer does not have enough money, make sure wall blocks player from leaving.
            SetDoor(door, true);
            //this.gameObject.GetComponent<BoxCollider2D>().enabled = true;
            //this.gameObject.GetComponent<SpriteRenderer>().enabled = true;
        }
    }

    private void SetDoor(GameObject obj, bool value)
    {
        obj.GetComponent<BoxCollider2D>().enabled = value;
        obj.GetComponent<SpriteRenderer>().enabled = value;
    }
}
