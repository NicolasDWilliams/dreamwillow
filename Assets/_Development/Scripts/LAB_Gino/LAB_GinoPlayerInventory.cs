﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LAB_GinoPlayerInventory : MonoBehaviour
{
    [SerializeField] public int Money = 500;

    [SerializeField] public int HealthPotion;

    [SerializeField] public GameObject Item0;
    [SerializeField] public GameObject Item1;
    [SerializeField] public GameObject Item2;

    [SerializeField] private GameObject ShopDoor;

    // Increment Money count by 1 when function is called.
    // Add functions that add different amounts of money.
    public void MoneyCollect()
    {
        Money++;
    }
    // Add functions that increment different items as item types are added.

    public void HealthPotionCollect()
    {
        HealthPotion++;
    }

    // Display Money and HealthPotion on screen(for testing)
    void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 300, 20), "Money: " + (Money - LAB_GinoShopInventory._inv.MoneyOwed).ToString());
        GUI.Label(new Rect(10, 0, 100, 20), "Potion: " + HealthPotion);
    }

    public void CheckPurchase()
    {
        if (Money - LAB_GinoShopInventory._inv.MoneyOwed < 0)
        {
            Debug.LogWarning("YOU CANNOT AFFORD THAT");
            ShopDoor.SetActive(true);
        }
        else
        {
            Debug.LogWarning("AFFORDABLE, YOU WILL BE CHARGED ON EXIT");
            ShopDoor.SetActive(false);
        }
    }
}
