﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class itemBuyMenuVisualTempScript : MonoBehaviour
{
    [SerializeField] GameObject costDisplay;
        private void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag == "Player") {
            costDisplay.SetActive(true);
        }
    }
    private void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.tag == "Player") {
            costDisplay.SetActive(false);
        }
    }
}
