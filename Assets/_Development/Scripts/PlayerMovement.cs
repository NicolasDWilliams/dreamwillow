/** Feel free to scrap this script, not following style guide at all here
 *  just being used to show off / test animations
 * @George Castle
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float moveSpeed = 0.0f;
    private Animator myAnim;

    void Start()
    {
        myAnim = gameObject.GetComponent<Animator>();
    }

    void Update()
    {
        transform.position = new Vector3(transform.position.x + (Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime), transform.position.y + (Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime), transform.position.z);

        if (Input.GetButtonDown("Fire1"))
        {
            myAnim.SetTrigger("Attack");
        }
        updateAnimator();
    }
    void updateAnimator()
    {
        if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0.1 || Mathf.Abs(Input.GetAxis("Vertical")) > 0.1)
        {
            myAnim.SetBool("IsMoving", true);
        }
        else
        {
            myAnim.SetBool("IsMoving", false);
        }

        if (Input.GetAxis("Horizontal") < -0.1)
        {
            transform.localScale = new Vector3(-0.7f, 0.7f, 1);
        }
        if (Input.GetAxis("Horizontal") > 0.1)
        {
            transform.localScale = new Vector3(0.7f, 0.7f, 1);
        }
    }
}
