﻿/* Used to log debug messages from a UnityEvent
 * @Max Perraut
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventLog : MonoBehaviour
{
    public void Log(string message)
    {
        Debug.Log(message);
    }

    public void LogFloat(float val)
    {
        Debug.Log("Logged float: " + val.ToString());
    }

    public void LogInt(int val)
    {
        Debug.Log("Logged int: " + val.ToString());
    }
}
