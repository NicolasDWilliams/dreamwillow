﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class ShopKeep : MonoBehaviour
{
    // List of children gameObjects, which are the items for sale.
    private List<GameObject> itemsForSale = new List<GameObject>();
    [HideInInspector] public int items;
    // List of bools, corresponding to whether each item has been picked up or not. 
    private List<bool> pickedUp = new List<bool>();
    // Total money owed, equal to total cost of items currently picked up.
    public int moneyOwed = 0;
    // Bool to tell if items have been purchased.
    [HideInInspector] public bool bought = false;
    // Shopkeeper dialogue text
    [SerializeField] private TextMeshPro shopText;
    private bool shopTextIsChanging = false;
    private string targetMessage;

    // Player
    private GameObject player;

    public UnityEvent OnSuccessfulItemPurchase;
    public UnityEvent OnUnSuccessfulItemPurchase;
    public UnityEvent ShopkeeperDialogue;

    void Start()
    {
        player = PlayerCharacter.Get();

        // Add items for sale to itemsForSale List.
        foreach (Transform child in transform)
        {

            if (child.GetComponent<ShopItem>() != null && child.GetComponent<Collider2D>() != null)
            {

                itemsForSale.Add(child.gameObject);
                pickedUp.Add(false);
            }

        }

        var message = "Buy Something!\nWalk up to an item\nto select it";
        shopText.text = message;
        targetMessage = message;
    }

    void Update()
    {
        items = itemsForSale.Count;
        // If item has been picked up, set correpsonding pickedUp to true;
        for (int i = 0; i < itemsForSale.Count; ++i)
        {

            if (itemsForSale[i].GetComponent<ShopItem>().pickedUpItem != "nothing")
            {
                pickedUp[i] = true;
            }
            if (itemsForSale[i].GetComponent<ShopItem>().pickedUpItem == "nothing" && pickedUp[i])
            {
                pickedUp[i] = false;
            }

        }

        if (shopTextIsChanging == false)
            UpdateShopText();

    }

    // Updates shopkeeper's dialogue
    private void UpdateShopText()
    {
        string message = "Buy Something!\nWalk up to an item\nto select it";

        // If the player has made a purchase
        if (bought)
        {
            message = "Thanks Friend!";
        }
        // If the cart is not empty
        if (moneyOwed > 0)
        {
            // If the player cannot pickup any more masks
            if (player.GetComponent<PlayerInventory>().Collars >= 6 && GetPickedUpItem().GetComponent<ShopItem>().isCollar())
            {
                message = "You can't carry\nanymore of those!";
            }
            // If the player cannot pickup any more hearts
            else if (player.GetComponent<Health>().max >= 6 && GetPickedUpItem().GetComponent<ShopItem>().isHeart())
            {
                message = "You can't carry\nanymore of those!";
            }
            // If the player cannot pickup any more spell upgrades
            else if (GetPickedUpItem().GetComponent<ShopItem>().isSpellUpgradeAtMax())
            {
                message = "You can't carry\nanymore of those!";
            }
            // If the player cannot afford the item
            else if (moneyOwed > player.GetComponent<PlayerInventory>().Money)
            {
                message = "You can't afford that!";
            }
        }
        // Set shopkeeper text if necessary
        if (targetMessage != message)
        {
            ShopkeeperDialogue.Invoke();
            ChangeText(message);
            targetMessage = message;
        }
    }

    // Purchase held items, called by interactable
    public void OnPurchaseItems()
    {
        int temp = itemsForSale.Count;

        if (GetPickedUpItem() == null) {
            return;
        }

        // If the player has no purchases or cannot afford the items
        if (moneyOwed == 0 || moneyOwed > player.GetComponent<PlayerInventory>().Money) {
            OnUnSuccessfulItemPurchase.Invoke();
            return;
        }

        // If the player cannot acquire any more masks
        if (player.GetComponent<PlayerInventory>().Collars >= 6 && GetPickedUpItem().GetComponent<ShopItem>().isCollar())
        {
            OnUnSuccessfulItemPurchase.Invoke();
            return;
        }
        // If the player cannot acquire any more hearts
        else if (player.GetComponent<Health>().max >= 6 && GetPickedUpItem().GetComponent<ShopItem>().isHeart())
        {
            OnUnSuccessfulItemPurchase.Invoke();
            return;
        }
        // If the player cannot acquire any more spell upgrades
        else if (GetPickedUpItem().GetComponent<ShopItem>().isSpellUpgradeAtMax())
        {
            OnUnSuccessfulItemPurchase.Invoke();
            return;
        }

        for (int i = 0; i < temp; ++i)
        {
            if (pickedUp[i])
            {
                itemsForSale[i].GetComponent<ShopItem>().OnPurchase();
                itemsForSale[i].GetComponent<ShopItem>().isBeingDestroyed = true;
                Destroy(itemsForSale[i]);
                itemsForSale.RemoveAt(i);
                pickedUp.RemoveAt(i);
                temp = pickedUp.Count;
                --i;

                OnSuccessfulItemPurchase.Invoke();
            }
        }

        player.GetComponent<PlayerInventory>().MoneyLose(moneyOwed);
        moneyOwed = 0;
        bought = true;
    }

    private GameObject GetPickedUpItem()
    {
        int temp = itemsForSale.Count;
        
        for (int i = 0; i < temp; i++)
        {
            if (pickedUp[i])
                return itemsForSale[i];
        }
     
        return null;
    }
    
    private void ChangeText(string message)
    {
        StartCoroutine(FadeTextOut(message));
    }

    private IEnumerator FadeTextOut(string message)
    {
        shopTextIsChanging = true;
        var popup = shopText.gameObject.GetComponentInParent<UIPopupMessage>();
        popup.HideMessage();
        // Wait for fade to finish
        yield return new WaitForSeconds(popup.fadeTime);
        StartCoroutine(FadeTextIn(message));
    }

    private IEnumerator FadeTextIn(string message)
    {
        shopText.text = message;
        var popup = shopText.gameObject.GetComponentInParent<UIPopupMessage>();
        popup.ShowMessageTypeWriter();
        // Wait for fade to finish
        yield return new WaitForSeconds(0.05f * message.Length);
        shopTextIsChanging = false;
    }

    public void OnUnfocused()
    {
        shopText.text = targetMessage;
        //shopText.text = "Buy Something!\nWalk up to an item\nto select it";
        shopTextIsChanging = false;
    }
}
