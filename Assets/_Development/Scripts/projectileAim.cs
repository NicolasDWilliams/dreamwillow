﻿/** Feel free to scrap this script, not following style guide at all here
 *  just being used to show off / test animations
 * 
 *  Kinda hard to explain, but basically there's an object that 
 *  rotates in a circle around a ranged character so that it's either facing the player
 *  if the character is an enemy, or facing the mouse if the character is the player
 * @George Castle
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectileAim : MonoBehaviour
{
    [SerializeField] Transform player;
    private float angle;

    [SerializeField] bool isEnemy;

    private float myNormalX = 0.7f;
    private float myFlipX = -0.7f;

    private void Start() {
        player = GameObject.FindWithTag("Player").transform;
    }

    
    void Update()
    {
        if (isEnemy) {
            EnemyAim();
        }
        else {
            PlayerAim();
        }
    }

    void EnemyAim() {
        Vector3 worldPos = player.position;       
        float dx = 0.0f;
        float dy = 0.0f;
        dx = this.transform.position.x - worldPos.x;
        dy = this.transform.position.y - worldPos.y;

        
        angle = Mathf.Atan2(dy, dx) * Mathf.Rad2Deg;
        

        Quaternion rot = Quaternion.Euler(new Vector3(0, 0, angle + 90));
        this.transform.rotation = rot;
    }

    void PlayerAim() {
            Vector3 worldPos = Input.mousePosition;
            worldPos.z = 10; 
            worldPos = Camera.main.ScreenToWorldPoint(worldPos);        

            float dx = 0.0f;
            float dy = 0.0f;


            dx = this.transform.position.x - worldPos.x;
            dy = this.transform.position.y - worldPos.y;

            string[] temp = Input.GetJoystickNames();
 
            //Check whether array contains anything
            if(temp.Length > 0)
            {
                //Iterate over every element
                for(int i =0; i < temp.Length; ++i)
                {
                    //Check if the string is empty or not
                    if(!string.IsNullOrEmpty(temp[i]))
                    {
                        //If the string isn't empty, a controller is plugged in
                        dx = -Input.GetAxis("RightStickHorizontal");
                        dy = Input.GetAxis("RightStickVertical");
                    }

                }
            }

            if (dx != 0.0f || dy != 0.0f) {
                angle = Mathf.Atan2(dy, dx) * Mathf.Rad2Deg;
            }

            Quaternion rot = Quaternion.Euler(new Vector3(0, 0, angle + 90));

            this.transform.rotation = rot;
    }
}
