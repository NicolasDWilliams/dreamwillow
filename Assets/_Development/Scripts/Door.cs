﻿// Script to go on wall that blocks off entrance to shop. If player picked up an item and does not have
// enough money to pay for it, boxcollider turns on and stops player from leaving. If player has enough
// money, total cost is subtracted from player's MoneyCount and boxcollider turns off, allowing player through.
// If more items are added, this script does not need to be changed. If items are added, change ShopItem and
// add more children to shop gameObject that this script is on.
// Seyhyun Yang(seyhyun@umich.edu)

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    // Total money owed, equal to total cost of items currently picked up.
    private int moneyOwed;
    // Player
    void Start()
    {
        moneyOwed = this.gameObject.GetComponentInParent<ShopKeep>().moneyOwed;
        this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
        this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
    }

    // When colliding with the wall, check if player has more money than moneyOwed. If player
    // has enough money, turn boxcollider and spriterenderer off, and subtract moneyOwed from
    // player's MoneyCount.
    private void Update()
    {
        moneyOwed = this.gameObject.GetComponentInParent<ShopKeep>().moneyOwed;

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (moneyOwed == 0)
        {
            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            this.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            moneyOwed = 0;

        }
        else
        {
            // If player does not have enough money, make sure wall blocks player from leaving.
            this.gameObject.GetComponent<BoxCollider2D>().enabled = true;
            this.gameObject.GetComponent<SpriteRenderer>().enabled = true;
        }
    }

}
