﻿// Sciprt for items in shop. Every item in shop should have this. What kind of item it is should be set in inspector. If
// it is a healthpotion, change potionNum, if it is attackUp, modify the according values, etc.
// If new item types are added, add another if else statement.
// This script checks if the player has collided with it, and if the player does,
// it gives the player the item, and turns on the wall that blocks the entrance of the shop.
// If the player has enough money, the wall's boxcollider will turn off, allowing the player
// to leave. If the player collides with the item having already picked it up, it will be put
// down and the item will be removed from player's inventory.
// Seyhyun Yang(seyhyun@umich.edu)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShopItem : MonoBehaviour
{
    // Dropdown menu for item type.
    // TODO : Add more item types if they are implemented.
    [System.Serializable]
    public enum itemType
    {
        potion,
        collar,
        heart,
        spellUpgrade
    }
    [SerializeField] private SpriteRenderer itemSprite;
    [SerializeField] private SpriteRenderer outlineSprite;
    
    [SerializeField] private itemType itemtype;
    [SerializeField] private Color interactColor;
    // Type of item.
    private string item;
    // The cost of the item.
    [SerializeField] public int cost;
    // If the item is a potion, set this to the number of potions that will be given.
    // TODO : If more items with variable sale amounts are added, implement.
    [SerializeField] private int potionNum;
    // The type of item, and whether it has been picked up, used by PurchaseItem.cs
    [HideInInspector] public string pickedUpItem = "nothing";
    [SerializeField] private bool pickedUp = false;
    // Player
    private PlayerInventory inventory;
    private AttackManager playerAttackManager;
    [SerializeField] private GameObject text;

    public bool isBeingDestroyed = false;

    [SerializeField] private TextMeshPro purchaseText;
    [SerializeField] private TextMeshPro noPurchaseText;
    [SerializeField] private TextMeshPro priceText;

    private void Start()
    {
        // Sets inventory to be the player's PlayerInventory component.
        inventory = PlayerCharacter.Get().GetComponent<PlayerInventory>();

        playerAttackManager = PlayerCharacter.Get().GetComponent<AttackManager>();

        // Switch cases for item types.
        // TODO: Add more if different items are implemented.
        ///itemtype = itemType.potion;
        switch (itemtype)
        {
            case itemType.potion:
                item = "potion";
                break;
            case itemType.collar:
                item = "collar";
                break;
            case itemType.heart:
                item = "heart";
                break;
            case itemType.spellUpgrade:
                item = "spellUpgrade";
                break;
            default:
                break;
        }

        priceText.text = "Cost: " + cost.ToString();
        priceText.color = (cost > inventory.Money) ? Color.red : Color.white;
        priceText.fontStyle = (cost > inventory.Money) ? FontStyles.Bold : FontStyles.Normal;
        priceText.fontWeight = (cost > inventory.Money) ? FontWeight.Black : FontWeight.Regular;
        purchaseText.gameObject.SetActive(cost > inventory.Money ? false : true);
        noPurchaseText.gameObject.SetActive(cost > inventory.Money ? true : false);
    }

    // Mark picked up item
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            purchaseText.gameObject.SetActive(cost > inventory.Money ? false : true);
            noPurchaseText.gameObject.SetActive(cost > inventory.Money ? true : false);
            priceText.color = (cost > inventory.Money) ? Color.red : Color.white;
            priceText.fontWeight = (cost > inventory.Money) ? FontWeight.Black : FontWeight.Regular;
            priceText.fontStyle = (cost > inventory.Money) ? FontStyles.Bold : FontStyles.Normal;
            purchaseText.gameObject.GetComponentInChildren<UIControllerPrompt>().UpdatePromptSprite();


            // If player has not picked up this item yet, pick it up.
            if (!pickedUp)
            {
                // Add cost of item to player's moneyOwed.
                transform.parent.gameObject.GetComponent<ShopKeep>().moneyOwed += cost;
                pickedUp = true;

                foreach (UIPopupMessage popup in text.GetComponentsInChildren<UIPopupMessage>())
                    popup.ShowMessage();

                // Cases for each item type.
                // TODO: add more item types when item implementation has been finalized.

                if (item == "potion")
                {
                    // TODO: Change to animation of item being picked up.
                    outlineSprite.color = Color.black;
                    itemSprite.color = interactColor;
                    // Add healthpotions to inventory.
                    //inventory.HealthPotionGain(potionNum);
                    inventory.inShop = true;
                    pickedUpItem = "potion";
                }

                else if (item == "collar")
                {
                    // TODO: Change to animation of item being picked up.
                    outlineSprite.color = Color.black;
                    itemSprite.color = interactColor;
                    // Add collar to inventory.
                    //inventory.CollarGain(1);
                    pickedUpItem = "collar";
                }

                else if (item == "heart")
                {
                    // TODO: Change to animation of item being picked up.
                    outlineSprite.color = Color.black;
                    itemSprite.color = interactColor;
                    // Add heart container to inventory.
                    pickedUpItem = "heart";
                }

                else if (item == "spellUpgrade")
                {
                    // TODO: Change to animation of item being picked up.
                    outlineSprite.color = Color.black;
                    itemSprite.color = interactColor;
                    // Add heart container to inventory.
                    pickedUpItem = "spellUpgrade";
                }
            }
        }
    }

    // Unmark dropped item
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player") && isBeingDestroyed == false)
        {
            // Remove cost of item from player's moneyOwed.
            transform.parent.gameObject.GetComponent<ShopKeep>().moneyOwed -= cost;

            pickedUp = false;

            foreach (UIPopupMessage popup in text.GetComponentsInChildren<UIPopupMessage>())
                popup.HideMessage();

            // Set sprite color back to normal.
            // TODO: Change to animation of item being put back down.
            outlineSprite.color = Color.clear;
            itemSprite.color = Color.white;
            pickedUpItem = "nothing";

            // Cases for each type of item.
            if (item == "potion")
            {
                // Remove healthpotions from inventory.
                //inventory.HealthPotionGain(-potionNum);
                inventory.inShop = false;
            }

            else if (item == "collar")
            {
                // Remove collar from inventory.
                // TODO : Add this in when collars have been implemented.
                //inventory.CollarLose(1);
            }


        }
    }

    // Add item to player inventory
    public void OnPurchase()
    {
        if (pickedUp)
        {
            if (item == "potion")
            {
                inventory.HealthPotionGain(potionNum);
            }
            else if (item == "collar")
            {
                inventory.CollarsMax++;
                inventory.CollarGain(1);
            }
            else if (item == "heart")
            {
                inventory.gameObject.GetComponent<Health>().max++;
                inventory.gameObject.GetComponent<Health>().Heal(999);
            }
            else if (item == "spellUpgrade")
            {
                int currentProjectCount = playerAttackManager.GetSecondaryProjectileCount();
                if (currentProjectCount < playerAttackManager.GetMaxSecondaryProjectileCount()) {
                    playerAttackManager.SetSecondaryProjectileCount(currentProjectCount + 1);
                }
            }
        }
    }

    public bool isCollar()
    {
        return item == "collar";
    }

    public bool isHeart()
    {
        return item == "heart";
    }

    public bool isSpellUpgradeAtMax()
    {
        int currentProjectCount = playerAttackManager.GetSecondaryProjectileCount();
        if ((currentProjectCount >= playerAttackManager.GetMaxSecondaryProjectileCount()) && item == "heart") {
            return true;
        }
    
        return false;
    }
}
