﻿// A heavily stripped version of the ShopItems script that only deals with
// picking up a health potion. 
// Originally written by Seyhyun Yang(seyhyun@umich.edu)
// - Matt Rader
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PotionItemPickup : MonoBehaviour
{
    [SerializeField] private SpriteRenderer itemSprite;

    [SerializeField] private SpriteRenderer outlineSprite;
    
    [SerializeField] private Color interactColor;

    // If the item is a potion, set this to the number of potions that will be given.
    // TODO : If more items with variable sale amounts are added, implement.
    [SerializeField] private int potionNum;

    // The type of item, and whether it has been picked up, used by PurchaseItem.cs
    [HideInInspector] public string pickedUpItem = "nothing";

    [SerializeField] private bool pickedUp = false;

    // Player
    private PlayerInventory playerInventory;

    [SerializeField] private TextMeshPro pickUpText;

    [SerializeField] private GameObject text;

    public bool isBeingDestroyed = false;

    private void Start()
    {
        // Sets inventory to be the player's PlayerInventory component.
        playerInventory = PlayerCharacter.Get().GetComponent<PlayerInventory>();
    }

    // Mark picked up item
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            pickUpText.gameObject.SetActive(true);
            pickUpText.gameObject.GetComponentInChildren<UIControllerPrompt>().UpdatePromptSprite();

            // If player has not picked up this item yet, pick it up.
            if (!pickedUp)
            {
                pickedUp = true;

                foreach (UIPopupMessage popup in text.GetComponentsInChildren<UIPopupMessage>())
                    popup.ShowMessage();

                // TODO: Change to animation of item being picked up.
                outlineSprite.color = Color.black;
                itemSprite.color = interactColor;
                // Add healthpotions to inventory.
                pickedUpItem = "potion";
            }
        }
    }

    // Unmark dropped item
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player") && isBeingDestroyed == false)
        {
            pickedUp = false;

            foreach (UIPopupMessage popup in text.GetComponentsInChildren<UIPopupMessage>())
                popup.HideMessage();

            // Set sprite color back to normal.
            // TODO: Change to animation of item being put back down.
            outlineSprite.color = Color.clear;
            itemSprite.color = Color.white;
            pickedUpItem = "nothing";
        }
    }

    // Add item to player inventory
    public void OnPickup()
    {
        if (pickedUp)
        {
            playerInventory.HealthPotionGain(potionNum);
            Destroy(gameObject);
        }
    }
}
