﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ShopUI : MonoBehaviour
{
    private List<GameObject> buttons = new List<GameObject>();
    private GameObject shopKeeper;
    private PlayerInventory inventory;
    [HideInInspector] public bool menuOn = false;

    // Start is called before the first frame update
    void Start()
    {
        inventory = PlayerCharacter.Get().GetComponent<PlayerInventory>();
        shopKeeper = transform.parent.gameObject.transform.parent.gameObject;

        foreach (Transform child in transform)
        {

            buttons.Add(child.gameObject);

        }

        for (int i = 0; i < buttons.Count; ++i)
        {
            buttons[i].SetActive(false);
        }
        if (buttons[1] != null)
        {
            EventSystem.current.SetSelectedGameObject(null);

            EventSystem.current.SetSelectedGameObject(buttons[1]);
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (shopKeeper.GetComponent<ShopKeep>().moneyOwed == 0 && shopKeeper.GetComponent<ShopKeep>().items != 0)
        {
            Debug.Log(shopKeeper.GetComponent<ShopKeep>().moneyOwed);
            buttons[0].SetActive(false);

            buttons[1].SetActive(false);
            buttons[2].SetActive(false);
            buttons[3].SetActive(true);
            buttons[4].SetActive(false);

        }
        else if (shopKeeper.GetComponent<ShopKeep>().items == 0)
        {
            buttons[0].SetActive(false);

            buttons[1].SetActive(false);
            buttons[2].SetActive(false);
            buttons[3].SetActive(false);
            buttons[4].SetActive(true);
        }
        else if (shopKeeper.GetComponent<ShopKeep>().moneyOwed <= inventory.Money && !menuOn)
        {
            buyItem();
            menuOn = true;
        }
        else if (shopKeeper.GetComponent<ShopKeep>().moneyOwed > inventory.Money)
        {
            buttons[0].SetActive(true);
            buttons[1].SetActive(false);
            buttons[2].SetActive(false);
            buttons[3].SetActive(false);
            buttons[4].SetActive(false);

        }
    }

    private void buyItem()
    {
        Time.timeScale = 0f;
        buttons[0].SetActive(false);

        buttons[1].SetActive(true);
        buttons[2].SetActive(true);
        buttons[3].SetActive(false);
        buttons[4].SetActive(false);
        Cursor.lockState = CursorLockMode.Confined;
        EventSystem.current.SetSelectedGameObject(null);

        EventSystem.current.SetSelectedGameObject(buttons[1]);
    }
}
