﻿/** Feel free to scrap this script, not following style guide at all here
 *  just being used to show off / test animations
 *
 *  This script 
 * @George Castle
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rangedAttack : MonoBehaviour
{
    public GameObject attacker;
    [SerializeField] GameObject projectileSpawnPoint;
    [SerializeField] GameObject projectilePrefab;
    private GameObject lastProjectile;

    public void spawnRangedAttack()
    {
        //Instantiate a projectile at the projectile spawn point position
        lastProjectile = Instantiate(projectilePrefab, projectileSpawnPoint.transform.position, Quaternion.identity);
        //set the attacker variable on the projectile
        lastProjectile.GetComponent<Projectile>().attacker = attacker;
        //get a vector line from the character using a ranged attack to the spawn point
        Vector2 moveDir = (projectileSpawnPoint.transform.position - attacker.transform.position);

        //set the moveDir variable on the projectile
        lastProjectile.transform.GetComponent<Projectile>().moveDir = moveDir;
        //set the projectile's rotation to be the same as the projectile's spawn point
        lastProjectile.transform.rotation = projectileSpawnPoint.transform.parent.rotation;
    }
}

