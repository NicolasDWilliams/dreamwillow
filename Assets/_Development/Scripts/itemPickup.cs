﻿/** Feel free to scrap this script, not following style guide at all here
 *  just being used to show off / test animations
 *  
 *  this script is used to show what picking up an item may look like
 * @George Castle
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class itemPickup : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other) {
        //if the player collides with an item, destroy the item
        if (other.tag == "Player") {
            Destroy(gameObject);
        }
    }
}
