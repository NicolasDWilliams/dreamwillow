﻿/** Feel free to scrap this script, not following style guide at all here
 *  just being used to show off / test animations
 * @George Castle
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other) {
        //When the player's hurtbox hit's the enemy's hitbox, destroy the enemy
        if (other.gameObject.tag == "Enemy") {
            Destroy(other.gameObject.transform.parent.gameObject);
        }
    }
}
