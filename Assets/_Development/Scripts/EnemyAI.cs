﻿/** Feel free to scrap this script, not following style guide at all here
 *  just being used to show off / test animations
 *  
 *  this script is very basic enemy behavior. Enemies turn to face 
 *  the player and play their attack animation if the player is in range
 * @George Castle
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    [SerializeField] float myFlipX = 0.0f;
    [SerializeField] float myNormalX = 0.0f;
    Animator myAnim;

    void Start()
    {
        myAnim = gameObject.GetComponent<Animator>();
    }

    private void OnTriggerStay2D(Collider2D other) {
        //if the player is witthin the enemy's attack range
        if (other.tag == "Player") {
            //If the player is to the right of the enemy, flip the enemy to face the player
            if (other.transform.position.x >= transform.position.x) {
                transform.localScale = new Vector3(myFlipX, transform.localScale.y, 1);
            }
            else {
                transform.localScale = new Vector3(myNormalX, transform.localScale.y, 1);
            }
            //Make the enemy play the attack animation
            myAnim.SetTrigger("Attack");
        }
    }
}
