﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enablePlayerParticle : MonoBehaviour
{
    [SerializeField] GameObject forceField;

    public void enablePlayerParticleEffect() {
        PlayerInventory playerInventory = PlayerCharacter.Get().GetComponent<PlayerInventory>();

        if (playerInventory.collarsUsed < playerInventory.CollarsMax)
        {
            forceField.SetActive(true);
            PlayerCharacter.Get().GetComponent<enableParticle>().enableObject();
        }
    }
}
