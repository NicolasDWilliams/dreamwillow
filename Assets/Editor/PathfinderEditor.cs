﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(WSoftPathfinder))]
public class PathfinderEditor : Editor
{
    WSoftPathfinder lastPathfinder;
    void OnEnable()
    {
        lastPathfinder = (WSoftPathfinder)target;
    }

    /*
    public override void OnInspectorGUI()
    {
        WSoftPathfinder pathfinder = (WSoftPathfinder)target;
        pathfinder.debugMode = (bool)EditorGUILayout.Toggle("Debug Mode", pathfinder.debugMode);
        pathfinder.currentMode = (PathfinderMode) EditorGUILayout.EnumPopup("Current Mode", pathfinder.currentMode);
        switch(pathfinder.currentMode)
        {
            case PathfinderMode.Straight:
                pathfinder.target = (Transform)EditorGUILayout.ObjectField("Target", pathfinder.target, typeof(Transform), true);
                pathfinder.continousUpdate = (bool)EditorGUILayout.Toggle("Continous Update", pathfinder.continousUpdate);
                //pathfinder.radius = lastPathfinder.radius;
                //pathfinder.boundingCollider = lastPathfinder.boundingCollider;
                break;
            case PathfinderMode.Nothing:
                /*pathfinder.target = lastPathfinder.target;
                pathfinder.continousUpdate = lastPathfinder.continousUpdate;
                pathfinder.radius = lastPathfinder.radius;
                pathfinder.boundingCollider = lastPathfinder.boundingCollider;
                break;
            case PathfinderMode.Random:
                //pathfinder.target = lastPathfinder.target;
                //pathfinder.continousUpdate = lastPathfinder.continousUpdate;
                pathfinder.radius = (float)EditorGUILayout.FloatField("Radius", pathfinder.radius);
                pathfinder.boundingCollider = lastPathfinder.boundingCollider;
                break;
            case PathfinderMode.Bounded:
                //pathfinder.target = lastPathfinder.target;
                //pathfinder.continousUpdate = lastPathfinder.continousUpdate;
                //pathfinder.radius = lastPathfinder.radius;
                pathfinder.boundingCollider = (Collider2D)EditorGUILayout.ObjectField("Bounding Collider", pathfinder.boundingCollider, typeof(Collider2D), true);
                break;
        }

        lastPathfinder = pathfinder;
        
    }
    */
}

