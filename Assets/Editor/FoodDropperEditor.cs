﻿/**
 * Adds dynamic inspector value changes to FoodDropper script; when not using 
 * default drop value, this enables the input field for a custom food drop value
 * As of final sprint, radius cannot be changed to avoid spawning food inside inaccesible areas
 * Zena Abulhab
 */

using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(FoodDropper))]
public class FoodDropperEditor : Editor
{
	public override void OnInspectorGUI()
	{
		FoodDropper target = (FoodDropper)this.target;

        if (!(target.useDefaultChance = EditorGUILayout.Toggle("Default Chance:", target.useDefaultChance)))
		{
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField("Drop Chance: ");
            GUILayout.Space(-100);
            // assign custom value back to dropChance
            target.dropChance = EditorGUILayout.FloatField(target.dropChance, GUILayout.MaxWidth(1000));

            EditorGUILayout.EndHorizontal();
        }
	}
}
