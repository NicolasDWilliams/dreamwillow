using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(LevelGenerator))]
public class LevelGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        EditorGUILayout.Space();
        EditorGUILayout.Space();

        LevelGenerator myScript = (LevelGenerator)target;
        if (GUILayout.Button("Create Level", GUILayout.Height(35)))
        {
            myScript.CreateNewMap();
        }

        EditorGUILayout.Space();

        if (GUILayout.Button("Clear Tilemap", GUILayout.Height(35)))
        {
            myScript.ClearTileMaps();
        }
    }
}